<?php

/**
 * Dismissable Contracts File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2021 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

$sql = "SELECT
    tc.id,
    tc.refnum,
    ta.lname,
    ta.fname,
    td.description as status,
    tcd.dt_dsm,
    deadline_wowe(tcd.dt_dsm, td.deadline) AS deadline,
    REPLACE(REPLACE(REPLACE(REPLACE(TEXT(array_agg(split_part(tu.\"name\", ' ', 1) order by tu.\"name\" asc)), '{', ''), '}', ''), ',', ', '), 'NULL', '') AS users,
    td.after_deadline,
    td.helptext_wk
FROM
    t_cont_dismissal tcd
LEFT JOIN
    t_dismissal td ON
    tcd.id_dsm = td.id
LEFT JOIN
    t_contracts tc ON
    tcd.id = tc.id
INNER JOIN
    t_address ta ON
    tc.ida = ta.id
LEFT JOIN
    t_dismissal_users tdu ON
    td.id = tdu.id_dismissal
LEFT JOIN
    t_users tu ON
    tdu.id_user = tu.id
LEFT JOIN
    t_cont_vd tcv ON
    tcv.id=tc.id
WHERE
	tcv.\"17\"='true'
GROUP BY
    (tc.id,
    tc.refnum,
    ta.lname,
    ta.fname,
    td.description,
    tcd.dt_dsm,
    deadline,
    after_deadline,
    helptext_wk)";
$stmt = $pdo->prepare($sql);
$stmt->execute();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/styles.css">

    <!-- css bootstrap datatables stuff -->
    <link rel="stylesheet" href="css/datatables.min.css" />

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jquery
        $(document).ready(function() {
            $('#dismissTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Alle"]
                ],
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });
    </script>

</head>

<body>


    <body>
        <!-- navbar -->
        <?php require_once 'includes/navbar.php'; ?>

        <!-- main part -->
        <div class="container">
            <div class="page-header" id="banner">
                <div class="row">
                    <div class="col-lg-12 col-md-8 col-sm-7">
                        <h3>Übersicht aller kündbaren Verträge</h3>
                    </div>

                    <!-- table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 tabley">
                        <table id="dismissTable" class="display table table-bordered table-striped table-hover cell-border nowrap">
                            <thead>
                                <tr>
                                    <th>Aktenzeichen</th>
                                    <th>Nachname</th>
                                    <th>Vorname</th>
                                    <th>Status</th>
                                    <th>Datum Änderung Mandatsstatus</th>
                                    <th>Frist Bearbeitung [Tage]</th>
                                    <th>Sachbearbeiter</th>
                                    <th>Folge bei Fristablauf</th>
                                    <th>Hinweistext Bearbeiter</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "</tr>";
                                    echo "<td><a href='contract.php?id=$row[id]'><strong>" . $row['refnum'] . "</strong></a></td>";
                                    echo "<td>" . $row['lname'] . "</td>";
                                    echo "<td>" . $row['fname'] . "</td>";
                                    echo "<td>" . $row['status'] . "</td>";
                                    echo "<td>" . $row['dt_dsm'] . "</td>";
                                    echo "<td>" . $row['deadline'] . "</td>";
                                    echo "<td>" . $row['users'] . "</td>";
                                    echo "<td>" . $row['after_deadline'] . "</td>";
                                    echo "<td>" . $row['helptext_wk'] . "</td>";
                                    echo "</tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
            </div>
            <!-- footer -->
            <?php require_once 'includes/footer.php'; ?>
        </div>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap3.min.js"></script>
        <script src="js/main.js"></script>
    </body>

</html>