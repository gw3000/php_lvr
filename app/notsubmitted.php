<?php

/**
 * Not Submitted from Plattform->Delete on Sync
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2022 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

$sql = "SELECT
    tic.contid,
    tic.customerid,
    tic.contnum,
    tic.refnum,
    tic.status,
    tic.created_at,
    tia.lname,
    tia.fname,
    tia.street,
    tia.city,
    tia.zip,
    ta.lastname,
    ta.firstname,
    ta.company,
    tcid.marked_to_delete
FROM
    t_import_contracts tic
LEFT JOIN t_import_address tia ON
    tic.customerid = tia.id
LEFT JOIN t_agents ta ON
    tic.created_by = ta.id
LEFT JOIN t_cont_insapp_del tcid ON
    tic.contid = tcid.contid
WHERE
    tic.status <> 'Submitted' AND tcid.deleted IS NULL";

$stmt = $pdo->query($sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        $(document).ready(function() {
            $('#notsubmittedTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Alle"]
                ],
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });
    </script>
</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <?php require_once 'includes/modals/nseditorModal.php'; ?>
        <div class="page-header" id="banner">
            <h3 class="h3-spacing">Nicht eingereichte Verträge <small class="state-header">löschen</small></h3>
            <p>Die zum Löschen mit <i class='fas fa-calendar-minus'></i>
            markierten Verträge  werden zur nächsten Synchronisation mit der
            lv-r.de Datenbank ebendort gelöscht.</p><br>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 tabley">
                    <table id="notsubmittedTable" class="display table table-bordered table-striped table-hover cell-border" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>ID</th>
                                <th>Vertragsnummer</th>
                                <th>Aktenzeichen</th>
                                <th>Status</th>
                                <th>erzeugt am</th>
                                <th>Vorname (Mandant)</th>
                                <th>Nachname (Mandant)</th>
                                <th>Straße (Mandant)</th>
                                <th>Ort (Mandant)</th>
                                <!-- <th>PLZ (Mandant)</th> -->
                                <th>Vorname (Vermittler)</th>
                                <th>Nachname (Vermittler)</th>
                                <th>Firma (Vermittler)</th>
                            </tr>
                        </thead>
                        <tbody class="custom-control custom-radio custom-control-inline">
                            <?php
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                // row red and icon, if its marked to delete
                                if (is_null($row['marked_to_delete'])) {
                                    $hclass = '';
                                    $marked_to_delete = '';
                                } else {
                                    $hclass = 'danger';
                                    $marked_to_delete = "<center>
                                        <i class='fas fa-calendar-minus'>
                                        </i></button></center>";
                                }

                                echo "<tr class=$hclass >";
                                    echo "<td><center><button type='button' onclick = \"loadDelModal('" . $row['contid'] . "')\" class='btn-trnsp' data-toggle='modal' data-target='#nsDelModal'><i class='fas fa-eraser'></i></button></center></td>";
                                    echo "<td>" . $marked_to_delete . "</td>";
                                    echo "<td>" . $row['customerid'] . "</td>";
                                    echo "<td>" . $row['contnum'] . "</td>";
                                    echo "<td>" . $row['refnum'] . "</td>";

                                    // status
                                    if ($row['status'] == 'Incomplete') {
                                        $status = 'unvollständig';
                                    } elseif ($row['status'] == 'Completed') {
                                        $status = 'vollständig';
                                    } elseif ($row['status'] == 'NotAccepted') {
                                        $status = 'abgelehnt';
                                    }
                                    echo "<td>" . $status . "</td>";

                                    // created at just date no time
                                    $date = new DateTime($row['created_at']);
                                    echo "<td>" . $date->format('Y-m-d') . "</td>";

                                    echo "<td>" . $row['lname'] . "</td>";
                                    echo "<td>" . $row['fname'] . "</td>";
                                    echo "<td>" . $row['street'] . "</td>";
                                    echo "<td>" . $row['city'] . "</td>";
                                    // echo "<td>" . $row['zip'] . "</td>";
                                    echo "<td>" . $row['lastname'] . "</td>";
                                    echo "<td>" . $row['firstname'] . "</td>";
                                    echo "<td>" . $row['company'] . "</td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                    <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
                    <?php
                    // close statement
                    unset($stmt);
                    // close connection
                    unset($pdo);
                    ?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/notsubmitted.js"></script>
    <script src="js/main.js"></script>
    <script src="js/mousewheel.js"></script>
</body>

</html>
