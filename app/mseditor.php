<?php

/**
 * Mandatsstatus Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

$sql = "SELECT
            ms.*,
            array_agg(split_part(us.\"name\", ' ', 2)) as users
        FROM
            t_ms ms
        LEFT JOIN t_ms_users mu ON
            ms.id = mu.id_ms
        LEFT JOIN t_users us ON
            mu.id_user = us.id
        GROUP BY
            (ms.id,
            ms.description,
            ms.helptext,
            ms.helptext_pf,
            ms.helptext_wk,
            ms.deadline,
            ms.after_deadline
            )
        ORDER BY
            ms.id asc";
$stmt = $pdo->query($sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        $(document).ready(function() {
            $('#mseditorTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [5, 10, 25, 50, 100, -1],
                    [5, 10, 25, 50, 100, "Alle"]
                ],
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 650,
                "scrollX": true,
            });
        });
    </script>
</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <?php require_once 'includes/modals/mseditorModal.php'; ?>
        <div class="page-header" id="banner">
            <h3 class="h3-spacing">Mandatsstatus-Rückabwicklung <small>Editor</small> </h3>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12  row-sm">
                    <table id="mseditorTable" class="display table table-bordered table-striped table-hover cell-border tabley" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Status</th>
                                <th>Hinweistext<br>Kommunikation</th>
                                <th>Hinweistext<br>Platform</th>
                                <th>Hinweistext<br>Bearbeiter</th>
                                <th>Frist (Tage)</th>
                                <th>Folge bei Fristablauf</th>
                                <th>Bearbeiter</th>
                                <?php
                                if ($_SESSION['role'] != 'user-fa' && $_SESSION['role'] != 'user-ra') {
                                    echo "<th></th><th></th><th></th>";
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <div class="custom-control custom-radio custom-control-inline">
                                <?php
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    if ($row['id'] % 1000 == 0) {
                                        $hclass = 'danger';
                                    } else {
                                        $hclass = '';
                                    }

                                    // users
                                    $users = $row['users'];
                                    if ($users == '{NULL}') {
                                        $users = '';
                                    } else {
                                        $users = str_replace(',', '<br>', substr_replace(substr_replace($users, '', '', 1), '', -1));
                                    }

                                    // deadline
                                    $deadline = $row['deadline'];
                                    if ($deadline == '') {
                                        $deadline = '';
                                    } elseif ($deadline == '1') {
                                        $deadline = $deadline . ' Tag';
                                    } else {
                                        $deadline = $deadline . ' Tage';
                                    }

                                    // the status table
                                    echo "<tr class=$hclass >";
                                    echo "<td>" . $row['id'] . "</td>";
                                    echo "<td>" . $row['description'] . "</td>";
                                    echo "<td>" . $row['helptext'] . "</td>";
                                    echo "<td>" . $row['helptext_pf'] . "</td>";
                                    echo "<td>" . $row['helptext_wk'] . "</td>";
                                    echo "<td>" . $deadline . "</td>";
                                    echo "<td>" . $row['after_deadline'] . "</td>";
                                    echo "<td>" . $users . "</td>";
                                    if ($_SESSION['role'] != 'user-fa' && $_SESSION['role'] != 'user-ra') {
                                        echo "<td><center><button type='button' onclick = \"loadModModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#msModModal'><i class='fas fa-edit'></i></button></center></td>";
                                        echo "<td><center><button type='button' onclick = \"loadUserModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#msUserModal'><i class='fas fa-user'></i></button></center></td>";
                                        echo "<td><center><button type='button' onclick = \"loadDelModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#msDelModal'><i class='fas fa-eraser'></i></button></center></td>";
                                    }
                                    echo "</tr>";
                                }
                                ?>
                            </div>
                        </tbody>
                    </table>
                    <button id="addStatus" type="button" data-toggle='modal' data-target='#msAddModal' class="btn btn-info">Status hinzufügen</button>
                    <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
                    <?php
                    // close statement
                    unset($stmt);
                    // close connection
                    unset($pdo);
                    ?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/mseditor.js"></script>
    <script src="js/main.js"></script>
    <script src="js/mousewheel.js"></script>
</body>

</html>