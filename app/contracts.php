<?php

/**
 * Contracts File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

if (isset($_GET['state'])) {
    $state = $_GET['state'];
	// show state in headline
	if ($state == 'de') {
		$true = 'Deutschland';
	} elseif ($state == 'at') {
		$true = 'Österreich';
	} else {
		$true = 'Deutschland';
		$state = 'de';
	}
	// use a cookie to get the param on contractsWorker
	setcookie('affiliation', $state);
} elseif (isset($_GET['unseen'])){
	setcookie('affiliation', 'unseen');
	$true = 'bisher nicht gesichtet';
}


// query import timestamp
$sql = "SELECT max(inserted_at) FROM public.t_import_contracts;";
$stmt = $pdo->prepare($sql);
$stmt->execute();
$row_ts = $stmt->fetch(PDO::FETCH_ASSOC);
$max_import = $row_ts['max'];

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>lvr &middot; Verträge</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/styles.css">

    <!-- css bootstrap datatables stuff -->
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/select.dataTables.min.css">
    <link rel="stylesheet" href="css/fixedColumns.bootstrap4.min.css">

    <!-- js datatables stuff -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/dataTables.fixedColumns.js"></script>

    <!-- init the table -->
    <script>
        // datatables
        $(document).ready(function() {
            $('#contractsTable').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    url: "includes/worker/contractsWorker.php",
                    type: "post"
                },
                "scrollX": true,
                "colReorder": true,
                "stateSave": true,
                "localStorage": true,
                "lengthMenu": [
                    [10, 20, 50, -1],
                    [10, 20, 50, "All"]
                ],
                "fixedColumns": {
                    leftColumns: 3
                },
                "order":[[0, 'desc']],
                "language": {
                    "url": "json/German.json"
                }
            });
        });
    </script>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12 col-md-8 col-sm-7">
                    <h3>Übersicht aller eingereichten Verträge <small class="state-header"><?php echo $true; ?></small></h3>
                    <input type="text" hidden=true id="state" value="<?php echo $state; ?>">
                </div>

                <!-- table -->
                <div class="col-lg-12 col-md-12 col-sm-12 tabley">
                    <table id="contractsTable" class="display table table-bordered table-striped table-hover cell-border nowrap">
                        <thead>
                            <tr>
                                <th>Aktenzeichen</th>
                                <th>Nachname</th>
                                <th>Vorname</th>
                                <th>Mandatsstatus</th>
                                <th>Datum Änderung Mandatsstatus</th>
                                <th>Frist Bearbeitung [Tage]</th>
                                <th>Sachbearbeiter</th>
                                <th>Folge bei Fristablauf</th>
                                <th>Hinweistext Bearbeiter</th>
                                <th>Rechtsschutzstatus</th>
                                <th>Datum der RSV-Status Änderung</th>
                                <th>Versicherungsnummer (alt)</th>
                                <th>Versicherungsnummer</th>
                                <th>Versicherer bei Abschluss</th>
                                <th>Aktueller Versicherer / Gegner</th>
                                <th>Vertragsart</th>
                                <th>Anlageart</th>
                                <th>Vertragsstatus</th>
                                <th>technischer Vertragsbeginn</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
