<?php

/**
 * Index File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}
require_once 'includes/header.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <title>lvr &middot; db</title>
</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>
    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12">
                    <h3>lvr &middot; Statistik</h3>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h4>Anzahl eingereichter Verträge <small>(seit 06.07.2020)</small></h4>
                    <canvas id="uploadHistory" width="400" height="200"></canvas>
                    <hr>
                </div>

                <!-- <div class="col-lg-12 col-md-12 col-sm-12">
                    <h4>Anzahl eingereichter Verträge pro Tag <small>an denen ein Verrag eingerreicht wurde</small></h4>
                    <canvas id="uploadPerDay" width="400" height="200"></canvas>
                    <hr>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h4>Anzahl eingereichter Verträge pro Vermittler <small>(Top 5)</small></h4>
                    <canvas id="uploadUserHistory" width="400" height="200"></canvas>
                    <hr>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h4>Anzahl der Verträge pro Versicherung bei Vertragsabschluss <small>(Top 5)</small></h4>
                    <canvas id="TopFiveInsurances" width="400" height="200"></canvas>
                    <hr>
                </div> -->
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>

    <script src="js/Chart.min.js"></script>
    <script src="js/statistics.js"></script>
</body>

</html>
