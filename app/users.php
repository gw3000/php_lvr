<?php

/**
 * Search File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

$sql = 'SELECT * FROM t_users order by id asc';
$stmt = $pdo->query($sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jsquery
        $(document).ready(function() {
            $('#usersTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });
    </script>
</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <?php require_once 'includes/modals/userModal.php'; ?>
        <div class="page-header" id="banner">
            <h3 class="h3-spacing">Benutzerverwaltung</h3>
            <div class="row">
                <div class="col-lg-12 col-md-8 col-sm-7">
                    <table id="usersTable" class="display table table-bordered table-striped table-hover cell-border tabley" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email Adresse <small>(Login)</small></th>
                                <th>Benutzerrolle</th>
                                <th>Datum der Accounterstellung</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="custom-control custom-radio custom-control-inline">
                            <?php
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                if ($row['role'] == 'admin') {
                                    $hclass = 'danger';
                                    $val = "Administrator";
                                } elseif ($row['role'] == 'manager') {
                                    $hclass = 'info';
                                    $val = "Manager";
                                } elseif ($row['role'] == 'user-ra') {
                                    $hclass = 'active';
                                    $val = "Rechtsanwalt";
                                } elseif ($row['role'] == 'user-fa') {
                                    $hclass = 'warning';
                                    $val = "Fachangestellte";
                                } elseif ($row['role'] == 'user-ph') {
                                    $hclass = 'success';
                                    $val = "Telefon";
                                } elseif ($row['role'] == 'insure') {
                                    $hclass = 'success';
                                    $val = "Manager Versicherung";
                                }

                                echo "<tr class=$hclass >";
                                echo "<td>" . $row['id'] . "</td>";
                                echo "<td>" . $row['name'] . "</td>";
                                echo "<td>" . $row['email'] . "</td>";
                                // echo "<td>" . $row['role'] . "</td>";
                                echo "<td>" . $val . "</td>";
                                echo "<td>" . $row['created_at'] . "</td>";
                                echo "<td><center><button type='button' onclick = \"loadModModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#userModModal'><i class='fas fa-user-edit'></i></button></center></td>";
                                echo "<td><center><button type='button' onclick = \"loadDelModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#userDelModal'><i class='fas fa-eraser'></i></button></center></td>";
                                echo "</tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                    <button id="addUser" type="button" data-toggle='modal' data-target='#userAddModal' class="btn btn-info">Benutzer hinzufügen</button>
                    <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
                    <?php
                    unset($stmt);
                    unset($pdo);
                    ?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/accounts.js"></script>
    <script src="js/main.js"></script>
    <script>
        window.onload = hidePwdChange();
    </script>
</body>

</html>
