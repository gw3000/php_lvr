<?php

/**
 * Contract File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';
require_once 'includes/functions/elements.php';

if (!htmlspecialchars($_GET['id']) == '') {
    $id = $_SESSION['id'] = htmlspecialchars($_GET['id']);
    $sql = "SELECT
                co.id,
                co.contnum,
                co.refnum,
                ad.lname,
                ad.fname,
                ad.lname2,
                ad.fname2,
                ad.id as ida,
                co.in_ram,
                rc.rsv_ablagenummer
            FROM
                t_contracts co
            INNER JOIN t_address ad ON
                co.ida=ad.id
            INNER JOIN t_ram_rsv_cont rc ON
                co.id=rc.id
            WHERE
                co.id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $id = $row['id'];
    if ($id == '') {
        header('location: search.php');
    }
    $_SESSION["refnum"] = $row["refnum"];
} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    include 'includes/writecontract.php';
    header('location: contract.php?id=' . $_SESSION["id"]);
} else {
    header('location: search.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel='stylesheet' href='css/fontawesome.all.css'>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/spinner.css" />

    <title><?php echo $row['contnum'] . " &middot; " . $row['refnum']; ?>
    </title>

    <!-- jquery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../js/datatables.min.js"></script>
</head>

<body>
    <!-- navbar -->
    <?php require 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
       <div id="savespinner" class="spinner"></div>
        <?php require 'includes/modals/commentsModal.php'; ?>
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <form class="form-horizontal" id="contract">
                        <!-- <form class="form-horizontal" id="contract" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST"> -->
                        <?php
                        if ($row['lname2'] != '') {
                            $name = $row['fname'] . " " . $row['lname'] . " &amp; " . $row['fname2'] . " " . $row['lname2'];
                        } else {
                            $name = $row['fname'] . " " . $row['lname'];
                        }

                        // Vertrag in RA-Micro
                        if ($row['in_ram'] == true) {
                            // $in_ram = " ist in RA-Micro &middot; ";
                            $ram_pic = " &middot; <img src='../../img/ram.png' alt='RAMICRO' width='15' height='15'>";
                        } elseif ($row['in_ram'] == null) {
                            // $in_ram = "";
                            $ram_pic = "";
                        }


                        ?>
                        <div id="contractId" hidden='true' value="<?php echo $id; ?>"></div>
                        <div id="currentUserEmail" hidden='true' value="<?php echo $_SESSION['email']; ?>"></div>

                        <h3>Vertrag<small><mark class="red"> <?php echo $row['contnum']; ?>
                                    &middot; <mark class="blue"><?php echo $row['refnum'] . $ram_pic; ?></mark>
                                    <?php  echo($row['rsv_ablagenummer']!="") ? "<mark>abgelegte Akte (" . $row['rsv_ablagenummer'] .")</mark>" : ""; ?>
                                    &middot; <a href="<?php echo 'address.php?ida=' . $row['ida']; ?>"><?php echo $name;  ?></a>
                                    <mark class="yellow">(<?php echo $row['ida']; ?>)</mark>
                            </small>
                            <div class='pull-right'>
                                <button type="button" class="btn btn-sm fas fa-plus-square" id='showAccordion' title='Zeige/Verberge alle Sektionen'></button>
                                <button type="button" class="btn btn-sm fas fa-plus-circle" id='showVars' title='Zeige/Verberge alle Variable'></button>
                            </div>
                        </h3>
                        <div class="h3-spacing">
                            <?php require "includes/contractdetails.php" ?>
                        </div>

                        <div class="sticky-btns">
                            <hr>
                            <div class="form-group ">
                                <div class="btn-group col-lg-10">
                                    <button type="button" class="btn btn-info" id="importData">Import Daten</button>
                                    <button type="button" id="resetData" class="btn btn-primary">Alle Werte zurücksetzen</button>
                                    <button type="button" id="writeContract" class="btn btn-success">Änderungen speichern</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/ms.js"></script>
    <script src="js/contract.js"></script>
    <script src="js/comments.js"></script>
    <script src="js/mousewheel.js"></script>
    <script src="js/rsv.js"></script>
    <script src="js/writeContract.js"></script>
</body>

</html>
