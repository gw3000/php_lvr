<?php

/**
 * One Specific Agent --> Table of Contracts
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

if (!htmlspecialchars($_GET['id']) == '') {
    $id = $_SESSION['id'] = htmlspecialchars($_GET['id']);
} else {
    header('location: index.php');
}

// query to name the agent
if (isset($id)) {
    $sql = "SELECT
            ag.*
        FROM
            t_agents ag
        WHERE
            ag.id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $agent_name = $row['firstname'] . " " . $row['lastname'];
}


// query contracts
if (isset($id)) {
    $sql = "SELECT
        ag.lastname,
        ag.firstname,
        ag.email,
        ag.status,
        ad.lname,
        ad.fname,
        ad.lname2,
        ad.fname2,
        ad.street,
        ad.zip,
        ad.city,
        co.id,
        co.contnum,
        co.refnum,
        co.created_at,
        co.in_ram
    FROM
        t_agents ag
    LEFT JOIN
		t_contracts co
	ON
        ag.id = co.created_by
    LEFT JOIN
		t_address ad
	ON
        ad.id = co.ida
    LEFT JOIN
        t_cont_ms cm
    ON
        cm.id=co.id
    LEFT JOIN
        t_ms ms
    ON
        cm.id_ms=ms.id
    WHERE
        ag.id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $id, PDO::PARAM_STR);
    $stmt->execute();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <!-- css bootstrap datatables stuff -->
    <link rel="stylesheet" href="css/datatables.min.css" />

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jsquery
        $(document).ready(function() {
            $('#agentTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });

        // make rows to links
        $('tr[data-href]').on("click", function() {
            document.location = $(this).data('href');
        });
    </script>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Übersicht der Adressdaten und aller Verträge<small><mark class="blue"> <?php echo $agent_name; ?></a></small> </h3>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <!-- section address -->
                        <div class="panel panel-default" id="myCollapsible">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Adressdaten
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <strong>Name: </strong><?php echo $row['lastname']; ?><br>
                                    <strong>Vorname: </strong> <?php echo $row['firstname']; ?><br>
                                    <strong>Straße: </strong> <?php echo $row['streetaddress'] . ' ' . $row['streetnumber']; ?><br>
                                    <strong>Postleitzahl: </strong> <?php echo $row['zipcode']; ?><br>
                                    <strong>Ort: </strong> <?php echo $row['city']; ?><br>
                                    <strong>Land: </strong> <?php echo $row['country']; ?><br>
                                    <strong>Email: </strong> <?php echo $row['email']; ?><br>
                                    <strong>Firma: </strong> <?php echo $row['company']; ?>
                                </div>
                            </div>
                        </div>
                        <!-- section contracts -->
                        <div class=" panel panel-default" id="myCollapsible">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Verträge
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <h3 class="h3-spacing">Verträge des Vermittlers</h3>
                                    <table id="agentTable" class="display table table-bordered table-striped table-hover cell-border nowrap">
                                        <thead>
                                            <tr>
                                                <th>Aktenzeichen</th>
                                                <th>Vertrag</th>
                                                <th>Nachname</th>
                                                <th>Vorname</th>
                                                <th>Status</th>
                                                <th>Anlagedatum</th>
                                                <th>in RAM</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                if ($row['refnum'] != "") {
                                                    $date = new DateTime($row['created_at']);
                                                    echo "<tr onclick=\"parent.location='contract.php?id=$row[id]'\" class=$hclass>";
                                                    echo "<td>" . $row['refnum'] . "</td>";
                                                    echo "<td>" . $row['contnum'] . "</td>";
                                                    echo "<td>" . $row['lname'] . "</td>";
                                                    echo "<td>" . $row['fname'] . "</td>";
                                                    echo "<td>" . $row['ms'] . "</td>";
                                                    echo "<td>" . $date->format('Y-m-d H:i:s') . "</td>";
                                                    if ($row['in_ram'] == true) {
                                                        $checked = "<i class='fa fa-check' aria-hidden='true'></i>";
                                                    } else {
                                                        $checked = "";
                                                    }
                                                    echo "<td>" . $checked . "</td>";
                                                    echo " </tr> ";
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
