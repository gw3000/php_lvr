// reset bootstrap classes and msgs when reset is clicked
function resetForm() {
    // remove the class
    let errClass = document.querySelectorAll('.form-group');
    errClass.forEach(function(div) {
        div.classList.remove('has-warning');
    });

    // remove the error msg
    let errSpan = document.querySelectorAll('span.invalid-feedback');
    errSpan.forEach(function(span) {
        span.innerText = '';
    });
}

// change the Date on select new entry
function dateOnChangeToday(destination) {
    document.getElementById(destination).value = new Date()
        .toISOString()
        .slice(0, 10);
}

// change width->form->select
function changeWidth(source) {
    // text inhalt des auswahlmenüs
    let str = document.getElementById(source.id).options[
        document.getElementById(source.id).selectedIndex
    ].text;

    let sourceID = '#' + source.id;
    let n = str.length;
    if (n >= 40 && n < 50) {
        document.querySelector(sourceID).parentNode.className = 'col-lg-5';
    } else if (n >= 50 && n < 60) {
        document.querySelector(sourceID).parentNode.className = 'col-lg-6';
    } else if (n >= 60 && n < 80) {
        document.querySelector(sourceID).parentNode.className = 'col-lg-7';
    } else if (n >= 80) {
        document.querySelector(sourceID).parentNode.className = 'col-lg-8';
    } else {
        document.querySelector(sourceID).parentNode.className = 'col-lg-4';
    }
}

// saves the active tab and collapsable to local storage
$(document).ready(function() {
    const url = window.location.href;
    if (url.includes('contract.php')) {
        // tab
        $(function() {
            //for bootstrap 3 use 'shown.bs.tab' instead of 'shown' in the next line
            $('a[data-toggle="tab"]').on('click', function(e) {
                //save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(e.target).attr('href'));
            });
            //go to the latest tab, if it exists:
            let lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('a[href="' + lastTab + '"]').click();
            }
        });

        // collapse
        $(function() {
            $('a[data-toggle="collapse"]').on('click', function(e) {
                localStorage.setItem('lastCollapse', $(e.target).attr('href'));
            });
            //go to the latest tab, if it exists:
            let lastCollapse = localStorage.getItem('lastCollapse');
            if (lastCollapse) {
                $('a[href="' + lastCollapse + '"]').click();
            }
        });
    } else {
        // clear local storage
        localStorage.setItem('lastCollapse', '');
        localStorage.setItem('lastTab', '');
    }
});

// reset DataTable filter
function delFilter() {
    const site = window.location.pathname;
    const table = site.split('.')[0].substring(1);
    let dtKey = 'DataTables_' + table + 'Table_' + site;
    localStorage.removeItem(dtKey);
    location.reload();
}
