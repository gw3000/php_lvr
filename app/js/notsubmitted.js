document.getElementById('nsModalSave').onclick = function () {
    let id = document.getElementById('contid').value;
    let check_to_delete = document.getElementById('check_to_delete').checked;
    let marked_to_delete = document.getElementById('marked_to_delete').value;
    if (check_to_delete && marked_to_delete == "") {
        let params = '&id=' + id + '&op=save';
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/nseditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            if (this.status == 200) {
                $('#nsDelModal').modal('hide');
                location.reload();
            }
        }
        xhr.send(params);
    } else if (check_to_delete == false) {
        let params = '&id=' + id + '&op=delete';
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/nseditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function () {
            if (this.status == 200) {
                $('#nsDelModal').modal('hide');
                location.reload();
            }
        }
        xhr.send(params);
        $('#nsDelModal').modal('hide');

    } else {
        $('#nsDelModal').modal('hide');
    }
};


function loadDelModal(contid) {
    let xhr = new XMLHttpRequest();
    let params = 'contid=' + contid + '&op=load';
    xhr.open('POST', '../includes/worker/nseditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function () {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('contid').value = val['contid'];
            document.getElementById('contnum').value = val['contnum'];
            document.getElementById('refnum').value = val['refnum'];

            // status
            let status = '';
            if (val['status'] == 'NotAccepted') {
                status = 'abgelehnt';
            } else if (val['status'] == 'Completed') {
                status = 'vollständig';
            } else if (val['status'] == 'Incomplete') {
                status = 'unvollständig';
            } else if (val['status'] == 'Submitted') {
                status = 'eingereicht';
            }
            document.getElementById('status').value = status;

            let nameCustomer = val['fname'] + ' ' + val['lname'];
            document.getElementById('nameCustomer').value = nameCustomer;

            let nameAgent = val['firstname'] + ' ' + val['lastname'];
            document.getElementById('nameAgent').value = nameAgent;

            document.getElementById('company').value = val['company'];
            const mkd = new Date(val['marked_to_delete']);
            if (val['marked_to_delete']) {
                document.getElementById('marked_to_delete').value = mkd;
                document.getElementById('marked_to_delete').parentNode.hidden = false
                document.getElementById('check_to_delete')
                document.querySelector('input[id="check_to_delete"]').checked = true
            } else {
                document.getElementById('marked_to_delete').value = '';
                document.querySelector('input[id="check_to_delete"]').checked = false
            }
        }
    };
    xhr.send(params);
}
