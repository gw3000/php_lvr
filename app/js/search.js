// tooltips
$("#searching :input").tooltip({
    position: "center right",
    offset: [-2, 10]
});

// autocomplete name
$(function() {
    $("#inputName").autocomplete({
        source: $(location).attr('protocol') + '//' + $(location).attr('host') + '/includes/autocomplete.php?autocomplete=name',
        minLength: 4,
    });
});

// autocomplete contnum
$(function() {
    $("#inputContNum").autocomplete({
        source: $(location).attr('protocol') + '//' + $(location).attr('host') + '/includes/autocomplete.php?autocomplete=contnum',
        minLength: 3,
    });
});

// autocomplete refnum
$(function() {
    $("#inputRefNum").autocomplete({
        source: $(location).attr('protocol') + '//' + $(location).attr('host') + '/includes/autocomplete.php?autocomplete=refnum',
        minLength: 3,
    });
});
