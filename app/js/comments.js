// modal functionality for comments
function loadModal(button) {
    let fid = button.id.substring(6);
    loadComment(fid);
}

// load comments
function loadComment(fid) {
    // event.preventDefault();
    let params = 'id=' + window.location.search.substring(4);
    params += '&fid=' + fid;
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/commentsWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('commentModalCenterTitle').lastChild.data =
                'Kommentar und Status (' + fid + ')';
            document.getElementById('commentModalLabelText').value =
                val['comment'];
            document.getElementById('commentModalLabelSelect').value =
                val['status'];
            document.getElementById('lastuserModalLabelSelect').innerText =
                val['last_user'];
            document.getElementById('lastupdateModalLabelSelect').innerText =
                val['last_update'];
        }
    };
    xhr.send(params + '&op=load');
}

// save comments on click
document
    .getElementById('commentModalSave')
    .addEventListener('click', saveComment);

// save comment function
function saveComment() {
    // event.preventDefault();
    let comment = document.getElementById('commentModalLabelText').value;
    let status = document.getElementById('commentModalLabelSelect').value;
    let id = window.location.search.substring(4);
    let fid = document
        .getElementById('commentModalCenterTitle')
        .lastChild.data.slice(22, -1);
    let params =
        'id=' +
        id +
        '&fid=' +
        fid +
        '&comment=' +
        comment +
        '&status=' +
        status;

    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/commentsWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            loadComment(fid);

            // change comment button
            let buttonClass;
            if (val['status'] == 'ungeprüft') {
                buttonClass = 'btn btn-primary btn-sm';
            } else if (val['status'] == 'geprüft') {
                buttonClass = 'btn btn-success btn-sm';
            } else if (val['status'] == 'unklar') {
                buttonClass = 'btn btn-info btn-sm';
            } else if (val['status'] == 'noch nicht bearbeitet') {
                buttonClass = 'btn btn-secondary btn-sm';
            } else if (val['status'] == 'keine gegenteiligen Indizien') {
                buttonClass = 'btn btn-warning btn-sm';
            }

            // button icon
            let buttonIcon;
            if (val['comment'] != '') {
                buttonIcon = '<i class="fas fa-comment-medical"></i>';
            } else {
                buttonIcon = '<i class="fas fa-comment-slash"></i>';
            }
            // set button appearance
            document.getElementById('check_' + fid).className = buttonClass;
            document.getElementById('check_' + fid).innerHTML = buttonIcon;
            document.getElementById('check_' + fid).title =
                'Status: ' + val['status'];
            // commentBadger();
        }
    };

    xhr.send(params + '&op=save');
}

// comments as info in sections
// function commentBadger() {
//   let comm = document.querySelectorAll(".btn-sm");
//   let comm_01 = (comm_02 = comm_03 = comm_04 = comm_05 = [0, 0, 0, 0]);
//   for (i = 0; i < comm.length; i++) {
//     // 01 basisdaten
//     if (comm[i].id.substr(6, 1) == 2) {
//       if (document.getElementById(comm[i].id).classList[1] == "btn-secondary") {
//         comm_01[3] += 1;
//       } else if (
//         document.getElementById(comm[i].id).classList[1] == "btn-primary"
//       ) {
//         comm_01[2] += 1;
//       } else if (
//         document.getElementById(comm[i].id).classList[1] == "btn-success"
//       ) {
//         comm_01[1] += 1;
//       } else if (
//         document.getElementById(comm[i].id).classList[1] == "btn-info"
//       ) {
//         comm_01[0] += 1;
//       }
//       // 02 situationsabfrage
//     } else if (comm[i].id.substr(6, 1) == 3) {
//       console.log(document.getElementById(comm[i].id).classList[1]);
//       // 04 unterlagen
//     } else if (comm[i].id.substr(6, 2) == 13) {
//       console.log(document.getElementById(comm[i].id).classList[1]);
//     } else if (comm[i].id.substr(6, 2) == 10 || comm[i].id.substr(6, 2) == 11) {
//       console.log(document.getElementById(comm[i].id).classList[1]);
//     }
//   }

//   // write info to section tab
//   for (let index = 0; index < comm_01.length; index++) {
//     if (comm_01[index] > 0) {
//       document.getElementById("c1_" + [index]).classList.remove("hide");
//       document.getElementById("c1_" + [index]).innerText = comm_01[index];
//     } else {
//       document.getElementById("c1_" + [index]).classList.add("hide");
//       document.getElementById("c1_" + [index]).innerText = "";
//     }
//   }
// }
// ziel ist

// comments overview in sections
function CommentSection(section) {
    this.section = section;
    this.commAll = document.querySelectorAll('.btn-sm');
    this.comm = [0, 0, 0, 0];
    this.getCommentsSection = function() {};
    // for (let i = 0; i < this.commAll.length; i++) {
    //   if (this.commAll[i].id.substr(6, 1) == 2) {
    //     console.log("aha");
    //   }
    // }
}

const section1 = new CommentSection('1');
// console.log(section1);

const section2 = new CommentSection('2');
// console.log(section2);
