// disable mousewheel on a input number field when in focus
$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});


