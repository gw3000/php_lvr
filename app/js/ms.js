document.getElementById("inputMS").addEventListener("change", function () {
  const id = document.getElementById("inputMS").value;
  let xhr = new XMLHttpRequest();
  let params = "id=" + id + "&op=load";
  xhr.open("POST", "../includes/worker/msLoadText.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onload = function () {
    if (this.status == 200) {
      let val = JSON.parse(this.responseText);
      let ht = val["helptext"];
      let htpf = val["helptext_pf"];
      let htwk = val["helptext_wk"];
      let adl = val["after_deadline"];
      if (ht === null) {
        ht = "kein Eintrag vorhanden";
      }
      if (htpf === null) {
        htpf = "kein Eintrag vorhanden";
      }
      if (htwk === null) {
        htwk = "kein Eintrag vorhanden";
      }
      if (adl === null) {
        adl = "kein Eintrag vorhanden";
      }
      document.getElementById("helptext").firstChild.data = ht;
      document.getElementById("helptext_pf").firstChild.data = htpf;
      document.getElementById("helptext_wk").firstChild.data = htwk;
      document.getElementById("after_deadline").firstChild.data = adl;
    }
  };
  xhr.send(params);
});

// date from string
function getDate(string) {
  let [_, year, month, day] = /(\d{1,4})-(\d{1,2})-(\d{2})/.exec(string);
  return new Date(year, month - 1, day);
}

// deadline worker date difference
$(document).ready(function () {
  const dline = Number(document.getElementById("deadlineVal").value);
  if (dline && dline != 0) {
    document.getElementById("deadlineInput").value = dline;
    let dl = document.getElementById("deadlineForm");
    if (dline <= 0) {
      document.getElementById("deadlineForm").classList.add("has-error");
    }
    dl.classList.remove("hidden");
  }
});


// show "ruecknahme"
if (document.getElementsByName('source__t_cont_vd__17')[0]) {
  let vd_17 = document.getElementsByName('source__t_cont_vd__17')[0].checked;
  if (vd_17 == true) {
    $('.dismissal').show();
  }
}
