document.getElementById('userAddModalSave').onclick = function() {
    let user = document.getElementById('username');
    let email = document.getElementById('email');
    let pwd1 = document.getElementById('pwd1');
    let pwd2 = document.getElementById('pwd2');
    let role = document.getElementById('role');
    let err = 0;
    //check if inupt fields are empty
    if (user.value == '') {
        user.parentElement.className = 'form-group has-error';
        user.parentElement.firstElementChild.innerText =
            'Bitte einen Benutzernamen eingeben!';
        err++;
    } else if (user.value.length < 4) {
        user.parentElement.className = 'form-group has-error';
        user.parentElement.firstElementChild.innerText =
            'Benutername (Vorname Nachname) ... sollte länger als 3 Zeichen sein';
        err++;
    }
    if (email.value == '') {
        email.parentElement.className = 'form-group has-error';
        email.parentElement.firstElementChild.innerText =
            'Bitte eine Email Adresse eingeben!';
        err++;
    }
    if (pwd1.value == '') {
        pwd1.parentElement.className = 'form-group has-error';
        pwd1.parentElement.firstElementChild.innerText =
            'Passwort ... Bitte ein Passwort eingeben!';
        err++;
    } else if (pwd1.value.length < 6) {
        pwd1.parentElement.className = 'form-group has-error';
        pwd1.parentElement.firstElementChild.innerText =
            'Passwort ... sollte länger als 6 Zeichen sein!';
        err++;
    }
    if (pwd2.value == '') {
        pwd2.parentElement.className = 'form-group has-error';
        pwd2.previousSibling.previousSibling.textContent =
            'Bitte das Passwort wiederholen!';
        err++;
    }
    if (pwd1.value != pwd2.value) {
        pwd1.parentElement.className = 'form-group has-error';
        pwd1.parentElement.firstElementChild.innerText = 'beide Passwörter';
        pwd2.parentElement.className = 'form-group has-error';
        pwd2.previousSibling.previousSibling.textContent =
            '... müssen übereinstimmen';
        err++;
    }

    //speichern wenn keine Eingabefehler
    if (err == 0) {
        let role = document.getElementById('role').value;
        let params =
            'name=' +
            user.value +
            '&email=' +
            email.value +
            '&password=' +
            pwd1.value +
            '&role=' +
            role;
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/accountWorker.php', true);
        xhr.setRequestHeader(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );
        xhr.onload = function() {
            if (this.status == 200) {
                let val = this.responseText;
                if (val == 'password_exist') {
                    email.parentElement.className = 'form-group has-error';
                    email.parentElement.firstElementChild.innerText =
                        'Email bereits vergeben!';
                } else {
                    $('#userAddModal').modal('hide');
                    location.reload();
                }
            }
        };
        xhr.send(params + '&op=save');
    }
};

//reset errors
let user = document.getElementById('username');
user.onkeydown = function() {
    user.parentElement.className = 'form-group';
    user.parentElement.firstElementChild.innerText =
        'Benutername (Vorname Nachname)';
};

let email = document.getElementById('email');
email.onchange = function() {
    email.parentElement.className = 'form-group';
    email.parentElement.firstElementChild.innerText = 'Email Adresse';
};

let pwd1 = document.getElementById('pwd1');
let pwd2;
pwd1.onclick = function() {
    pwd1.parentElement.className = 'form-group';
    pwd1.parentElement.firstElementChild.innerText = 'Passwort';
    pwd1.value = pwd2.value = '';
    pwd2.parentElement.className = 'form-group';
    pwd2.previousSibling.previousSibling.textContent = 'Passwort wiederholen!';
};

// loadModModal
function loadModModal(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/accountWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('userIdMod').value = id;
            document.getElementById('usernameMod').value = val['name'];
            document.getElementById('emailMod').value = val['email'];
            document.getElementById('roleMod').value = val['role'];
        }
    };
    xhr.send(params);
}

// reset password errors in userModModal
document.getElementById('userModPwd1').onclick = function() {
    document.getElementById('userModPwd1').parentElement.className =
        'form-group';
    document.getElementById(
        'userModPwd1'
    ).parentElement.firstElementChild.innerText = 'Passwort';
    document.getElementById('userModPwd1').value = document.getElementById(
        'userModPwd2'
    ).value = '';
    document.getElementById('userModPwd2').parentElement.className =
        'form-group';
    document.getElementById(
        'userModPwd2'
    ).previousSibling.previousSibling.textContent = 'Passwort wiederholen!';
};

// updateModModal
document.getElementById('userModModalSave').onclick = function() {
    let id = document.getElementById('userIdMod').value;
    let user = document.getElementById('usernameMod').value;
    let role = document.getElementById('roleMod').value;
    let pwd1 = document.getElementById('userModPwd1').value;
    let pwd2 = document.getElementById('userModPwd2').value;
    if (pwd1 != pwd2) {
        document.getElementById('userModPwd1').parentElement.className =
            'form-group has-error';
        document.getElementById('userModPwd2').parentElement.className =
            'form-group has-error';
        document.getElementById(
            'userModPwd1'
        ).parentElement.firstElementChild.innerText = 'Passwörter ...';
        document.getElementById(
            'userModPwd2'
        ).previousSibling.previousSibling.textContent =
            '... müssen übereinstimmer!';
    } else if (pwd1 && pwd1.length < 6) {
        document.getElementById('userModPwd1').parentElement.className =
            'form-group has-error';
        document.getElementById('userModPwd2').parentElement.className =
            'form-group has-error';
        document.getElementById(
            'userModPwd1'
        ).parentElement.firstElementChild.innerText = 'Passwörter müssen ...';
        document.getElementById(
            'userModPwd2'
        ).previousSibling.previousSibling.textContent =
            '... mehr als 6 Zeichen haben!';
    } else {
        let params =
            'id=' +
            id +
            '&name=' +
            user +
            '&password=' +
            pwd1 +
            '&role=' +
            role;
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/accountWorker.php', true);
        xhr.setRequestHeader(
            'Content-Type',
            'application/x-www-form-urlencoded'
        );
        xhr.onload = function() {
            if (this.status == 200) {
                $('#userModModal').modal('hide');
                location.reload();
            }
        };
        xhr.send(params + '&op=update');
    }
};

//show and hide passwort ändern
document
    .getElementById('showPwdChange')
    .addEventListener('click', showPwdChange);

// hide on load
function hidePwdChange() {
    document.getElementById('pwdChange').style.display = 'none';
}

// toggle hide and show
function showPwdChange() {
    if (document.getElementById('pwdChange').style.display == 'none') {
        $('#pwdChange').show('fade');
    } else {
        if (
            document.getElementById('userModPwd1').value == '' &&
            document.getElementById('userModPwd2').value == ''
        ) {
            $('#pwdChange').hide('fade');
            document.getElementById('userModPwd1').parentElement.className =
                'form-group';
            document.getElementById('userModPwd2').parentElement.className =
                'form-group';
            document.getElementById(
                'userModPwd1'
            ).parentElement.firstElementChild.innerText = 'neues Passwort';
            document.getElementById(
                'userModPwd2'
            ).previousSibling.previousSibling.textContent =
                'neues Passwort wiederholen';
        } else {
            document.getElementById('userModPwd1').parentElement.className =
                'form-group has-error';
            document.getElementById('userModPwd2').parentElement.className =
                'form-group has-error';
            document.getElementById(
                'userModPwd1'
            ).parentElement.firstElementChild.innerText =
                'Erst die Formularfelder löschen ...';
            document.getElementById(
                'userModPwd2'
            ).previousSibling.previousSibling.textContent =
                '... dann ist das Ausblenden möglich!';
        }
    }
}

// delete user
document.getElementById('UserDelete').addEventListener('click', UserDelete);

function loadDelModal(id) {
    document.getElementById('userIdDel').value = id;
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/accountWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('usernameDel').innerHTML =
                'Nutzname: <strong>' + val['name'] + '</strong><br>';
            document.getElementById('emailDel').innerHTML =
                'Email: <strong>' + val['email'] + '</strong>';
        }
    };
    xhr.send(params);
}

function UserDelete() {
    let id = document.getElementById('userIdDel').value;
    let params = 'id=' + id;
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/accountWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            $('#userDelModal').modal('hide');
            location.reload();
        }
    };
    xhr.send(params + '&op=delete');
}
