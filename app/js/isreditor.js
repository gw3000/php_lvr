// add insurance functionality
document.getElementById('isrModalAdd').onclick = function() {
  let opponent = document.getElementById('opponentAdd');
  let selection = document.getElementById('selectionAdd');
  let presentation = document.getElementById('presentationAdd');
  let country = document.getElementById('countryAdd');
  let idram = document.getElementById('idRamAdd');
  let err = 0;

  //check if input fields are empty
  if (selection.value == '') {
    selection.parentElement.className = 'form-group has-error';
    selection.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Versicherer bei Auswahl</strong> eingeben!';
    err++;
  }

  if (presentation.value == '') {
    presentation.parentElement.className = 'form-group has-error';
    presentation.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Versicherer bei Vertragsabschluss</strong> eingeben!';
    err++;
  }

  if (opponent.value == '') {
    opponent.parentElement.className = 'form-group has-error';
    opponent.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Gegner</strong> eingeben!';
    err++;
  }

  if (idram.value == '') {
    idram.parentElement.className = 'form-group has-error';
    idram.parentElement.firstElementChild.innerHTML =
      'Bitte eine gülitge <strong>Ra-Micro Adressnummmer</strong> eingeben!';
    err++;
  }

  //speichern wenn keine Eingabefehler
  if (err == 0) {
    let params =
      '&selection=' + selection.value +
      '&presentation=' + presentation.value +
      '&opponent=' + opponent.value +
      '&country=' + country.value +
      '&idram=' + idram.value +
      '&op=save';
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/isreditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      if (this.status == 200) {
        $('#isrAddModal').modal('hide');
        // empty input fields
        document.getElementById('opponentAdd').value="";
        document.getElementById('selectionAdd').value="";
        document.getElementById('presentationAdd').value="";
        document.getElementById('countryAdd').value="";
        document.getElementById('idRamAdd').value="";
        location.reload();
      }
    };
    xhr.send(params);
  }
};

//++++++++++++++++++++
// reset errors
//++++++++++++++++++++
let selAdd = document.getElementById('selectionAdd');
selAdd.onkeydown = function() {
  selAdd.parentElement.className = 'form-group';
  selAdd.parentElement.firstElementChild.innerText =
    'Versicherer (Auswahlmenü)';
};

let presAdd = document.getElementById('presentationAdd');
presAdd.onkeydown = function() {
  presAdd.parentElement.className = 'form-group';
  presAdd.parentElement.firstElementChild.innerText =
    'Bezeichnung bei Darstellung';
};

let presOpp = document.getElementById('opponentAdd');
presOpp.onkeydown = function() {
  presOpp.parentElement.className = 'form-group';
  presOpp.parentElement.firstElementChild.innerText = 'Gegner';
};

let idramAdd = document.getElementById('idRamAdd');
idramAdd.onkeydown = function() {
  idramAdd.parentElement.className = 'form-group';
  idramAdd.parentElement.firstElementChild.innerText = 'RaMicro Adressnummer';
};

let selMod = document.getElementById('selectionMod');
selMod.onkeydown = function() {
  selMod.parentElement.className = 'form-group';
  selMod.parentElement.firstElementChild.innerText = 'Bezeichnung bei Auswahl';
};

let presMod = document.getElementById('presentationMod');
presMod.onkeydown = function() {
  presMod.parentElement.className = 'form-group';
  presMod.parentElement.firstElementChild.innerText =
    'Bezeichnung bei Darstellung';
};
//++++++++++++++++++++
// reset errors
//++++++++++++++++++++

// loadModModal
function loadModModal(id) {
  let xhr = new XMLHttpRequest();
  let params = 'id=' + id + '&op=load';
  xhr.open('POST', '../includes/worker/isreditorWorker.php', true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
    if (this.status == 200) {
      let val = JSON.parse(this.responseText);
      document.getElementById('isrIdMod').value = id;
      document.getElementById('selectionMod').value = val['selection'];
      document.getElementById('presentationMod').value = val['presentation'];
      document.getElementById('opponentMod').value = val['opponent'];
      document.getElementById('countryMod').innerHTML = val['country'];
      document.getElementById('idRamMod').value = val['id_ram'];
    }
  };
  xhr.send(params);
}

// updateModModal
document.getElementById('isrModModalSave').onclick = function() {
  const mod_id = document.getElementById('isrIdMod');
  const mod_selection = document.getElementById('selectionMod');
  const mod_presentation = document.getElementById('presentationMod');
  const mod_opponent = document.getElementById('opponentMod');
  const mod_country = document.getElementById('countryModSel');
  const mod_idram = document.getElementById('idRamMod');
  let err = 0;
  //check if input fields are empty
  if (mod_selection.value == '') {
    mod_selection.parentElement.className = 'form-group has-error';
    mod_selection.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Versicherer (Auswahlmenü)</strong> eingeben!';
    err++;
  }

  if (mod_presentation.value == '') {
    mod_presentation.parentElement.className = 'form-group has-error';
    mod_presentation.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Versicherer bei Vertragsabschluss</strong> eingeben!';
    err++;
  }

  if (mod_opponent.value == '') {
    mod_opponent.parentElement.className = 'form-group has-error';
    mod_opponent.parentElement.firstElementChild.innerHTML =
      'Bitte einen <strong>Gegner</strong> eingeben!';
    err++;
  }

  if (err == 0) {
    let params =
      'mod_id=' + mod_id.value +
      '&mod_selection=' + encodeURIComponent(mod_selection.value) +
      '&mod_presentation=' + encodeURIComponent(mod_presentation.value) +
      '&mod_opponent=' + encodeURIComponent(mod_opponent.value) +
      '&mod_country=' + encodeURIComponent(mod_country.value) +
      '&mod_idram=' + encodeURIComponent(mod_idram.value);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/isreditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
      if (this.status == 200) {
        $('#isrModModal').modal('hide');
        location.reload();
      }
    };
    xhr.send(params + '&op=update');
  }
};

// delete status
document.getElementById('isrDelete').addEventListener('click', msDelete);

function loadDelModal(id) {
  let xhr = new XMLHttpRequest();
  let params = 'id=' + id + '&op=load';
  xhr.open('POST', '../includes/worker/isreditorWorker.php', true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
    if (this.status == 200) {
      let val = JSON.parse(this.responseText);
      document.getElementById('isrIdDel').value = val['id'];
      document.getElementById('selectionDel').innerHTML =
        '<hr>Auswahl: <strong>' + val['selection'] + '</strong><br><hr>';
      document.getElementById('presentationDel').innerHTML =
        'Darstellung: <strong>' + val['presentation'] + '</strong><br><hr>';
      if (val['id_ram'] == null) {
        val['id_ram'] = 'nicht vergeben';
      }
      document.getElementById('opponentDel').innerHTML =
        'Gegner: <strong>' + val['opponent'] + '</strong><br><hr>';
      document.getElementById('idRamDel').innerHTML =
        'Ra-Micro Adressnummer: <strong>' + val['id_ram'] + '</strong><br>';
    }
  };
  xhr.send(params);
}

function msDelete() {
  let id = document.getElementById('isrIdDel').value;
  let params = 'id=' + id;
  let xhr = new XMLHttpRequest();
  xhr.open('POST', '../includes/worker/isreditorWorker.php', true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
    if (this.status == 200) {
      $('#isrDelModal').modal('hide');
      location.reload();
    }
  };
  xhr.send(params + '&op=delete');
}
