let xhr = new XMLHttpRequest();
let params = '&op=uploadHistory';
let dataset1 = [];

xhr.open('POST', '../includes/worker/statisticsWorker.php', true);
xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
xhr.onload = function () {
    if (this.status == 200) {
        let val = JSON.parse(this.response);
        for (var i = 0, len = val.length; i < len; i++) {
            dataset1.push(val[i].count);
        }
        const ctx = document.getElementById('uploadHistory').getContext('2d');
        let myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['1 Tag', '7 Tage', '30 Tage', '90 Tage', '365 Tage'],
                datasets: [{
                    label: 'Anzahl der Verträge',
                    data: dataset1,
                    backgroundColor: [
                        'rgba(145, 168, 192, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderColor: [
                        'rgba(145, 168, 192, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                    ],
                    borderWidth: 1,
                },],
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                    },],
                },
                legend: {
                    display: false,
                }
            },
        });
    }
};
xhr.send(params);

// xhr = new XMLHttpRequest();
// params = '&op=uploadUserHistory';
// let dataset2 = [];
// xhr.open('POST', '../includes/worker/statisticsWorker.php', true);
// xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
// xhr.onload = function () {
//     if (this.status == 200) {
//         val = JSON.parse(this.response);
//         for (var i = 0, len = val.length; i < len; i++) {
//             dataset2.push(val[i].counts);
//         }
//         const ctx = document.getElementById('uploadUserHistory').getContext('2d');
//         myChart = new Chart(ctx, {
//             type: 'bar',
//             data: {
//                 labels: [val[0]['agent'], val[1]['agent'], val[2]['agent'], val[3]['agent'], val[4]['agent'],],
//                 datasets: [{
//                     data: dataset2,
//                     backgroundColor: [
//                         'rgba(255, 99, 132, 0.2)',
//                         'rgba(54, 162, 235, 0.2)',
//                         'rgba(255, 206, 86, 0.2)',
//                         'rgba(75, 192, 192, 0.2)',
//                         'rgba(145, 168, 192, 0.2)',
//                     ],
//                     borderColor: [
//                         'rgba(255, 99, 132, 1)',
//                         'rgba(54, 162, 235, 1)',
//                         'rgba(255, 206, 86, 1)',
//                         'rgba(75, 192, 192, 1)',
//                         'rgba(145, 168, 192, 1)',
//                     ],
//                     borderWidth: 1,
//                 },],
//             },
//             options: {
//                 scales: {
//                     yAxes: [{
//                         ticks: {
//                             beginAtZero: true,
//                         },
//                     },],
//                 },
//                 legend: {
//                     display: false,
//                 }
//             },
//         });
//     }
// };
// xhr.send(params);

// // ###############
// //  UPLOAD PER DAY
// // ###############

// xhr = new XMLHttpRequest();
// params = '&op=uploadPerDay';
// let labelsUPD = [];
// let dataUPD = [];
// xhr.open('POST', '../includes/worker/statisticsWorker.php', true);
// xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

// xhr.onload = function () {
//     if (this.status == 200) {
//         val = JSON.parse(this.response);

//         for (var i = 0, len = val.length; i < len; i++) {
//             labelsUPD.push(val[i].date);
//             dataUPD.push(val[i].counts);
//         }

//         for (let index = 0; index < labelsUPD.length; index++) {
//             dt0 = new Date(labelsUPD[index]);
//             dt1 = new Date();
//             dtDiff = parseInt((dt1.getTime() - dt0.getTime()) / (24 * 3600 * 1000));
//             if (dtDiff >= 1) {
//                 dt1 = addDays(dt0, 1);
//                 labelsUPD.splice(index + 1, 0, dt1)
//                 dataUPD.splice(index + 1, 0, 0)
//             }
//         }

//         function addDays(date, days) {
//             var result = new Date(date);
//             result.setDate(result.getDate() + days);
//             return result.toISOString().slice(0, 10);
//         }

//         const ctx = document.getElementById('uploadPerDay').getContext('2d');
//         myChart = new Chart(ctx, {
//             type: 'line',
//             data: {
//                 "labels": labelsUPD,
//                 "datasets": [{
//                     "label": "Verträge pro Tag",
//                     "data": dataUPD,
//                     "fill": false,
//                     "borderColor": "rgb(75, 192, 192)",
//                     "lineTension": 0.5,
//                     "steppedLine": false,
//                 }]
//             },
//             options: {
//                 scales: {
//                     yAxes: [{
//                         stacked: true,
//                         ticks: {
//                             stepSize: 1.0
//                         }

//                     }]
//                 },
//                 legend: {
//                     display: false,
//                 }
//             },
//         });
//     }
// };
// xhr.send(params);

// xhr = new XMLHttpRequest();
// params = '&op=isr';
// let dataset3 = [];
// xhr.open('POST', '../includes/worker/statisticsWorker.php', true);
// xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
// xhr.onload = function () {
//     if (this.status == 200) {
//         val = JSON.parse(this.response);
//         for (var i = 0, len = val.length; i < len; i++) {
//             dataset3.push(val[i].counts);
//         }
//         const ctx = document.getElementById('TopFiveInsurances').getContext('2d');
//         myChart = new Chart(ctx, {
//             type: 'bar',
//             data: {
//                 labels: [val[0]['presentation'], val[1]['presentation'], val[2]['presentation'], val[3]['presentation'], val[4]['presentation'],],
//                 datasets: [{
//                     data: dataset3,
//                     backgroundColor: [
//                         'rgba(255, 99, 132, 0.2)',
//                         'rgba(54, 162, 235, 0.2)',
//                         'rgba(255, 206, 86, 0.2)',
//                         'rgba(75, 192, 192, 0.2)',
//                         'rgba(145, 168, 192, 0.2)',
//                     ],
//                     borderColor: [
//                         'rgba(255, 99, 132, 1)',
//                         'rgba(54, 162, 235, 1)',
//                         'rgba(255, 206, 86, 1)',
//                         'rgba(75, 192, 192, 1)',
//                         'rgba(145, 168, 192, 1)',
//                     ],
//                     borderWidth: 1,
//                 },],
//             },
//             options: {
//                 scales: {
//                     yAxes: [{
//                         ticks: {
//                             beginAtZero: true,
//                         },
//                     },],
//                 },
//                 legend: {
//                     display: false,
//                 }
//             },
//         });
//     }
// };
// xhr.send(params);
