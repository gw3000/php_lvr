document.getElementById('rsModalSave').onclick = function() {
    let id = document.getElementById('id');
    let description = document.getElementById('description');
    let helptext = document.getElementById('helptext');
    let err = 0;

    //check if input fields are empty
    if (description.value == '') {
        description.parentElement.className = 'form-group has-error';
        description.parentElement.firstElementChild.innerText =
            'Bitte einen Status eingeben!';
        err++;
    }

    if (helptext.value == '') {
        helptext.parentElement.className = 'form-group has-warning';
        helptext.parentElement.firstElementChild.innerText =
            'Bitte Hilfetext eingeben! (optional)';
    }

    //speichern wenn keine Eingabefehler
    if (err == 0) {
        let params = '&id=' + id.value +
            '&description=' + description.value + '&helptext=' + helptext.value;
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/rseditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (this.status == 200) {
                let val = this.responseText;
                if (val == 'id_exist') {
                    id.parentElement.className = 'form-group has-error';
                    id.parentElement.firstElementChild.innerText = 'ID bereits vergeben!';
                } else {
                    $('#msAddModal').modal('hide');
                    location.reload();
                }
            }
        };
        xhr.send(params + '&op=save');
    }
};

//reset errors
let addstatus = document.getElementById('description');
addstatus.onkeydown = function() {
    addstatus.parentElement.className = 'form-group';
    addstatus.parentElement.firstElementChild.innerText = 'Status';
};
let addHelpText = document.getElementById('helptext');
addHelpText.onkeydown = function() {
    addHelpText.parentElement.className = 'form-group';
    addHelpText.parentElement.firstElementChild.innerText = 'Hilfetext';
};
let modstatus = document.getElementById('mod_description');
modstatus.onkeydown = function() {
    modstatus.parentElement.className = 'form-group';
    modstatus.parentElement.firstElementChild.innerText = 'Status';
};
let modHelpText = document.getElementById('mod_helptext');
modHelpText.onkeydown = function() {
    modHelpText.parentElement.className = 'form-group';
    modHelpText.parentElement.firstElementChild.innerText = 'Hilfetext';
};

// loadModModal
function loadModModal(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/rseditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('mod_id').value = id;
            document.getElementById('mod_description').value = val['description'];
            document.getElementById('mod_helptext').value = val['helptext'];
        }
    };
    xhr.send(params);
}

// updateModModal
document.getElementById('rsModModalSave').onclick = function() {
    const mod_id = document.getElementById('mod_id');
    const mod_description =  document.getElementById('mod_description');
    const mod_helptext = document.getElementById('mod_helptext');
    let err = 0;
    //check if input fields are empty
    if (mod_description.value == '') {
        mod_description.parentElement.className = 'form-group has-error';
        mod_description.parentElement.firstElementChild.innerText =
            'Bitte einen Status eingeben!';
        err++;
    }

    if (mod_helptext.value == '') {
        mod_helptext.parentElement.className = 'form-group has-warning';
        mod_helptext.parentElement.firstElementChild.innerText =
            'Bitte Hilfetext eingeben! (optional)';
    }
    if (err == 0) {
        let params =
            'mod_id=' +
            mod_id.value +
            '&mod_description=' +
            encodeURIComponent(mod_description.value) +
            '&mod_helptext=' +
            encodeURIComponent(mod_helptext.value);
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/rseditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (this.status == 200) {
                $('#rsModModal').modal('hide');
                location.reload();
            }
        };
        xhr.send(params + '&op=update');
    }
};

// delete status
document.getElementById('rsDelete').addEventListener('click', msDelete);

function loadDelModal(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/rseditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('rsIdDel').value = val['id'];
            document.getElementById('del_id').innerHTML =
                'ID: <strong>' + val['id'] + '</strong><br>';
            document.getElementById('del_description').innerHTML =
                'Status: <strong>' + val['description'] + '</strong>';
        }
    };
    xhr.send(params);
}

function msDelete() {
    let id = document.getElementById('rsIdDel').value;
    let params = 'id=' + id;
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/rseditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            $('#userDelModal').modal('hide');
            location.reload();
        }
    };
    xhr.send(params + '&op=delete');
}
