document.getElementById('dsmModalSave').onclick = function() {
    let id = document.getElementById('id');
    let description = document.getElementById('description');
    let helptext = document.getElementById('helptext');
    let helptext_pf = document.getElementById('helptext_pf');
    let helptext_wk = document.getElementById('helptext_wk');
    let deadline = document.getElementById('deadline');
    let after_deadline = document.getElementById('after_deadline');
    let err = 0;

    //check if inupt fields are empty
    if (id.value == '') {
        id.parentElement.className = 'form-group has-error';
        id.parentElement.firstElementChild.innerText = 'Bitte eine ID eingeben!';
        err++;
    }

    if (description.value == '') {
        description.parentElement.className = 'form-group has-error';
        description.parentElement.firstElementChild.innerText =
            'Bitte einen Status eingeben!';
        err++;
    }

    if (helptext.value == '') {
        helptext.parentElement.className = 'form-group has-warning';
        helptext.parentElement.firstElementChild.innerText =
            'Bitte Hilfetext eingeben! (optional)';
    }

    if (helptext_pf.value == '') {
        helptext_pf.parentElement.className = 'form-group has-warning';
        helptext_pf.parentElement.firstElementChild.innerText =
            'Bitte Hilfetext für die Plattform eingeben! (optional)';
    }

    //speichern wenn keine Eingabefehler
    if (err == 0) {
        let params =
            'id=' + id.value + '&description=' + description.value +
            '&helptext=' + helptext.value + '&helptext_pf=' +
            helptext_pf.value + '&deadline=' + deadline.value +
            '&after_deadline=' + after_deadline.value +
            '&helptext_wk=' + helptext_wk.value;
        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (this.status == 200) {
                let val = this.responseText;
                if (val == 'id_exist') {
                    id.parentElement.className = 'form-group has-error';
                    id.parentElement.firstElementChild.innerText = 'ID bereits vergeben!';
                } else {
                    $('#dsmAddModal').modal('hide');
                    location.reload();
                }
            }
        };
        xhr.send(params + '&op=save');
    }
};

//reset errors
let addid = document.getElementById('id');
addid.onkeydown = function() {
    addid.parentElement.className = 'form-group';
    addid.parentElement.firstElementChild.innerText = 'Status ID';
};

let addstatus = document.getElementById('description');
addstatus.onkeydown = function() {
    addstatus.parentElement.className = 'form-group';
    addstatus.parentElement.firstElementChild.innerText = 'Status';
};

let addHelpText = document.getElementById('helptext');
addHelpText.onkeydown = function() {
    addHelpText.parentElement.className = 'form-group';
    addHelpText.parentElement.firstElementChild.innerText = 'Hilfetext';
};

let addHelpTextPF = document.getElementById('helptext_pf');
addHelpTextPF.onkeydown = function() {
    addHelpTextPF.parentElement.className = 'form-group';
    addHelpTextPF.parentElement.firstElementChild.innerText = 'Hilfetext';
};

// update status
document.getElementById('dsmModModalSave').onclick = function() {
    const mod_id = document.getElementById('mod_id').value;
    const mod_description = encodeURIComponent(
        document.getElementById('mod_description').value
    );
    const mod_helptext = encodeURIComponent(
        document.getElementById('mod_helptext').value
    );
    const mod_helptext_pf = encodeURIComponent(
        document.getElementById('mod_helptext_pf').value
    );
    const mod_helptext_wk = encodeURIComponent(
        document.getElementById('mod_helptext_wk').value
    );
    const mod_deadline = encodeURIComponent(
        document.getElementById('mod_deadline').value
    );
    const mod_after_deadline = encodeURIComponent(
        document.getElementById('mod_after_deadline').value
    );
    let params =
        'mod_id=' +
        mod_id +
        '&mod_description=' +
        mod_description +
        '&mod_helptext=' +
        mod_helptext +
        '&mod_helptext_pf=' +
        mod_helptext_pf +
        '&mod_helptext_wk=' +
        mod_helptext_wk +
        '&mod_deadline=' +
        mod_deadline +
        '&mod_after_deadline=' +
        mod_after_deadline;
    console.log(params);
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            $('#dsmModModal').modal('hide');
            location.reload();
        }
    };
    xhr.send(params + '&op=update');
};

// delete status
document.getElementById('dsmDelete').addEventListener('click', msDelete);

function loadModModal(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    // document.getElementById('mod_helptext').parentNode.className = 'form-group';
    // document.getElementById('mod_helptext_pf').parentNode.className =
        // 'form-group';
    // document.getElementById('mod_helptext_wk').parentNode.className =
        // 'form-group';
    // document.getElementById('mod_deadline').parentNode.className = 'form-group';
    // document.getElementById(
        // 'mod_after_deadline'
    // ).parentNode.parentNode.className = 'form-group';

    xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.response);
            document.getElementById('mod_id').value = id;
            document.getElementById('mod_description').value = val['description'];
            document.getElementById('mod_helptext').value = val['helptext'];
            document.getElementById('mod_helptext_pf').value = val['helptext_pf'];
            document.getElementById('mod_helptext_wk').value = val['helptext_wk'];
            document.getElementById('mod_deadline').value = val['deadline'];
            document.getElementById('mod_after_deadline').value = val['after_deadline'];
        }
    };
    xhr.send(params);
}

function loadDelModal(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('dsmIdDel').value = val['id'];
            document.getElementById('del_id').innerHTML =
                'ID: <strong>' + val['id'] + '</strong><br>';
            document.getElementById('del_description').innerHTML =
                'Status: <strong>' + val['description'] + '</strong>';
        }
    };
    xhr.send(params);
}

function msDelete() {
    let id_dismissal = document.getElementById('dsmIdDel').value;
    let params = 'id_dismissal=' + id_dismissal  + '&op=delete';
    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            $('#userDelModal').modal('hide');
            location.reload();
        }
    };
    xhr.send(params);
    console.log(params);
}

function loadUserModal(id) {
    // empy selection list
    document.getElementById('mod_add_worker').innerHTML = '';

    // empty unorderd list
    document.getElementById('mod_show_worker').innerHTML = '';

    document.getElementById('dsmUserModalCenterTitle').innerHTML =
        'Bearbeiter des Mandatsstatus <small id="dsmID">' +
        encodeURIComponent(id) +
        '</small>';
    // user selection list
    let xhr = new XMLHttpRequest();
    let params = 'op=usersSelection';
    xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.response);
            let select = document.getElementById('mod_add_worker');
            val.forEach(element => {
                var opt = document.createElement('option');
                opt.value = element['id'];
                opt.innerHTML = element['name'];
                select.appendChild(opt);
            });
        }
    };
    xhr.send(params);

    // ul -> li -> user
    params = 'id=' + id + '&op=usersList';
    var oReq = new XMLHttpRequest();
    oReq.open('POST', '../includes/worker/dsmeditorWorker.php', true);
    oReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    oReq.onload = function() {
        if (this.status == 200) {
            let valList = JSON.parse(this.response);
            let uoList = document.getElementById('mod_show_worker');
            for (let index = 0; index < valList.length; index++) {
                let listElement = document.createElement('li');
                listElement.appendChild(
                    document.createTextNode(valList[index]['name'])
                );
                uoList.appendChild(listElement);
            }
        }
    };
    oReq.send(params);
}

function addWorker() {
    let items = document.getElementById('mod_show_worker').children;
    let selName = $('#mod_add_worker option:selected').text();
    let counter = 0;
    for (let index = 0; index < items.length; index++) {
        const addedName = items[index].innerText;
        if (selName == addedName) {
            counter += 1;
        }
    }
    if (counter == 0) {
        // write it to the database
        const sel = document.getElementById('mod_add_worker');
        const opt = sel.options[sel.selectedIndex];
        const userID = opt.value;
        const id_dismissal = document.getElementById('dsmID').innerText;
        const params = 'userID=' + userID + '&id_dismissal=' + id_dismissal + '&op=userAdd';

        let xhr = new XMLHttpRequest();
        xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onload = function() {
            if (this.status == 200) {
                $(
                    '<li>' + $('#mod_add_worker option:selected').text() + '</li>'
                ).appendTo('#mod_show_worker');
            }
        };
        xhr.send(params);
    }
}

function delWorker() {
    const items = document.getElementById('mod_show_worker').children;
    const sel = document.getElementById('mod_add_worker');
    const opt = sel.options[sel.selectedIndex];
    const userID = opt.value;
    const userName = opt.text;
    const id_dismissal = document.getElementById('dsmID').innerText;
    const params = 'userID=' + userID + '&id_dismissal=' + id_dismissal + '&op=userDel';

    for (let index = 0; index < items.length; index++) {
        if (userName == items[index].innerText) {
            let xhr = new XMLHttpRequest();
            xhr.open('POST', '../includes/worker/dsmeditorWorker.php', true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.onload = function() {
                if (this.status == 200) {
                    document.getElementById('mod_show_worker').children[index].remove();
                }
            };
            xhr.send(params);
        }
    }
}

function breakUpUser() {
    location.reload();
}

document.getElementById('addWorker').addEventListener('click', addWorker);
document.getElementById('delWorker').addEventListener('click', delWorker);
document.getElementById('breakUpUser').addEventListener('click', breakUpUser);
