// // loadModModal
function loadraidEditor(id) {
    let xhr = new XMLHttpRequest();
    let params = 'id=' + id + '&op=load';
    xhr.open('POST', '../includes/worker/raieditorWorker.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onload = function() {
        if (this.status == 200) {
            let val = JSON.parse(this.responseText);
            document.getElementById('rsIdMod').value = val['id'];
            document.getElementById('mod_id').value = val['iadressnummer'];
        }
    };
    xhr.send(params);
    console.log(id)
}