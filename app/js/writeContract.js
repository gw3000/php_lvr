document
  .getElementById("writeContract")
  .addEventListener("click", writeContract);

function getInputFields(type) {
  let params = '';
  if (type === "hidden" || type === "checkbox" || type === "text" || type === "date" || type === "number" || type === "radio") {
    queryType = 'input[type="' + type + '"]';
  } else if (type === "textarea" || type === "select") {
    queryType = type;
  }
  const inpFields = document.querySelectorAll(queryType);
  for (const inpField of inpFields) {
    if (inpField.name.includes("target__t") && (!inpField.disabled)) {
      if ((type === 'radio' && inpField.checked) || (type === 'checkbox' && inpField.checked)) {
        params += inpField.name + '="' + inpField.value + '"&';
      } else if (type === 'select') {
        params += inpField.name + '="' + inpField.selectedOptions[0].value + '"&';
      } else if (type != 'radio' && type != 'checkbox') {
        params += inpField.name + '="' + inpField.value + '"&';
      }
    }
  }
  return params;
}

function getParams() {
  let params = ""
  const selectText = getInputFields("text");
  const selectFields = getInputFields("select");
  const selectDates = getInputFields("date");
  const selectNumbers = getInputFields("number");
  const selectAreas = getInputFields("textarea");
  const btnRadio = getInputFields("radio");
  const btnCheckboxHidden = getInputFields("hidden");
  const btnCheckbox = getInputFields("checkbox");

  params =
    selectText +
    selectFields +
    selectDates +
    selectNumbers +
    selectAreas +
    btnRadio +
    btnCheckboxHidden +
    btnCheckbox;

  return params;
}

function writeContract() {
  let xhr = new XMLHttpRequest();
  // init to send params
  let params = getParams();

  //  add contract id to object
  const contractId = document.getElementById("contractId").attributes.value
    .value;
  params += 'contractId' + '="' + contractId + '"&';

  // add current user email to object
  const currentUserEmail = document.getElementById("currentUserEmail")
    .attributes.value.value;
  params += 'currentUserEmail' + '="' + currentUserEmail + '"&';

  // remove last &
  params = params.slice(0, -1);

  // load spinner
  document.getElementById('savespinner').innerHTML = "<div class='loader loader quantum-spinner'></div>";

  // ajax stuff
  xhr.open("POST", "../includes/worker/contractsWriter.php", true);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.onload = function () {
    if (this.status == 200) {
      const val = this.responseText;
      let last_update = val.split('.')[0]
      last_update = last_update.replace("\"", "");
      document.getElementById('savespinner').innerHTML = "";
      document.getElementsByClassName('col-lg-12 footer-desc')[0].firstElementChild.firstChild.textContent = 'letzte Änderung (letztes Speichern): ' + last_update;
    }
  };
  xhr.send(params);
}