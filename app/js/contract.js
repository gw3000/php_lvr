// delete localStorage Badge Item if exists
window.onload = function() {
    if (localStorage['importBadge']) {
        localStorage.removeItem('importBadge');
    }
};

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 make the accordion collapsable to every section without closing the other
 https://getbootstrap.com/docs/3.4/javascript/#collapseListGroupHeading1
 */

$('.collapse').collapse({
    toggle: false,
});

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 jquery show hide effect
 https://api.jqueryui.com/category/effects/
 */
const fx = 'slide';

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
 * returns a specific database table according to the secVar
 * section 1: t_cont_bd
 * section 2: t_cont_sa
 * section 3: t_cont_vn --> 10 und 11
 * section 4: t_cont_ul --> 13
 * section 5: t_cont_vd --> 14 und 15
 * @param {number} secVar
 */
const prefixTable = (secVar) => {
    if (secVar.substring(0, 1) == 2 || secVar.substring(0, 1) == 9) {
        return 't_cont_bd';
    } else if (secVar.substring(0, 1) == 3) {
        return 't_cont_sa';
    } else if (secVar.substring(0, 2) == 10 || secVar.substring(0, 2) == 11) {
        return 't_cont_vn';
    } else if (secVar.substring(0, 2) == 13) {
        return 't_cont_ul';
    } else if (secVar.substring(0, 2) == 14 || secVar.substring(0, 2) == 15) {
        return 't_cont_vd';
    }
};
// ***

/**
 * return a value  or a array of radio button object
 * @param {text} sec which subsection you want to work on
 * @param {text} query posible values: getVal, getArr
 * @param {number} number sets the number of radio buttons
 */
const radios = (sec, query, number) => {
    let value;
    if (query == 'getArr') {
        let radioArray = [];
        for (let num = 0; num < number; num++) {
            radioArray.push(
                document.getElementsByClassName(sec).item(0).children[num]
                .children[0].children[0].checked
            );
        }
        return radioArray;
    } else if (query == 'getVal') {
        value = document.getElementsByClassName(sec).item(0).children[number]
            .children[0].children[0].checked;
        return value;
    } else if (query == 'getBtn') {
        value = document.getElementsByClassName(sec).item(0).children[number]
            .children[0].children[0];
        return value;
    } else if (query == 'delVal') {
        for (let num = 0; num < number; num++) {
            document.getElementsByClassName(sec).item(0).children[
                num
            ].children[0].children[0].checked = false;
        }
    }
};
// ***

/**
 * get, set or simply return the forms checkboxes object
 * @param {text} sec which subsection you want to work on
 * @param {text} query posible values: getArr, justItem
 * @param {text, number} val sets the chosen value to the input object
 */
const checkboxes = (sec, query, item) => {
    if (query == 'getArr') {
        let checkArray = [];
        let checkBoxes = document.getElementsByClassName(sec);
        for (let index = 0; index < checkBoxes.length; index++) {
            checkArray.push(
                document.getElementsByClassName(sec)[index].firstElementChild
                .childNodes[1].checked
            );
        }
        return checkArray;
    } else if (query == 'justItem') {
        return document.getElementsByClassName(sec)[item].firstElementChild
            .childNodes[1];
    }
};

/**
 * get, set or simply return the forms inputs object
 * @param {text} sec which subsection you want to work on
 * @param {text} query posible values: getVal, setVal, justItem
 * @param {text, number} val sets the chosen value to the input object
 */
const inputs = (sec, query, val) => {
    let target_table = prefixTable(sec);
    let nameInput = 'target__' + target_table + '__' + sec;
    if (query == 'getVal') {
        return document.getElementsByName(nameInput).item(0).value;
    } else if (query == 'setVal') {
        document.getElementById(
            sec
        ).children[0].children[2].children[0].value = val;
    } else if (query == 'justItem') {
        return document.getElementsByName(nameInput).item(0);
    } else if (query == 'delVal') {
        document.getElementById(sec).children[0].children[2].children[0].value =
            '';
    }
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 1. basisdaten
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2d
const cd_2d = () => {
    if (radios('2d', 'getArr', 2)[1] == true) {
        $('#abschlussprodukte').hide(fx);
    } else {
        $('#abschlussprodukte').show(fx);
    }
};

radios('2d', 'getBtn', 0).onclick = () => $('#abschlussprodukte').show(fx);

radios('2d', 'getBtn', 1).onclick = () => {
    $('#abschlussprodukte').hide(fx);
    for (let index = 1; index < 12; index++) {
        document.getElementById('asp_' + index).checked = false;
    }
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 2. situationsabfrage
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 3f
const cd_3fa = () => {
    if (inputs('3f', 'getVal') != 'TDFL pauschal bei Vertragsbeginn') {
        inputs('3fa', 'setVal', '');
        $('#3fa').hide(fx);
    } else {
        $('#3fa').show(fx);
    }
};

inputs('3f', 'justItem').onchange = () => cd_3fa();
// ***

// 3g
const cd_3g = () => {
    if (checkboxes('3g', 'getArr')[5] == true) {
        $('#3ga').show(fx);
    } else {
        $('#3ga').hide(fx);
        inputs('3ga', 'setVal', '');
    }
};

checkboxes('3g', 'justItem', 5).onclick = () => cd_3g();
// ***

// 3g1
const cd_3g1 = () => {
    if (checkboxes('3g', 'getArr')[7] == true) {
        $('#3g1').hide(fx);
        radios('3g1', 'delVal', 0);
        radios('3g1', 'delVal', 1);
    } else {
        $('#3g1').show(fx);
    }
};

checkboxes('3g', 'justItem', 7).onclick = () => cd_3g1();
// ***

// 3klm
const cd_3klm = () => {
    if (radios('3j', 'getVal', 1) == true) {
        $('#3k').hide(fx);
        $('#3l').hide(fx);
        $('#3m').hide(fx);
    } else {
        $('#3k').show(fx);
        $('#3l').show(fx);
        $('#3m').show(fx);
    }
};

radios('3j', 'getBtn', 1).onclick = () => {
    cd_3klm();
    radios('3k', 'delVal', 2);
    radios('3l', 'delVal', 2);
    radios('3m', 'delVal', 2);
};

radios('3j', 'getBtn', 0).onclick = () => cd_3klm();
// ***

// 3o
const cd_3o = () => {
    if (radios('3n', 'getVal', 1) == true) {
        $('#3o').hide(fx);
    } else {
        $('#3o').show(fx);
    }
};

radios('3n', 'getBtn', 1).onclick = () => {
    cd_3o();
    inputs('3o', 'setVal', '');
};
// ***

radios('3n', 'getBtn', 0).onclick = () => cd_3o();

// 3q
const cd_3q = () => {
    if (radios('3p', 'getVal', 1) == true) {
        $('#3q').hide(fx);
    } else {
        $('#3q').show(fx);
    }
};

radios('3p', 'getBtn', 0).onclick = () => cd_3q();

radios('3p', 'getBtn', 1).onclick = () => {
    cd_3q();
    inputs('3q', 'setVal', '');
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 3. versicherungsnehmer / versicherte person
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 10a1
const cd_10a1 = () => {
    let val = inputs('9a', 'getVal');
    if (val == 'eine Einzelperson' || val == 'Eheleute/Lebenspartnerschaft') {
        $('#10a1').show(fx);
    } else {
        $('#10a1').hide(fx);
        radios('10a1', 'delVal', 2);
    }
};
// ***

// 10a
const cd_10a = () => {
    let val0 = radios('10a1', 'getVal', 0);
    let val1 = radios('10a1', 'getVal', 1);
    if (val0 == '1') {
        $('#10a').hide(fx);
        inputs('10a', 'delVal');
    } else if (val1 == '1') {
        $('#10a').show(fx);
    }
};

radios('10a1', 'getBtn', 1).onclick = () => {
    cd_10a();
    cd_10e();
    cd_10f();
    cd_10g();
};

radios('10a1', 'getBtn', 0).onclick = () => {
    cd_10a();
    cd_10e();
    cd_10f();
    cd_10g();
};
// ***

// 10c
const cd_10c = () => {
    let val = inputs('9a', 'getVal');
    let w1 = 'eine Einzelperson';
    let w2 = 'Eheleute/Lebenspartnerschaft';
    let w3 = 'Firma/Gesellschaft';
    if (val == w1 || val == w2 || val == w3) {
        $('#10c').hide(fx);
        inputs('10c', 'delVal');
    } else {
        $('#10c').show(fx);
    }
};
// ***

// 10d
const cd_10d = () => {
    let val = inputs('10a', 'getVal');
    let w1 =
        'Eine Person außer der Mandantschaft war bei Abschluss des Vertrages Versicherungsnehmer';
    let w2 =
        'Mehrere Personen außer der Mandantschaft waren bei Abschluss des Vertrages Versicherungsnehmer';

    if (val == w1 || val == w2) {
        $('#10d').hide(fx);
        inputs('10d', 'delVal');
    } else {
        $('#10d').show(fx);
    }
};

inputs('10a', 'justItem').onchange = () => {
    cd_10d();
    cd_10f();
    cd_10g();
};
// ***

// 10e
const cd_10e = () => {
    let val = inputs('9a', 'getVal');
    if (val == 'Eheleute/Lebenspartnerschaft') {
        $('#10e').show(fx);
    } else {
        $('#10e').hide(fx);
        inputs('10e', 'delVal');
    }
};

// ***

// 10f
const cd_10f = () => {
    let val0 = radios('10a1', 'getVal', 0);
    let val1 = inputs('10a', 'getVal');
    if (
        val0 == true ||
        val1 ==
        'Eine weitere Person neben der Mandantschaft war bei Abschluss des Vertrages Versicherungsnehmer'
    ) {
        $('#10f').hide(fx);
        inputs('10f', 'delVal');
    } else {
        $('#10f').show(fx);
    }
};
// ***

// 10g
const cd_10g = () => {
    let val0 = radios('10a1', 'getVal', 0);
    let val1 = inputs('10a', 'getVal');
    if (
        val0 == true ||
        val1 ==
        'Eine Person außer der Mandantschaft war bei Abschluss des Vertrages Versicherungsnehmer'
    ) {
        $('#10g').hide(fx);
        inputs('10g', 'delVal');
    } else {
        $('#10g').show(fx);
    }
};
// ***

// 11b
const cd_11b = () => {
    let val = radios('11a', 'getVal', 0);
    if (val == true) {
        $('#11b').hide(fx);
        inputs('11b', 'delVal');
    } else {
        $('#11b').show(fx);
    }
};

radios('11a', 'getBtn', 0).onclick = () => {
    cd_11b();
    cd_11c();
};

radios('11a', 'getBtn', 1).onclick = () => {
    cd_11b();
    cd_11c();
};
// ***

// 11c
const cd_11c = () => {
    let val = radios('11a', 'getVal', 0);
    if (val == true) {
        $('#11c').hide(fx);
        inputs('11c', 'delVal');
    } else {
        $('#11c').show(fx);
    }
};
// ***

// 11g
const cd_11g = () => {
    let val0 = inputs('9a', 'getVal');
    let val1 = radios('11f', 'getVal', 0);
    let w0 = 'Eheleute/Lebenspartnerschaft';
    if (val0 == w0 && val1 == true) {
        $('#11g').show(fx);
    } else {
        $('#11g').hide(fx);
        inputs('11g', 'delVal');
    }
};

radios('11f', 'getBtn', 0).onclick = () => {
    cd_11g();
    cd_11hi();
};

radios('11f', 'getBtn', 1).onclick = () => {
    cd_11g();
    cd_11hi();
};
// ***

// 11h + 11i
const cd_11hi = () => {
    let val0 = radios('11f', 'getVal', 1);
    let val1 = radios('11g', 'getVal', 0);
    if (val0 == true || val1 == true) {
        $('#11h').hide(fx);
        inputs('11h', 'delVal');
        $('#11i').hide(fx);
        inputs('11i', 'delVal');
    } else {
        $('#11h').show(fx);
        $('#11i').show(fx);
    }
};
radios('11g', 'getBtn', 0).onclick = () => cd_11hi();

radios('11g', 'getBtn', 1).onclick = () => cd_11hi();
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 4. unterlagen
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 13b + 13d + 14e + 14u
const cd_13b = () => {
    let val = inputs('3c', 'getVal');
    if (val == 'laufender Vertrag') {
        // 13b
        $('#13b').hide(fx);
        radios('13b', 'delVal', 0);
        radios('13b', 'delVal', 1);
        radios('13b', 'delVal', 2);
        // 13d
        $('#13d').hide(fx);
        radios('13d', 'delVal', 0);
        radios('13d', 'delVal', 1);
        // 14e
        $('#14e').hide(fx);
        inputs('14e', 'delVal');
        // 14u
        $('#14u').hide(fx);
        inputs('14u', 'delVal');
    } else {
        $('#13b').show(fx);
        $('#13d').show(fx);
        $('#14e').show(fx);
        $('#14u').show(fx);
    }
};

inputs('3c', 'justItem').onchange = () => {
    cd_13b();
    cd_13c();
    cd_14e();
    //cd_14u();
};
// ***

// 13c
const cd_13c = () => {
    let val = inputs('3c', 'getVal');
    if (val == 'beendeter Vertrag (gekündigt oder abgelaufen)') {
        $('#13c').hide(fx);
        radios('13c', 'delVal', 0);
        radios('13c', 'delVal', 1);
        radios('13c', 'delVal', 2);
    } else {
        $('#13c').show(fx);
    }
};
// ***

// 13f
const cd_13f = () => {
    let val = inputs('9a', 'getVal');
    let w0 = 'eine Einzelperson';
    let w1 = 'Eheleute/Lebenspartnerschaft';
    let w2 = 'Firma/Gesellschaft';
    if (val == w0 || val == w1 || val == w2) {
        $('#13f').hide(fx);
        radios('13f', 'delVal', 0);
        radios('13f', 'delVal', 1);
    } else {
        $('#13f').show(fx);
    }
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 5. vertagsdaten
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 14e
const cd_14e = () => {
    let val = inputs('3c', 'getVal');
    if (val == '') {
        $('#14e').hide(fx);
        inputs('14c', 'delVal');
    } else {
        $('#14e').show(fx);
    }
};
// ***

// 14h
const cd_14h = () => {
    let val = radios('14g', 'getVal', 1);
    if (val == true) {
        $('#14h').hide(fx);
        inputs('14h', 'delVal');
    } else {
        $('#14h').show(fx);
    }
};

radios('14g', 'getBtn', 0).onclick = () => {
    cd_14h();
    cd_14i();
    cd_14l();
    cd_14p();
};

radios('14g', 'getBtn', 1).onclick = () => {
    cd_14h();
    cd_14i();
    cd_14l();
    cd_14p();
};
// ***

// 14i + 14j + 14k + 14m
const cd_14i = () => {
    let val = radios('14g', 'getVal', 0);
    if (val == true) {
        $('#14i').hide(fx);
        inputs('14i', 'delVal');
        $('#14j').hide(fx);
        inputs('14j', 'delVal');
        $('#14k').hide(fx);
        inputs('14k', 'delVal');
        $('#14m').hide(fx);
        inputs('14m', 'delVal');
    } else {
        $('#14i').show(fx);
        $('#14j').show(fx);
        $('#14k').show(fx);
        $('#14m').show(fx);
    }
};
// ***

// 14l
const cd_14l = () => {
    let val0 = radios('14g', 'getVal', 0);
    let val1 = radios('14k', 'getVal', 0);
    if (val0 == true || val1 == true) {
        $('#14l').hide(fx);
        inputs('14l', 'delVal');
    } else {
        $('#14l').show(fx);
    }
};

radios('14k', 'getBtn', 0).onclick = () => cd_14l();

radios('14k', 'getBtn', 1).onclick = () => cd_14l();
// ***

// 14p
const cd_14p = () => {
    let val0 = radios('14g', 'getVal', 0);
    let val1 = radios('14m', 'getVal', 0);
    if (val0 == true || val1 == true) {
        $('#14p').hide(fx);
        inputs('14p', 'delVal');
    } else {
        $('#14p').show(fx);
    }
};

radios('14m', 'getBtn', 0).onclick = () => cd_14p();

radios('14m', 'getBtn', 1).onclick = () => cd_14p();
// ***

// 14s + 14t
const cd_14s = () => {
    let val = radios('14r', 'getVal', 1);
    if (val == true) {
        $('#14s').hide(fx);
        inputs('14s', 'delVal');
        $('#14t').hide(fx);
        inputs('14t', 'delVal');
    } else {
        $('#14s').show(fx);
        $('#14t').show(fx);
    }
};

radios('14r', 'getBtn', 0).onclick = () => cd_14s();

radios('14r', 'getBtn', 1).onclick = () => cd_14s();
// ***

// 14v + 14w
const cd_14v = () => {
    let val0 = inputs('3c', 'getVal');
    let val1 = radios('14u', 'getVal', 1);
    if (val0 == '' || val1 == true) {
        $('#14v').hide(fx);
        inputs('14v', 'delVal');
        $('#14w').hide(fx);
        inputs('14w', 'delVal');
    } else {
        $('#14v').show(fx);
        $('#14w').show(fx);
    }
};

radios('14u', 'getBtn', 0).onclick = () => cd_14v();

radios('14u', 'getBtn', 1).onclick = () => cd_14v();
// ***
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// einzelperson 9a
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
inputs('9a', 'justItem').onchange = () => {
    cd_10a1();
    cd_10c();
    cd_10e();
    cd_11g();
    cd_13f();
};
// ***

// show or hide "no data banner"
const noDataBanner = () => {
    const val = document.getElementById('target_1').value;
    if (val != '') {
        $('#nodata').removeClass('alert alert-danger banner-warning');
        $('#nodata').addClass('hidden');
        $('#nodata').hide();
    } else {
        $('#nodata').removeClass('hidden');
        $('#nodata').addClass('alert alert-danger banner-warning');
        $('#nodata').show();
    }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// contract functionality
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// sumarized functions on contract page
const contract = () => {
    // show or hide banner after importing data
    noDataBanner();
    // hide import button
    hideImportButton();
    // section 02
    cd_2d();
    cd_3fa();
    cd_3g();
    cd_3g1();
    cd_3klm();
    cd_3o();
    cd_3q();
    // section 03
    cd_10a1();
    cd_10a();
    cd_10c();
    cd_10d();
    cd_10e();
    cd_10f();
    cd_10g();
    cd_11b();
    cd_11c();
    cd_11g();
    cd_11hi();
    //section 04
    cd_13b();
    cd_13c();
    cd_13f();
    // section 05
    cd_14h();
    cd_14i();
    cd_14l();
    cd_14p();
    cd_14s();
    cd_14v();
    // insurane
    emptyInsurance();
    // hide rsv comments
    hideRSVComments();
    // hide left side in sections
    hideLeftSide();
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// import data from left to right in details section
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const importData = () => {
    let elements = document.getElementsByTagName('input');
    let num;
    for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        if (
            element.id.substring(0, 7) == 'source_' &&
            element.id != 'source_76'
        ) {
            num = element.id.substring(7);
            // only import contract details -- rsv should be handled seperatly
            // 121 corresponses to Mandatsbegründung -> Wird in die
            // Übermittlung der personenbezogenen Daten gem. .....
            if (num < 121) {
                if (element.type == 'radio') {
                    if (element.checked) {
                        document.getElementById(`target_${num}`).checked = true;
                    } else {
                        document.getElementById(
                            `target_${num}`
                        ).checked = false;
                    }
                } else if (
                    element.type == 'text' ||
                    element.type == 'number' ||
                    element.type == 'date'
                ) {
                    if (
                        document.getElementById(`target_${num}`).type ==
                        'text' ||
                        document.getElementById(`target_${num}`).type ==
                        'number' ||
                        document.getElementById(`target_${num}`).type ==
                        'select-one'
                    ) {
                        document.getElementById(`target_${num}`).value =
                            element.value;
                    } else if (
                        document.getElementById(`target_${num}`).type ==
                        'date' &&
                        document.getElementById(`source_${num}`).value != ''
                    ) {
                        document.getElementById(`target_${num}`).value =
                            element.value;
                    }
                } else if (element.type == 'checkbox') {
                    if (element.checked) {
                        document.getElementById(`target_${num}`).checked = true;
                    } else {
                        document.getElementById(
                            `target_${num}`
                        ).checked = false;
                    }
                }
            }
        }
    }

    // Ratenzahlungen (Zeiträume und Beträge) (14l)
    document.getElementById('target_94').innerHTML = document.getElementById(
        'source_94'
    ).innerHTML;

    // weitere Angaben
    document.getElementById('target_109').innerHTML = document.getElementById(
        'source_109'
    ).innerHTML;

    // show or hide banner after importing data
    noDataBanner();

    // Badge in Navbar decrement
    let badge = document.getElementById('badge');
    let badgeNum = parseInt(badge.innerText) - 1;
    if (badgeNum > 0) {
        badge.innerText = badgeNum;
    } else {
        badge.classList.remove('badge');
        badge.innerText = '';
    }
    localStorage.setItem('importBadge', 'True');
};
// ***

// data import on click
document.getElementById('importData').onclick = () => {
    importData();
    contract();
};
// ***

// reset data on click 'Alle Werte zurücksetzen'
document.getElementById('resetData').onclick = () => {
    document.getElementById('contract').reset();
    contract();
    // show or hide banner after importing data
    noDataBanner();
    // show banner or inclease counter

    // Badge in Navbar increment (on reset)
    if (localStorage['importBadge']) {
        let badge = document.getElementById('badge');
        let badgeNum;
        if (badge.innerText == '') {
            badgeNum = 1;
            badge.classList.add('badge');
        } else {
            badgeNum = parseInt(badge.innerText) + 1;
        }
        badge.innerText = badgeNum;
        localStorage.removeItem('importBadge');
    }
};

// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// show or hide all accordions
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
document.getElementById('showAccordion').onclick = () => {
    const btn = document.getElementById('showAccordion').classList;
    //const acc = document.getElementsByClassName('panel-collapse');
    if (btn[3] === 'fa-plus-square') {
        // use jquery to open all accordions
        $('.panel-collapse').collapse('show');
        document
            .getElementById('showAccordion')
            .classList.remove('fa-plus-square');
        document
            .getElementById('showAccordion')
            .classList.add('fa-minus-square');
    } else if (btn[3] === 'fa-minus-square') {
        // use jquery to close all accordions
        $('.panel-collapse').collapse('hide');
        document
            .getElementById('showAccordion')
            .classList.remove('fa-minus-square');
        document
            .getElementById('showAccordion')
            .classList.add('fa-plus-square');
    }
};
// ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// show hide all varables
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
document.getElementById('showVars').onclick = () => {
    const btn = document.getElementById('showVars').classList;
    const fg = document.getElementsByClassName('form-group');
    if (btn[3] === 'fa-plus-circle') {
        document.getElementById('showVars').classList.remove('fa-plus-circle');
        document.getElementById('showVars').classList.add('fa-minus-circle');
        for (let num = 21; num <= 86; num++) {
            $('#' + fg[num].parentNode.id).show(fx);
        }
    } else if (btn[3] === 'fa-minus-circle') {
        document.getElementById('showVars').classList.remove('fa-minus-circle');
        document.getElementById('showVars').classList.add('fa-plus-circle');
        contract();
    }
};
//  ***

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// hide rsv comments
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const hideRSVComments = () => {
    $('#check_12a').hide();
    $('#check_12b').hide();
    $('#check_12c').hide();
    $('#check_12d').hide();
    $('#check_12e').hide();
    $('#check_12f').hide();
    $('#check_12g').hide();
    $('#vs_num').children().children().children()[0].textContent = '';
    $('#ds').children().children().children()[0].textContent = '';
    $('#vs_arb').children().children().children()[0].textContent = '';
    $('#dt_chg_status').children().children().children()[0].textContent = '';
    $('#bm').children().children().children()[0].textContent = '';
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// what functions should be loaded on site loading
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$(document).ready(function() {
    contract();
});

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// autoload insurance
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
$(function() {
    $('#insurance_presentation').autocomplete({
        source: $(location).attr('protocol') +
            '//' +
            $(location).attr('host') +
            '/includes/autocomplete.php?autocomplete=insurance',
        minLength: 3,
        select: function(event, ui) {
            $('#14a label')[0].innerHTML =
                '<strong>Versicherer bei Vertragsabschluss und Auswahl (14a)</strong>';
            $('#insurance_l2 label')[0].innerHTML =
                '<strong>Versicherer bei Vertragsabschluss und Gegner</strong>';
            $('#target_76').val(ui.item.id);
            $('#insurance_presentation').val(ui.item.selection);
            $('#insurance_selection').val(ui.item.presentation);
            $('#insurance_opponent').val(ui.item.opponent);
            $('#insurance_selection').show();
            $('#insurance_opponent').show();
            $('#insurance_l2').show();
        },
    });
});

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Empty 14a group on empty input 14a
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const emptyInsurance = () => {
    if (document.getElementById('insurance_presentation').value == '') {
        document.getElementById('insurance_selection').value = '';
        document.getElementById('insurance_opponent').value = '';
        document.getElementById('target_76').value = '';
        $('#14a label')[0].innerHTML =
            'Versicherer bei Vertragsabschluss (14a)';
        $('#insurance_l2 label')[0].innerHTML =
            'Versicherung bei Auswahl (links) Gegner (rechts)';
        $('#insurance_selection').hide(fx);
        $('#insurance_opponent').hide(fx);
        $('#insurance_l2').hide(fx);
    }
    if (Number(document.getElementById('target_76').value) > 0) {
        document.getElementsByClassName('14a')[0].className = 'col-lg-4';
        $('#target_76').hide();
    }
};

document.getElementById('insurance_presentation').onchange = () =>
    emptyInsurance();

document.getElementById('insurance_presentation').onkeyup = () =>
    emptyInsurance();

const hideImportButton = () => {
    let val0 = document.getElementById('source_0').value;
    let val1 = document.getElementById('target_0').value;
    if (val0 == '') {
        $('#importData').hide();
    } else if (val0 != '' && val1 != '') {
        $('#importData').hide();
    } else if (val0 != '' && val1 == '') {
        $('#importData').show();
    }
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// hide left side in sections if there is no platform input
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const hideLeftSide = () => {
    const fg = document.getElementsByClassName('form-group');
    const e9a = document.getElementById('9a').children[0].children[1].children[0].value;

    if (e9a == '') {
        for (var i = 0, len = fg.length; i < len; i++) {
            if (fg[i].lastElementChild.className == 'col-lg-1 pull-right') {
                fg[i].children[1].style.display = 'none'
                fg[i].children[2].className = 'col-lg-8 col-md-8 col-sm-8';
            }
        }
    }
}
