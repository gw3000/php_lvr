<?php

/**
 * Results File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

if (!htmlspecialchars($_GET['lname']) == '' && !htmlspecialchars($_GET['fname']) == '') {
    $lname = htmlspecialchars($_GET['lname']);
    $fname = htmlspecialchars($_GET['fname']);
    // query address witch lname and fname
    // $sql = "SELECT * FROM t_address ad JOIN t_ram_addr_num an ON ad.id = an.id WHERE lname = :lname AND fname = :fname";
    $sql = "SELECT * FROM t_address ad WHERE lname = :lname AND fname = :fname";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':lname', $lname, PDO::PARAM_STR);
    $stmt->bindParam(':fname', $fname, PDO::PARAM_STR);
    // if ($stmt->rowCount() === 0) {
    //     header('location: search.php');
    // }
    $stmt->execute();
} elseif (!htmlspecialchars($_GET['lname']) == '' && htmlspecialchars($_GET['fname']) == '') {
    $lname = htmlspecialchars($_GET['lname']) . '%';
    // query address with lname
    // $sql = "SELECT * FROM t_address ad JOIN t_ram_addr_num an ON ad.id = an.id WHERE lname ILIKE :lname";
    $sql = "SELECT * FROM t_address ad WHERE lname ILIKE :lname";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':lname', $lname, PDO::PARAM_STR);
    $stmt->execute();
}

// query import timestamp
$sql_ts = "SELECT max(inserted_at) FROM public.t_import_address;";
$stmt_ts = $pdo->prepare($sql_ts);
$stmt_ts->execute();
$row_ts = $stmt_ts->fetch(PDO::FETCH_ASSOC);
$max_import = $row_ts['max'];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/styles.css">
    <!-- css bootstrap datatables stuff -->
    <link rel="stylesheet" href="css/datatables.min.css" />

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jquery
        $(document).ready(function() {
            $('#resultsTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Alle"]
                ],
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });
    </script>

</head>

<body>

    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <?php require_once 'includes/modals/raieditorModal.php'; ?>
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12 col-md-8 col-sm-7">
                    <?php
                    if (htmlspecialchars($_GET['lname']) == '%') {
                        $heading = 'Übersicht aller Adressen';
                    } else {
                        $heading = 'Suchergebnisse <small>für den Suchbegriff ' . htmlspecialchars($_GET['lname']) . " " . htmlspecialchars($_GET['fname']) . "</small>";
                    }
                    echo "<h3>" . $heading . "</h3>";
                    ?>
                </div>
                <!-- table -->
                <div class="col-lg-12 col-md-12 col-sm-12 tabley">
                    <table id="resultsTable" class="display table table-bordered table-striped table-hover cell-border nowrap">
                        <thead>
                            <tr>
                                <th>ID::RAM</th>
                                <!-- <th>ID::PF</th> -->
                                <!-- <?php
                                if ($_SESSION['role'] != 'user-fa' && $_SESSION['role'] != 'user-ra') {
                                    echo "<th></th>";
                                }
                                ?> -->
                                <th>Nachname</th>
                                <th>Vorname</th>
                                <th>Nachname (2)</th>
                                <th>Vorname (2)</th>
                                <th>Straße</th>
                                <th>PLZ</th>
                                <th>Ort</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                $date = new DateTime($row['created_at']);
                                echo "<tr>";
                                echo "<td><a href='address.php?ida=$row[id]'><strong>" . $row['id'] . "</strong></a></td>";
                                // echo "<td><a href='address.php?ida=$row[id]'><strong>" . $row['iadressnummer'] . "</strong></a></td>";
                                // echo "<td>" . $row['clientnum'] . "</td>";
                                // if ($_SESSION['role'] != 'user-fa' && $_SESSION['role'] != 'user-ra') {
                                //     echo "<td><center><button type='button' onclick = \"loadraidEditor(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#raiModModal'><i class='fas fa-edit'></i></button></center></td>";
                                // }
                                echo "<td>" . $row['lname'] . "</td>";
                                echo "<td>" . $row['fname'] . "</td>";
                                echo "<td>" . $row['lname2'] . "</td>";
                                echo "<td>" . $row['fname2'] . "</td>";
                                echo "<td>" . $row['street'] . "</td>";
                                echo "<td>" . $row['zip'] . "</td>";
                                echo "<td>" . $row['city'] . "</td>";
                                echo " </tr> ";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/raieditor.js"></script>
</body>

</html>