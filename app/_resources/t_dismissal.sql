-- public.t_dismissal definition
-- Drop table
DROP TABLE IF EXISTS public.t_dismissal;
CREATE TABLE public.t_dismissal (
	id bigserial NOT NULL,
	description varchar NOT NULL,
	helptext text NULL,
	helptext_pf text NULL,
	helptext_wk text NULL,
	deadline int4 NULL,
	after_deadline text NULL
);
CREATE UNIQUE INDEX t_dismissal_idx ON public.t_dismissal USING btree (id);

-- public.t_dismissal_users definition
-- Drop table
DROP TABLE IF EXISTS public.t_dismissal_users;
CREATE TABLE public.t_dismissal_users (id_dismissal int4 NULL, id_user int4 NULL);

-- public.t_cont_dismissal definition
-- Drop table
DROP TABLE IF EXISTS public.t_cont_dismissal;
create table public.t_cont_dismissal (
	id uuid not null,
	id_dsm int4 null,
	dt_dsm date null,
	last_update timestamp null,
	last_user varchar null,
	constraint t_cont_dismissal_pk primary key (id)
);
COMMENT ON COLUMN public.t_cont_dismissal.id_dsm IS 'id aus Tabell t_dismissal';
COMMENT ON COLUMN public.t_cont_dismissal.dt_dsm IS 'Datum der Statusänderung';

insert into t_cont_dismissal (id) select id from t_contracts;

CREATE OR REPLACE FUNCTION public.func_on_contract()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
begin
   	if tg_op = 'DELETE' then
        delete from t_cont_bd where id = old.id;
        delete from t_cont_rsv where id = old.id;
        delete from t_cont_ms where id = old.id;
        delete from t_cont_dismissal where id = old.id;
        delete from t_cont_sa where id = old.id;
        delete from t_cont_ul where id = old.id;
        delete from t_cont_vd where id = old.id;
        delete from t_cont_vn where id = old.id;
        delete from t_cont_check where id = old.id;
        delete from t_cont_calc where id = old.id;
        delete from t_cont_import where id = old.id;
        delete from t_ram_rsv_cont where id = old.id;
        return old;
    elseif tg_op = 'INSERT' then
        insert into t_cont_bd (id) select new.id;
        insert into t_cont_ms (id) select new.id;
        insert into t_cont_dismissal (id) select new.id;
        insert into t_cont_rsv (id) select new.id;
        insert into t_cont_sa (id) select new.id;
        insert into t_cont_ul (id) select new.id;
        insert into t_cont_vd (id) select new.id;
        insert into t_cont_vn (id) select new.id;
        insert into t_cont_calc (id) select new.id;
       	insert into t_cont_check (id, checked) select new.id, '{"2b": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "2c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "2d": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "2e": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "2f": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3b": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3f": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3g": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3h": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3i": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3j": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3k": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3l": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3m": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3n": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3o": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3p": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3q": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "9a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10d": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10e": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10f": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10g": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11b": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11f": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11g": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11h": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "11i": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "13a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "13b": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "13c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "13d": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "13f": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14a": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14b": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14c": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14d": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14e": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14g": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14h": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14i": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14j": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14k": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14l": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14m": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14p": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14q": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14r": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14s": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14t": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14u": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14v": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "14w": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3g1": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "10a1": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "15": {"status": "", "comment": "", "last_user": "", "last_update": ""}, "3fa": {"status": "", "comment": "", "last_user": "", "last_update": ""}}';
       	insert into t_cont_import (id, inserted_at) select new.id, now();
        insert into t_ram_rsv_cont (id) select new.id;
        return new;
    elseif tg_op = 'UPDATE' then
        update t_cont_bd set id=new.id where id = old.id;
        update t_cont_ms set id=new.id where id = old.id;
        update t_cont_dismissal set id=new.id where id = old.id;
        update t_cont_rsv set id=new.id where id = old.id;
        update t_cont_sa set id=new.id where id = old.id;
        update t_cont_ul set id=new.id where id = old.id;
        update t_cont_vd set id=new.id where id = old.id;
        update t_cont_vn set id=new.id where id = old.id;
       	update t_cont_check set id=new.id where id = old.id;
       	update t_cont_calc set id=new.id where id = old.id;
       	update t_cont_import set id=new.id where id = old.id;
       	update t_ram_rsv_cont set id=new.id where id = old.id;
        return new;
    end if;
end; $function$
;
