CREATE TABLE public.t_ram_addr_num (
	id int NOT NULL,
	iadressnummer int NULL,
	clientnum int NULL,
	last_update TIMESTAMP,
	last_user varchar NULL

);
COMMENT ON TABLE public.t_ram_addr_num IS 'consists of the lv-r id and the iAdressnummer from RAMICRO';

-- Column comments

COMMENT ON COLUMN public.t_ram_addr_num.iadressnummer IS 'iAdressnummer aus RAMICRO';


-- add ids from platform;
insert into t_ram_addr_num (id, iadressnummer) select id, id from t_address;