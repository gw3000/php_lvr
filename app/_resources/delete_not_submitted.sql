CREATE TABLE public.t_cont_insapp_del (
	contid int varchar,
	marked_to_delete timestamp NULL,
	deleted timestamp NULL,
	history json NULL
);
