#!/bin/bash

BRANCH=$(git symbolic-ref --short HEAD)
FILE="dump.sql"

if [ $BRANCH != 'master' ]; then
    if test -f "$FILE"; then
        echo "-------------------------"
        echo "    RESTORE..DATEBASE    "
        echo "-------------------------"
        docker exec -it lvr-postgres psql -U lvr -d postgres -c "DROP DATABASE db_lvr;"
        docker exec -it lvr-postgres psql -U lvr -d postgres -c "CREATE DATABASE db_lvr;"
        cat $FILE | docker exec -i lvr-postgres psql -U lvr -d db_lvr
        rm -fr $FILE
    else
        echo "------------------------"
        echo "  there is no $FILE  "
        echo "------------------------"
    fi
else
    echo 'You are in the wrong branch! Change it to develop!'
fi
