import json

import psycopg2

# which new key should be added to the json->checked column
new_key = '17'

# Connect to an existing database
try:
    conn = psycopg2.connect(
        "dbname='db_lvr' user='lvr' host='klaus' password='wOpH3Wx8geKSg'")
except:
    print('Error')

# Open a cursor to perform database operations
cur = conn.cursor()

# Query the database and obtain data as Python objects
cur.execute("SELECT id, checked FROM t_cont_check WHERE checked IS NOT NULL;")
data = cur.fetchall()
for item in data:
    id = item[0]
    checked = item[1]
    checked[new_key] = {'status': '', 'comment': '',
                        'last_user': '', 'last_update': ''}
    json_obj = json.dumps(checked)
    sql = "UPDATE t_cont_check SET checked = %s WHERE id = %s"
    cur.execute(sql, (json_obj, id))

    # Make the changes to the database persistent
    conn.commit()

# Close communication with the database
cur.close()
conn.close()
