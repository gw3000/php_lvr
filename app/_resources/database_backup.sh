#!/bin/bash
rm -fr dump.sql 2>/dev/null
docker exec -t lvr-postgres pg_dumpall -c -U postgres > dump.sql
cp dump.sql dump_`date +%Y-%m-%d"__"%H_%M_%S`.sql