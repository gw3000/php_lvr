#!/bin/bash

BRANCH=$(git symbolic-ref --short HEAD)
FILE="dump_insurance.sql"

if [ $BRANCH != 'master' ]; then
    if test -f "$FILE"; then
        echo "---------------------------------------"
        echo "    RESTORE..DATEBASE..INSURANCEAPP    "
        echo "---------------------------------------"
        docker exec -it lvr-postgres psql -U lvr -d postgres -c "DROP DATABASE IF EXISTS insuranceapp;"
        docker exec -it lvr-postgres psql -U lvr -d postgres -c "CREATE DATABASE insuranceapp;"
        cat $FILE | /usr/bin/docker exec -i lvr-postgres psql -U lvr -d insuranceapp
    else
        echo "------------------------"
        echo "  there is no $FILE  "
        echo "------------------------"
    fi
else
    echo 'You are in the wrong branch! Change it to develop!'
fi

