-- public.t_cont_insapp_del definition

-- Drop table

-- DROP TABLE public.t_cont_insapp_del;

CREATE TABLE public.t_cont_insapp_del (
	contid varchar NULL,
	marked_to_delete timestamp NULL,
	deleted timestamp NULL,
	history json NULL
);
