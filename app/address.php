<?php

/**
 * Address File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

if (!htmlspecialchars($_GET['ida']) == '') {
    $ida = htmlspecialchars($_GET['ida']);
    // query address
    $sql = "SELECT * FROM t_address WHERE id = :ida;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':ida', $ida, PDO::PARAM_INT);
    $stmt->execute();
    $row = $stmt->fetch();
    $inram = ($row['in_ram'] == true) ? 'bereits in RAM' : 'noch nicht in RAM';
    

    unset($stmt);
    unset($sql);

    // query contracts
    $sql = "SELECT 
            co.* 
        FROM 
            t_address ad 
        INNER JOIN 
            t_contracts co 
        ON 
            ad.id=co.ida 
        WHERE 
            ad.id = :ida 
        ORDER BY 
            co.refnum ASC;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':ida', $ida, PDO::PARAM_INT);
    $stmt->execute();
} else {
    header('location: index.php');
}

// check if address is in ramicro
// $sql_ram = "SELECT
//             iAdressNummer
//         FROM
//             RAMICRO.dbo.tblAdressen
//         WHERE
//             iAdressNummer = :ida;";
// $stmt_ram = $pdo_ram->prepare($sql_ram);
// $stmt_ram->bindParam(':ida', $row['id'], PDO::PARAM_INT);
// $stmt_ram->execute();
// $row_ram['in_ram'] = $stmt_ram->fetch();
// $inram = ($row_ram['in_ram'][0] == true) ? 'bereits in RAM' : 'noch nicht in RAM';

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <title>lvr &middot; db</title>

    <!-- jquery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <h2>Mandantenkurzübersicht</small></h2>
                <div class="col-lg-7 col-md-7 col-sm-7">
                    <br>
                    <form class="form-horizontal">
                        <div id="pers1">
                            <fieldset>
                                <legend>Mandant 1 <small class="pull-right text-muted smt"><?php echo $inram; ?></small>
                                </legend>

                                <!-- ID -->
                                <div class="form-group">
                                    <label for="mbd_id" class="col-lg-5 control-label">ID
                                        <small>(MBD intern)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="mbd_id" placeholder="ID" value="<?php echo $row['id']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- number of clients -->
                                <div class="form-group">
                                    <label for="9a" class="col-lg-5 control-label">Art und Anzahl der Mandantschaft
                                        <small>(9a)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9a" placeholder="Art und Anzahl der Mandantschaft" value="<?php echo $row['clienttype']; ?>" disabled>
                                    </div>
                                </div>


                                <!-- ID -->
                                <div class="form-group">
                                    <label for="9a(1)" class="col-lg-5 control-label">Kundennummer
                                        <small>(9a1)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9a1" placeholder="Kundennummer" value="<?php echo $row['customerid']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- degree -->
                                <div class="form-group">
                                    <label for="9b" class="col-lg-5 control-label">Titel <small>(9b)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9b" placeholder="Titel" value="<?php echo $row['degree']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- salutation -->
                                <div class="form-group">
                                    <label for="9c" class="col-lg-5 control-label">Anrede <small>(9c)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9c" placeholder="Anrede" value="<?php echo $row['salutation']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- fname -->
                                <div class="form-group">
                                    <label for="9d" class="col-lg-5 control-label">Vorname <small>(9d)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9d" placeholder="Vorname" value="<?php echo $row['fname']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- lname -->
                                <div class=" form-group">
                                    <label for="9e" class="col-lg-5 control-label">Nachname <small>(9e)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9e" placeholder="Nachname" value="<?php echo $row['lname']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- street -->
                                <div class="form-group">
                                    <label for="9f" class="col-lg-5 control-label">Straße und Hausnummer
                                        <small>(9f/9f1)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9f" placeholder="Straße" value="<?php echo $row['street'] . ' ' . $row['9f1']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- city -->
                                <div class=" form-group">
                                    <label for="9h" class="col-lg-5 control-label">Ort <small>(9h)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9h" placeholder="Ort" value="<?php echo $row['city']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- zip -->
                                <div class=" form-group">
                                    <label for="zip1" class="col-lg-5 control-label">Postleitzahl
                                        <small>(9g)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="zip1" placeholder="Postleitzahl" value="<?php echo $row['zip']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- country -->
                                <div class=" form-group">
                                    <label for="9i" class="col-lg-5 control-label">Land <small>(9i)</small></label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9i" placeholder="Land" value="<?php echo $row['country']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- phone -->
                                <div class=" form-group">
                                    <label for="9j" class="col-lg-5 control-label">Telefon (9j)</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9j" placeholder="Telefon" value="<?php echo $row['phone']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- phone mobile -->
                                <div class=" form-group">
                                    <label for="9k" class="col-lg-5 control-label">Mobil Telefon (9k)</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9k" placeholder="Telefon Mobil" value="<?php echo $row['mobile']; ?>" disabled>
                                    </div>
                                </div>

                                <!-- email -->
                                <div class="form-group">
                                    <label for="9l" class="col-lg-5 control-label">Email-Adresse (9l)</label>
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" id="9l" placeholder="Email-Adresse" value="<?php echo $row['mail']; ?>" disabled>
                                    </div>
                                </div>
                        </div>
                        <div id="pers2">
                            <legend>Mandant 2</legend>

                            <div class="form-group">
                                <label for="9m" class="col-lg-5 control-label">Titel <small>(9m)</small></label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="9m" placeholder="Titel" value="<?php echo $row['degree2']; ?>" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="9n" class="col-lg-5 control-label">Anrede <small>(9n)</small></label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="9n" placeholder="Anrede" value="<?php echo $row['salutation2']; ?>" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="9o" class="col-lg-5 control-label">Vorname (9o)</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="9o" placeholder="Vorname" value="<?php echo $row['fname2']; ?>" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="9p" class="col-lg-5 control-label">Nachname (9p)</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="9p" placeholder="Nachname" value="<?php echo $row['lname2']; ?>" disabled>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="9r" class="col-lg-5 control-label">Email-Adresse (9r)</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control" id="9r" placeholder="Email-Adresse" value="<?php echo $row['mail2']; ?>" disabled>
                                </div>
                            </div>
                        </div>
                        </fieldset>
                    </form>
                </div>

                <div class="col-lg-3 col-lg-offset-1 col-md-5 col-sm-10">
                    <h3>Verträge</h3>
                    <p>Übersicht aller Verträge des Mandanten:</p>
                    <?php
                    while ($row_contr = $stmt->fetch(PDO::FETCH_ASSOC)) {
                        echo "<ul>";
                        echo "<li><a href='contract.php?id=" . $row_contr['id'] . "'>" . $row_contr['refnum'] . " &middot; " . $row_contr['contnum'] . "</a></li>";
                        echo "</ul>";
                    }
                    ?>
                </div>


            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/main.js"></script>
    <script src="js/address.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
</body>

</html>