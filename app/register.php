<?php

/**
 * Register File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

// Init vars
$name = $email = $password = $confirm_password = '';
$name_err = $email_err = $password_err = $confirm_password_err = '';

// Process form when post submit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Sanitize POST
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    // Put post vars in regular vars
    $name =  trim($_POST['name']);
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);
    $confirm_password = trim($_POST['confirm_password']);

    // Validate email
    if (empty($email)) {
        $email_err = 'Bitte eine Email eingeben!';
    } else {
        // Prepare a select statement
        $sql = 'SELECT id FROM t_users WHERE email = :email';

        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);

            // Attempt to execute
            if ($stmt->execute()) {
                // Check if email exists
                if ($stmt->rowCount() === 1) {
                    $email_err = 'Email Adresse bereits vergeben!';
                }
            } else {
                die('Etwas ging schief');
            }
        }

        unset($stmt);
    }

    // Validate name
    if (empty($name)) {
        $name_err = 'Bitte einen Namen eingeben!';
    }

    // Validate password
    if (empty($password)) {
        $password_err = 'Bitte ein Passwort eingeben!';
    } elseif (strlen($password) < 6) {
        $password_err = 'Passwort muss mindestens aus 6 Zeichen bestehen! ';
    }

    // Validate Confirm password
    if (empty($confirm_password)) {
        $confirm_password_err = 'Bitte das Passwort bestätigen';
    } else {
        if ($password !== $confirm_password) {
            $password_err = $confirm_password_err = 'Passwörter nicht identisch';
        }
    }

    // Make sure errors are empty
    if (empty($name_err) && empty($email_err) && empty($password_err) && empty($confirm_password_err)) {
        // Hash password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Prepare insert query
        $sql = 'INSERT INTO t_users (name, email, password, role, created_at) VALUES (:name, :email, :password, :role, :created_at)';
        $role = 'user';
        $created_at = 'now()';

        if ($stmt = $pdo->prepare($sql)) {
            // Bind params
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->bindParam(':role', $role, PDO::PARAM_STR);
            $stmt->bindParam(':created_at', $created_at, PDO::PARAM_STR);

            // Attempt to execute
            if ($stmt->execute()) {
                // Redirect to login
                header('location: users.php');
            } else {
                die('Something went wrong 2');
            }
        }
        unset($stmt);
    }

    // Close connection
    unset($pdo);
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>lvr &middot; db</title>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-5">

                    <h3 class="h3-spacing">Account anlegen</h3>

                    <p>Füllen Sie alle Felder aus, um einen neuen Account anzulegen!</p>

                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal" id="register">

                        <!-- Name -->
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-warning' : ''; ?>">
                            <label class="col-lg-3 control-label" for="name">Name</label>
                            <div class="col-lg-9">
                                <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($name_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $name; ?>">
                                <span class="invalid-feedback"><?php echo $name_err; ?></span>
                            </div>
                        </div>

                        <!-- Email -->
                        <div class="form-group <?php echo (!empty($email_err)) ? 'has-warning' : ''; ?>">
                            <label class="col-lg-3 control-label" for="email" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-9">
                                <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($email_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $email; ?>">
                                <span class="invalid-feedback"><?php echo $email_err; ?></span>
                            </div>
                        </div>

                        <!-- Password -->
                        <div class="form-group <?php echo (!empty($password_err)) ? 'has-warning' : ''; ?>">
                            <label class="col-lg-3 control-label" for="password">Passwort</label>
                            <div class="col-lg-9">
                                <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $password; ?>">
                                <span class="invalid-feedback"><?php echo $password_err; ?></span>
                            </div>
                        </div>

                        <!-- Password confirmation -->
                        <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-warning' : ''; ?>">
                            <label class="col-lg-3 control-label" for="confirm_password">Passwort wiederholen</label>
                            <div class="col-lg-9">
                                <input type="password" name="confirm_password" class="form-control form-control-lg <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>" value="<?php echo $confirm_password; ?>">
                                <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
                            </div>
                        </div>

                        <!-- submit button -->
                        <div class="form-group">
                            <div class="btn-group col-lg-9 col-lg-offset-3">
                                <button type="reset" onclick="resetForm()" class="btn btn-default col-lg-6 col-md-6 col-sm-6">Rücksetzen</button>
                                <button type="submit" class="btn btn-primary col-lg-6 col-md-6 col-sm-6">Account anlegen</button>
                                <!-- <input type="submit" value="Account anlegen" class="btn btn-primary col-lg-6 col-md-6 col-sm-6"> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>

    <script src="js/main.js"></script>
</body>

</html>