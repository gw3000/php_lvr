<?php

/**
 * Financial Service Provider
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

$sql = 'SELECT id, selection, presentation, opponent, country, id_ram
          FROM public.t_isr ORDER BY id ASC;';
$stmt = $pdo->query($sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/datatables.min.css" />
    <link rel="stylesheet" href="css/fontawesome.all.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jsquery
        $(document).ready(function() {
            $('#isreditorTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Alle"]
                ],
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 650,
                "scrollX": true,
            });
        });
    </script>
</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <?php require_once 'includes/modals/isreditorModal.php'; ?>
        <div class="page-header" id="banner">
            <h3 class="h3-spacing">Übersicht aller Versicherer</h3>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <table id="isreditorTable" class="display table table-bordered table-striped table-hover cell-border tabley" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Versicherer (Auswahlmenü)</th>
                                <th>Versicherer bei Vertragsabschluss</th>
                                <th>Gegner</th>
                                <th>Land</th>
                                <th>RAM ID</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <div class="custom-control custom-radio custom-control-inline">
                                <?php
                                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                    echo "<tr>";
                                    echo "<td>" . $row['id'] . "</td>";
                                    echo "<td>" . $row['selection'] . "</td>";
                                    echo "<td>" . $row['presentation'] . "</td>";
                                    echo "<td>" . $row['opponent'] . "</td>";
                                    echo "<td>" . $row['country'] . "</td>";
                                    echo "<td>" . $row['id_ram'] . "</td>";
                                    echo "<td><center><button type='button' onclick = \"loadModModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#isrModModal'><i class='fas fa-edit'></i></button></center></td>";
                                    echo "<td><center><button type='button' onclick = \"loadDelModal(" . $row['id'] . ")\" class='btn-trnsp' data-toggle='modal' data-target='#isrDelModal'><i class='fas fa-eraser'></i></button></center></td>";
                                    echo "</tr>";
                                }
                                ?>
                            </div>
                        </tbody>
                    </table>
                    <button id="addStatus" type="button" data-toggle='modal' data-target='#isrAddModal' class="btn btn-info">Versicherer hinzufügen</button>
                    <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
                    <?php
                    // close statement
                    unset($stmt);
                    // close connection
                    unset($pdo);
                    ?>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/isreditor.js"></script>
    <script src="js/main.js"></script>
    <script src="js/mousewheel.js"></script>
</body>

</html>
