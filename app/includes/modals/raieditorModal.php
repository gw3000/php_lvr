<?php

/**
 * RSV Status Editor Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2021 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal mod rainum -->
<div class="modal fade" id="raiModModal" tabindex="-1" role="dialog" aria-labelledby="raiModModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="raiModModalCenterTitle">RA-Micro Adressnummern ändern</h4>
                <input id="rsIdMod" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- ram address id -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>RA-Micro Adress-Nummer (alt)</h5>
                    </label>
                    <input type="number" disabled id="mod_id" class="form-control">
                </div>
                <!-- ram address id 2 -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>RA-Micro Adress-Nummer (neu)</h5>
                    </label>
                    <input type="number" id="mod_id" class="form-control">
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="raiModModalSave" class="btn btn-success">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>