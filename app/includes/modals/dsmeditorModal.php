<?php

/**
 * Mandatsstatus Editor Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal add status -->
<div class="modal fade" id="dsmAddModal" tabindex="-1" role="dialog" aria-labelledby="msEditorModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dsmEditorModalCenterTitle">Mandatsstatus Kündigung &middot; Eintrag  hinzufügen</h4>
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- id -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Status ID</h5>
                    </label>
                    <input type="number" id="id" class="form-control">
                </div>

                <!-- status -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="description"></textarea>
                </div>

                <!-- hinweistext kommunikation -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Kommunikation</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="helptext"></textarea>
                </div>

                <!-- hinweistext plattform -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Plattform<small>(128 Zeichen)</small></h5>
                    </label>
                    <textarea class="form-control" rows="2" maxlength="128" id="helptext_pf"></textarea>
                </div>

                <!-- hinweistext bearbeiter -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Bearbeiter</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="helptext_wk"></textarea>
                </div>

                <!-- deadline -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Frist der Bearbeitung (in Tagen)</small></h5>
                    </label>
                    <input class="form-control" type="number" min="0" max="365" id="deadline"></input>
                </div>

                <!-- after deadline -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Folge bei Fristablauf</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="after_deadline"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="dsmModalSave" class="btn btn-success">Status speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal mod status -->
<div class="modal fade" id="dsmModModal" tabindex="-1" role="dialog" aria-labelledby="msModModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dsmModModalCenterTitle">Mandatsstatus Kündigung &middot; Eintrag  ändern</h4>
                <input id="dsmIdMod" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- id -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Status ID</h5>
                    </label>
                    <input type="number" disabled id="mod_id" class="form-control">
                </div>

                <!-- description -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="mod_description"></textarea>
                </div>

                <!-- hinweistext kommunikation aenderung -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Kommunikation</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="mod_helptext"></textarea>
                </div>

                <!-- hinweistext plattform aenderung -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Plattform <small>(128 Zeichen)</small></h5>
                    </label>
                    <textarea class="form-control" rows="2" maxlength="128" id="mod_helptext_pf"></textarea>
                </div>

                <!-- hinweistext bearbeiter aenderung -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Hinweistext Bearbeiter</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="mod_helptext_wk"></textarea>
                </div>

                <!-- deadline -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Frist der Bearbeitung <small>(in Tagen)</small></h5>
                    </label>
                    <input class="form-control" type="number" min="0" max="365" id="mod_deadline"></input>
                </div>

                <!-- after deadline -->
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Folge bei Fristablauf</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="mod_after_deadline"></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-success" id="dsmModModalSave">Eintrag speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal delete status -->
<div class="modal fade" id="dsmDelModal" tabindex="-1" role="dialog" aria-labelledby="msDelModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dsmDelModalCenterTitle">Mandatsstatus Kündigung &middot; Eintrag löschen</h4>
                <input id="dsmIdDel" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <p>Wollen Sie folgenden Mandatsstatus wirklich löschen?</p>
                <div id="del_id">ID: </div>
                <div id="del_description">Status: </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="dsmDelete" class="btn btn-primary">Status löschen</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal user add or delete status -->
<div class="modal fade" id="dsmUserModal" tabindex="-1" role="dialog" aria-labelledby="msUserModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dsmUserModalCenterTitle">Mandatsstatus Kündigung &middot; Bearbeiter hinzufügen/entfernen</h4>
                <input id="dsmIdUser" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="dsmEditorModalLabelText" class="control-label">
                        <h5>Bearbeiter hinzufügen oder löschen</h5>
                    </label>
                    <p>
                        <select id="mod_add_worker"></select>
                        <button class='btn btn-sm btn-success' id="addWorker">Hinzufügen</button>
                        <button class='btn btn-sm btn-primary' id="delWorker">Entfernen</button>
                    </p>
                    <p>
                        <ul id='mod_show_worker'></ul>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" id='breakUpUser' class="btn btn-default" data-dismiss="modal">Beenden</button>,
                </div>
            </div>
        </div>
    </div>
</div>