<?php

/**
 * Not Submitted Editor Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2022 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal add status -->
<div class="modal fade" id="nsDelModal" tabindex="-1" role="dialog" aria-labelledby="nsEditorModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="nsEditorModalCenterTitle">Nicht eingereichten Vertrag löschen <small class="state-header">(auf der Plattform)</small></h4>
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
				<!-- hidden contid -->
				<input hidden="true" type="text" id="contid">
                <!-- contnum -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Vertragsnummer</h5>
                    </label>
                    <input disabled="true" type="text" id="contnum" class="form-control">
                </div>
                <!-- Aktenzeichen -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Aktenzeichen</h5>
                    </label>
                    <input disabled="true" type="text" id="refnum" class="form-control">
                </div>
                <!-- Status -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <input disabled="true" type="text" id="status" class="form-control">
                </div>
                <!-- Name Mandant -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Name (Mandant)</h5>
                    </label>
                    <input disabled="true" type="text" id="nameCustomer" class="form-control">
                </div>
                <!-- Name Vermittler -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Name (Vermittler)</h5>
                    </label>
                    <input disabled="true" type="text" id="nameAgent" class="form-control">
                </div>
                <!-- Vermittlerfirma -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Firma (Vermittler)</h5>
                    </label>
                    <input disabled="true" type="text" id="company" class="form-control">
                </div>
                <!-- Datum der Markierung zum löschen -->
                <div hidden="true" class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Datum der Markierung zum löschen</h5>
                    </label>
                    <input disabled="true" type="text" id="marked_to_delete" class="form-control">
                </div>
                <!--Checkbox der Markierung zum löschen -->
                <div class="form-group">
                    <label for="nsEditorModalLabelText" class="control-label">
                        <h5>Markieren zum Löschen</h5>
                    </label>
                    <div class="alert alert-dismissible alert-danger">
                        <strong>Warnung!</strong><p> Sind Sie sicher, dass Sie den Vertrag von der Plattform löschen wollen?</p>
                        <label class="form-check-label" for="check_to_delete"><strong>Ja, löschen: </strong></label>
                        <input class="form-check-input" type="checkbox" value="" id="check_to_delete">
                    </div>
				</div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="nsModalSave" class="btn btn-primary">zum Löschen markieren</button>
                </div>
            </div>
        </div>
    </div>
</div>

