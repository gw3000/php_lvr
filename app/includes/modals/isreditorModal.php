<?php

/**
 * RSV Status Editor Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- ###################################################
################### Modal add status ###################
######################################################## -->
<div class="modal fade" id="isrAddModal" tabindex="-1" role="dialog" aria-labelledby="isrEditorModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="isrEditorModalCenterTitle">Finanzdienstleister hinzufügen</h4>
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">

                <!-- selection -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Versicherer (Auswahlmenü)</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="selectionAdd"></textarea>
                </div>

                <!-- presentation -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Versicherer bei Vertragsabschluss</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="presentationAdd"></textarea>
                </div>

                <!-- opponent -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Gegner</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="opponentAdd"></textarea>
                </div>

                <!-- country -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Land</h5>
                    </label>
                    <select id="countryAdd" class="form-control">
                        <option value="Deutschland">Deutschland</option>
                        <option value="Österreich">Österreich</option>
                    </select>
                </div>

                <!-- ram id -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Ra-Micro Adressnummer</h5>
                    </label>
                    <input type="number" class="form-control" min="1" id="idRamAdd"></input>
                </div>

            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="isrModalAdd" class="btn btn-success">Status speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ###################################################
################### Modal mod status ###################
######################################################## -->
<div class="modal fade" id="isrModModal" tabindex="-1" role="dialog" aria-labelledby="isrModModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="isrModModalCenterTitle">Finanzdienstleister ändern</h4>
                <input id="isrIdMod" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">

                <!-- selection -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Bezeichnung bei Auswahl</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="selectionMod"></textarea>
                </div>

                <!-- presentation -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Bezeichnung bei Darstellung</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="presentationMod"></textarea>
                </div>

                <!-- opponent -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Gegner</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="opponentMod"></textarea>
                </div>

                <!-- country -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Land</h5>
                    </label>
                    <select id="countryModSel" class="form-control">
                        <option id="countryMod"></option>
                        <option disabled>-------------</option>
                        <option value="Deutschland">Deutschland</option>
                        <option value="Österreich">Österreich</option>
                    </select>
                </div>

                <!-- ram id -->
                <div class="form-group">
                    <label for="isrEditorModalLabelText" class="control-label">
                        <h5>Ra-Micro Adressnummer</h5>
                    </label>
                    <input type="number" class="form-control" min="1" id="idRamMod"></input>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="isrModModalSave" class="btn btn-success">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ###################################################
################### Modal del status ###################
######################################################## -->
<div class="modal fade" id="isrDelModal" tabindex="-1" role="dialog" aria-labelledby="isrDelModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="isrDelModalCenterTitle">Versicherer löschen</h4>
                <input id="isrIdDel" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <p>Wollen Sie folgenden Versicherer wirklich löschen?</p>
                <div id="isrIdDel" hidden="true"></div>
                <div id="selectionDel"></div>
                <div id="presentationDel"></div>
                <div id="opponentDel"></div>
                <div id="idRamDel"></div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="isrDelete" class="btn btn-primary">Dienstleister löschen</button>
                </div>
            </div>
        </div>
    </div>
</div>