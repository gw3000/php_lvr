<?php

/**
 * User Add Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal add user -->
<div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="userAddModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userAddModalCenterTitle">Benutzer hinzufügen</h4>
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- Name -->
                <div class="form-group">
                    <label for="userAddModalLabelText" class="control-label">
                        <h5>Benutzername (Vorname Name)</h5>
                    </label>
                    <input type="text" id="username" class="form-control">
                </div>

                <!-- email adresse -->
                <div class="form-group">
                    <label for="userAddModalLabelText" class="control-label">
                        <h5>Email Adresse</h5>
                    </label>
                    <input type="email" id="email" class="form-control">
                </div>

                <!-- passwort -->
                <div class="form-group">
                    <label for="userAddModalLabelText" class="control-label">
                        <h5>Passwort</h5>
                    </label>
                    <input type="password" id="pwd1" class="form-control"><br>
                    <label for="userAddModalLabelText" class="control-label">
                        <h5>Passwort wiederholen!</h5>
                    </label>
                    <input type="password" id="pwd2" class="form-control">
                </div>

                <!-- role -->
                <div class="form-group">
                    <label for="userAddModalLabelSelect" class="control-label">
                        <h5>Rolle</h5>
                    </label>
                    <select id="role" class="form-control">
                        <option value="user-ph">Telefon (Benutzer)</option>
                        <option value="user-fa">Fachangestellte (Benutzer)</option>
                        <option value="user-ra">Anwalt (Benutzer)</option>
                        <option value="manager">Manager</option>
                        <option value="insure">Manager Versicherung</option>
                        <option value="admin">Administrator</option>
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-success" id="userAddModalSave">Benutzer hinzufügen</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal mod user -->
<div class="modal fade" id="userModModal" tabindex="-1" role="dialog" aria-labelledby="userModModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userModModalCenterTitle">Benutzer ändern</h4>
                <input id="userIdMod" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- Name -->
                <div class="form-group">
                    <label for="userModModalLabelText" class="control-label">
                        <h5>Benutzername (Vorname Name)</h5>
                    </label>
                    <input type="text" id="usernameMod" class="form-control">
                </div>

                <!-- email adresse -->
                <div class="form-group">
                    <label for="userModModalLabelText" class="control-label">
                        <h5>Email Adresse</h5>
                    </label>
                    <input type="email" disabled id="emailMod" class="form-control">
                </div>

                <!-- passwort -->
                <div id="pwdChange">
                    <div class="form-group">
                        <label for="userModModalLabelText" class="control-label">
                            <h5>neues Passwort</h5>
                        </label>
                        <input type="password" id="userModPwd1" class="form-control"><br>

                        <label for="userModModalLabelText" class="control-label">
                            <h5>neues Passwort wiederholen!</h5>
                        </label>
                        <input type="password" id="userModPwd2" class="form-control">
                    </div>
                </div>

                <!-- role -->
                <div class="form-group">
                    <label for="userModModalLabelSelect" class="control-label">
                        <h5>Rolle</h5>
                    </label>
                    <select id="roleMod" class="form-control">
                        <option value="user-ph">Telefon (Benutzer)</option>
                        <option value="user-fa">Fachangestellte (Benutzer)</option>
                        <option value="user-ra">Anwalt (Benutzer)</option>
                        <option value="manager">Manager</option>
                        <option value="insure">Manager Versicherung</option>
                        <option value="admin">Administrator</option>
                    </select>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-info" id="showPwdChange">Passwort ändern</button>
                    <button type="button" class="btn btn-success" id="userModModalSave">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal del user -->
<div class="modal fade" id="userDelModal" tabindex="-1" role="dialog" aria-labelledby="userDelModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="userDelModalCenterTitle">Benutzer löschen</h4>
                <input id="userIdDel" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <p>Wollen Sie folgenden Benutzer wirklich löschen?</p>
                <div id="usernameDel"></div>
                <div id="emailDel"></div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-primary" id="UserDelete">Benutzer löschen</button>
                </div>
            </div>
        </div>
    </div>
</div>