<?php

/**
 * Modal template
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal -->
<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="commentModalCenterTitle">Status und Kommentar</h4>
                <div class="pull-right">
                    <span id="lastuserModalLabelSelect" class="label label-default"></span>
                    <span id="lastupdateModalLabelSelect" class="label label-primary"></span>
                </div>
            </div>
            <div class="modal-body">
                <!-- input text -->
                <div class="form-group">
                    <label for="commentModalLabelText" class="control-label">
                        <h5>Kommentar</h5>
                    </label>
                    <input type="text" id="commentModalLabelText" class="form-control">
                </div>

                <!-- select -->
                <div class="form-group">
                    <label for="commentModalLabelSelect" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <select id="commentModalLabelSelect" class="form-control">
                        <option value="noch nicht bearbeitet">noch nicht bearbeitet</option>
                        <option value="ungeprüft">ungeprüft</option>
                        <option value="geprüft">geprüft</option>
                        <option value="unklar">unklar</option>
                        <option value="keine gegenteiligen Indizien">keine gegenteiligen Indizien</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
                    <button type="button" id="commentModalSave" class="btn btn-success">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>