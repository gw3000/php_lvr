<?php

/**
 * RSV Status Editor Modal
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<!-- Modal add status -->
<div class="modal fade" id="rsAddModal" tabindex="-1" role="dialog" aria-labelledby="rsEditorModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="rsEditorModalCenterTitle">RSV-Status hinzufügen</h4>
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- id -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Status ID</h5>
                    </label>
                    <input type="number" id="id" class="form-control">
                </div>
                <!-- description -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="description"></textarea>
                </div>
                <!-- hilfetext -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Hilfetext</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="helptext"></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="rsModalSave" class="btn btn-success">Status speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal mod status -->
<div class="modal fade" id="rsModModal" tabindex="-1" role="dialog" aria-labelledby="rsModModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="rsModModalCenterTitle">RSV-Status ändern</h4>
                <input id="rsIdMod" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <!-- id -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Status ID</h5>
                    </label>
                    <input type="number" disabled id="mod_id" class="form-control">
                </div>
                <!-- description -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Status</h5>
                    </label>
                    <textarea class="form-control" rows="1" id="mod_description"></textarea>
                </div>
                <!-- hilfetext -->
                <div class="form-group">
                    <label for="rsEditorModalLabelText" class="control-label">
                        <h5>Hilfetext</h5>
                    </label>
                    <textarea class="form-control" rows="2" id="mod_helptext"></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="rsModModalSave" class="btn btn-success">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal del status -->
<div class="modal fade" id="rsDelModal" tabindex="-1" role="dialog" aria-labelledby="rsDelModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="rsDelModalCenterTitle">RSV-Status löschen</h4>
                <input id="rsIdDel" type="hidden">
                <div class="pull-right">
                </div>
            </div>
            <div class="modal-body">
                <p>Wollen Sie folgenden Mandatsstatus wirklich löschen?</p>
                <div id="del_id">ID: </div>
                <div id="del_description">Status: </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Abbrechen</button>
                    <button type="button" id="rsDelete" class="btn btn-primary">Status löschen</button>
                </div>
            </div>
        </div>
    </div>
</div>