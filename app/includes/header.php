<?php
/**
 * Header Doc Comment
 * PHP Version 7.
 *
 * @category  Partials
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// set default timezone
date_default_timezone_set('Europe/Berlin');

// Init session
if (!isset($_SESSION)) {
    session_start();
}

// Include db config
require_once 'includes/db.php';

// Validate login
if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    header('location: login.php');
    exit;
}
