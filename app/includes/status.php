<?php
/**
 * Status = Mandatsstatus + RSV
 * PHP Version 7
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<div class="form-group">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <!-- section mandatssstatus -->
            <div class="panel panel-default" id="myCollapsible">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseMS" aria-expanded="true" aria-controls="collapseMS">
                            Mandatsstatus
                        </a>
                    </h4>
                </div>
                <div id="collapseMS" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?php require "ms.php" ?>
                    </div>
                </div>
            </div>
            <!-- section rsv -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo" href="#collapseRSV">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseRSV" aria-expanded="false" aria-controls="collapseRSV">
                            Rechtsschutzstatus
                        </a>
                    </h4>
                </div>
                <div id="collapseRSV" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <?php require "rsv.php" ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>