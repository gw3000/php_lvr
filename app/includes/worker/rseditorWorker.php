<?php

/**
 * RSEDITOR worker to load and save client status
 * PHP Version 7
 *
 * @category  AJAX_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}

// validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    header("location: ../../login.php");
    exit;
} else {
    // include database credentials
    include_once "../db.php";
    // what is committed operation
    if ($_POST["op"] == "load") {
        // check if the required operation is "load"
        $id = $_POST["id"];
        $sql = "SELECT \"id\", \"description\", \"helptext\" FROM t_rsv_status WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "update") {
        // update entry
        $sql = "UPDATE t_rsv_status SET \"description\" = :description, helptext = :helptext WHERE \"id\" = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["mod_id"], PDO::PARAM_STR);
        $stmt->bindParam(":description", $_POST["mod_description"], PDO::PARAM_STR);
        // set helptext null if its empty
        if ($_POST["mod_helptext"] == "") {
            $stmt->bindParam(":helptext", $_POST["mod_helptext"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":helptext", $_POST["mod_helptext"], PDO::PARAM_STR);
        }

        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "save") {
        //check if email exist?
        $sql = "SELECT id FROM t_rsv_status WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        unset($stmt);
        if ($count == 0) {
            $sql = "INSERT INTO t_rsv_status (\"id\", \"description\", \"helptext\") VALUES (:id, :description, :helptext);";
            if ($stmt = $pdo->prepare($sql)) {
                // Bind params
                $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
                $stmt->bindParam(":description", $_POST["description"], PDO::PARAM_STR);
                $stmt->bindParam(":helptext", $_POST["helptext"], PDO::PARAM_STR);
                // Attempt to execute
                $stmt->execute();
            }
            unset($stmt);
        } else {
            echo "id_exist";
        }
        //echo json_encode($_POST);
    } elseif ($_POST["op"] == "delete") {
        // delete entry
        $sql = "DELETE FROM t_rsv_status WHERE \"id\" = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        unset($stmt);
    }
}
