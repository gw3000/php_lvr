<?php

/**
 * Comment worker to load and save comments
 * PHP Version 7
 *
 * @category  AJAX_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}
// Validate login
if (!isset($_SESSION['email']) || empty($_SESSION['email'])) {
    header('location: ../../login.php');
    exit;
} else {
    // include database credentials
    include_once '../db.php';
    // check if the required operation is "load"
    if ($_POST['op'] == 'load') {
        $id = $_POST['id'];
        $fid = $_POST['fid'];
        // query to load data from db in json format
        $sql = "SELECT checked->:fid->>'status' AS status,
                checked->:fid->>'comment' AS comment,
                checked->:fid->>'last_user' AS last_user,
                checked->:fid->>'last_update' AS last_update
            FROM t_cont_check
            WHERE id = :id;";
        // pdo stuff
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':fid', $fid, PDO::PARAM_STR);
        $stmt->bindParam(':id', $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        // return results in json format
        echo json_encode($row);
    } elseif ($_POST['op'] == 'save') {
        $today = date("Y-m-d  H:i:s");
        $sql = "UPDATE t_cont_check SET checked = jsonb_set(jsonb_set(jsonb_set(jsonb_set(checked, '{" . $_POST['fid'] . ", status}', '\"" . $_POST['status'] . "\"'), '{" . $_POST['fid'] . ", comment}', '\"" . $_POST['comment'] . "\"'), '{" . $_POST['fid'] . ", last_user}', '\"" . $_SESSION['name'] . "\"'), '{" . $_POST['fid'] . ", last_update}', '\"" . $today . "\"') where id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':id', $_POST['id'], PDO::PARAM_STR);
        $stmt->execute();
        echo json_encode($_POST);
    }
}
