<?php

/**
 * Comment worker to load and save comments
 * PHP Version 7
 *
 * @category  AJAX_Account_Workder
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}
// Validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    header("location: ../../login.php");
    exit;
} else {
    // include database credentials
    include_once "../db.php";
    // check if the required operation is "load"
    if ($_POST["op"] == "load") {
        $id = $_POST["id"];
        $sql = "SELECT \"id\", name, email, \"role\" FROM t_users WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "update") {
        // update entry
        echo $_POST["role"];
        if (isset($_POST["password"]) && $_POST["password"] != "") {
            $sql = "UPDATE t_users SET name = :name, role = :role, password = :password WHERE \"id\" = :id";
        } else {
            $sql = "UPDATE t_users SET name = :name, role = :role WHERE \"id\" = :id";
        }
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":name", $_POST["name"], PDO::PARAM_STR);
        $stmt->bindParam(":role", $_POST["role"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        if (isset($_POST['password']) && $_POST["password"] != "") {
            $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
        }
        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "save") {
        //check if email exist?
        $sql = "SELECT email FROM t_users WHERE email = :email";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":email", $_POST["email"], PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        unset($stmt);
        if ($count == 0) {
            $sql = "INSERT INTO t_users (\"name\", \"email\", \"password\", \"role\", \"created_at\") VALUES (:name, :email, :password, :role, :created_at);";
            if ($stmt = $pdo->prepare($sql)) {
                // Bind params
                $created_at = date("Y-m-d H:i:s");
                $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
                $stmt->bindParam(":name", $_POST["name"], PDO::PARAM_STR);
                $stmt->bindParam(":email", $_POST["email"], PDO::PARAM_STR);
                $stmt->bindParam(":password", $password, PDO::PARAM_STR);
                $stmt->bindParam(":role", $_POST["role"], PDO::PARAM_STR);
                $stmt->bindParam(":created_at", $created_at, PDO::PARAM_STR);
                // Attempt to execute
                $stmt->execute();
            }
            unset($stmt);
        } else {
            echo "password_exist";
        }
        //echo json_encode($_POST);
    } elseif ($_POST["op"] == "delete") {
        // delete entry from t_users
        $sql = "DELETE FROM t_users WHERE \"id\" = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        unset($stmt);

        // delete all entries from t_ms_users
        $sql = "DELETE FROM t_ms_users WHERE id_user = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        unset($stmt);
    }
}
