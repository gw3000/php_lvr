<?php

/**
 * Not Submitted worker to load and save contract deleting information
 * PHP Version 7
 *
 * @category  AJAX_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
	session_start();
}

// validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
	header("location: ../../login.php");
	exit;
} else {
	// include database credentials
	include_once "../db.php";
	// what is committed operation
	if ($_POST["op"] == "load") {
		// check if the required operation is "load"
		$id = $_POST["contid"];
		$sql = "SELECT
			tic.contid,
			tic.customerid,
			tic.contnum,
			tic.refnum,
			tic.status,
			tic.created_at,
			tia.lname,
			tia.fname,
			tia.street,
			tia.city,
			tia.zip,
			ta.lastname,
			ta.firstname,
			ta.company,
			tcid.marked_to_delete
		FROM
			t_import_contracts tic
		LEFT JOIN t_import_address tia ON
			tic.customerid = tia.id
		LEFT JOIN t_agents ta ON
			tic.created_by = ta.id
		LEFT JOIN t_cont_insapp_del tcid ON
			tic.contid = tcid.contid
		WHERE
			tic.contid = :id";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":id", $id, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		echo json_encode($row);
	} elseif ($_POST["op"] == "save") {
		//check if email exist?
		$id = $_POST["id"];
		$sql = "SELECT jsonb_agg(jobj) as history FROM(
				SELECT
					tic.customerid,
					tic.contnum,
					tic.refnum,
					tic.status,
					tic.created_at,
					tia.lname AS lastname_customer,
					tia.fname AS fname_customer,
					tia.street,
					tia.city,
					tia.zip,
					ta.lastname AS lname_agent,
					ta.firstname AS fname_agent,
					ta.company
				FROM
					t_import_contracts tic
				LEFT JOIN t_import_address tia ON
					tic.customerid = tia.id
				LEFT JOIN t_agents ta ON
					tic.created_by = ta.id
				WHERE
					tic.contid  = :id) as jobj";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":id", $id, PDO::PARAM_STR);
		$stmt->execute();
		$count = $stmt->rowCount();
		$arr = $stmt->fetch(PDO::FETCH_ASSOC);
		$obj = json_encode($arr);
		unset($stmt);
		if ($count == 1) {
			$sql = "INSERT INTO t_cont_insapp_del
					(contid, marked_to_delete, history)
				VALUES
					(:id, now(), :obj)";
			$stmt = $pdo->prepare($sql);
			$stmt->bindParam(":id", $id, PDO::PARAM_STR);
			$stmt->bindParam(":obj", $obj, PDO::PARAM_STR);
			$stmt->execute();
			unset($stmt);
		}
	} elseif ($_POST["op"] == "delete") {
		$id = $_POST["id"];
		$sql = "DELETE FROM public.t_cont_insapp_del
		WHERE contid=:id";
		$stmt = $pdo->prepare($sql);
		$stmt->bindParam(":id", $id, PDO::PARAM_STR);
		$stmt->execute();
		unset($stmt);
	}
}
