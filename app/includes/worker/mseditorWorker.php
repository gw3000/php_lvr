<?php

/**
 * MSEditor worker to load and save client status
 * PHP Version 7
 *
 * @category  AJAX_Account_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}
// validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    header("location: ../../login.php");
    exit;
} else {
    // include database credentials
    include_once "../db.php";

    if ($_POST["op"] == "load") {
        // check if the required operation is "load"
        $id = $_POST["id"];
        $sql = "SELECT ms.id,ms.description,ms.helptext,ms.helptext_pf,
			ms.helptext_wk,ms.deadline,ms.after_deadline
            FROM
				t_ms ms
			WHERE ms.id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $json_array = json_encode($load_arr);
        echo json_encode($row);
    } elseif ($_POST["op"] == "update") {
        // update entry
        $sql = "UPDATE t_ms SET \"description\" = :description, helptext = :helptext, helptext_pf = :helptext_pf, helptext_wk = :helptext_wk, deadline = :deadline, after_deadline = :after_deadline WHERE \"id\" = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["mod_id"], PDO::PARAM_STR);
        $stmt->bindParam(":description", $_POST["mod_description"], PDO::PARAM_STR);

        if ($_POST["mod_helptext"] == "") {
            $stmt->bindParam(":helptext", $_POST["mod_helptext"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":helptext", $_POST["mod_helptext"], PDO::PARAM_STR);
        }

        if ($_POST["mod_helptext_pf"] == "") {
            $stmt->bindParam(":helptext_pf", $_POST["mod_helptext_pf"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":helptext_pf", $_POST["mod_helptext_pf"], PDO::PARAM_STR);
        }

        if ($_POST["mod_helptext_wk"] == "") {
            $stmt->bindParam(":helptext_wk", $_POST["mod_helptext_wk"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":helptext_wk", $_POST["mod_helptext_wk"], PDO::PARAM_STR);
        }

        if ($_POST["mod_deadline"] == "") {
            $stmt->bindParam(":deadline", $_POST["mod_deadline"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":deadline", $_POST["mod_deadline"], PDO::PARAM_STR);
        }

        if ($_POST["mod_after_deadline"] == "") {
            $stmt->bindParam(":after_deadline", $_POST["mod_after_deadline"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":after_deadline", $_POST["mod_after_deadline"], PDO::PARAM_STR);
        }

        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "save") {
        //check if email exist?
        $sql = "SELECT id FROM t_ms WHERE id = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        $count = $stmt->rowCount();
        unset($stmt);
        if ($count == 0) {
            $sql = "INSERT INTO t_ms (\"id\", \"description\", \"helptext\",
                \"helptext_pf\", \"helptext_wk\", \"deadline\", \"after_deadline\")
                VALUES (:id, :description, :helptext, :helptext_pf,
                :helptext_wk, :deadline, :after_deadline);";
            if ($stmt = $pdo->prepare($sql)) {
                // Bind params
                $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
                $stmt->bindParam(":description", $_POST["description"], PDO::PARAM_STR);

                if ($_POST["helptext"] == "") {
                    $stmt->bindParam(":helptext", $_POST["helptext"] = null, PDO::PARAM_INT);
                } else {
                    $stmt->bindParam(":helptext", $_POST["helptext"], PDO::PARAM_STR);
                }

                if ($_POST["helptext_pf"] == "") {
                    $stmt->bindParam(":helptext_pf", $_POST["helptext_pf"] = null, PDO::PARAM_INT);
                } else {
                    $stmt->bindParam(":helptext_pf", $_POST["helptext_pf"], PDO::PARAM_STR);
                }

                if ($_POST["helptext_wk"] == "") {
                    $stmt->bindParam(":helptext_wk", $_POST["helptext_wk"] = null, PDO::PARAM_INT);
                } else {
                    $stmt->bindParam(":helptext_wk", $_POST["helptext_wk"], PDO::PARAM_STR);
                }

                if ($_POST["deadline"] == "") {
                    $stmt->bindParam(":deadline", $_POST["deadline"] = null, PDO::PARAM_INT);
                } else {
                    $stmt->bindParam(":deadline", $_POST["deadline"], PDO::PARAM_STR);
                }

                if ($_POST["after_deadline"] == "") {
                    $stmt->bindParam(":after_deadline", $_POST["after_deadline"] = null, PDO::PARAM_INT);
                } else {
                    $stmt->bindParam(":after_deadline", $_POST["after_deadline"], PDO::PARAM_STR);
                }

                // Attempt to execute
                $stmt->execute();
            }
            unset($stmt);
        } else {
            echo "id_exist";
        }
        //echo json_encode($_POST);
    } elseif ($_POST["op"] == "delete") {
        // delete entry
        $sql = "DELETE FROM t_ms WHERE \"id\" = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "usersSelection") {
        $sql = "SELECT id, \"name\" FROM t_users WHERE id !='39' ORDER BY name ASC;";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "usersList") {
        $id = $_POST["id"];
        $sql = "SELECT tus.\"name\" FROM t_ms_users tmu 
            INNER JOIN t_users tus ON
            tmu.id_user = tus.id 
            WHERE tmu.id_ms  = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "userAdd") {
        $id_ms = $_POST["msID"];
        $id_user = $_POST["userID"];
        $sql = "INSERT INTO t_ms_users (id_ms, id_user) VALUES (:id_ms, :id_user);";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id_ms", $id_ms, PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $id_user, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "userDel") {
        $id_ms = $_POST["msID"];
        $id_user = $_POST["userID"];
        $sql = "DELETE FROM t_ms_users WHERE id_ms= :id_ms AND id_user= :id_user;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id_ms", $id_ms, PDO::PARAM_STR);
        $stmt->bindParam(":id_user", $id_user, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    }
}
