<?php

require_once '../db.php';
require_once '../functions/dt_helpers.php';

// filter DE and AT from URL
if ($_COOKIE['affiliation'] == 'de') {
    $where = " WHERE cs.\"3a\" IN ('Deutschland','true') AND cb.last_update IS NOT NULL";
} elseif ($_COOKIE['affiliation'] == 'at') {
    $where = "  WHERE (cs.\"3a\" IS NULL OR cs.\"3a\" IN ('false')) AND cb.last_update IS NOT NULL";
} elseif ($_COOKIE['affiliation']=='unseen') {
    $where = "  WHERE cb.last_update IS NULL";
} else {
    $where = " WHERE 1=1";
}

$col = array(
    0 => 'akz',
    1 => 'lname',
    2 => 'fname',
    3 => 'mandate_status',
    4 => 'change_mandate_status',
    5 => 'deadline',
    6 => 'users',
    7 => 'after_deadline',
    8 => 'helptext_wk',
    9 => 'rsv_status',
    10 => 'rsv_last_update',
    11 => 'contnum_old',
    12 => 'contnum_new',
    13 => 'contract_upon_conclusion',
    14 => 'opponent',
    15 => 'kind_of_contract',
    16 => 'asset_type',
    17 => 'contracts_status',
    18 => 'technical_contract_start',
);

$sql = "SELECT
        co.id,
        co.refnum AS akz,
        cb.\"2b\" AS contnum_new,
        co.contnum AS contnum_old,
        ad.lname,
        ad.fname,
        ir.presentation AS contract_upon_conclusion,
        ir.opponent,
        cs.\"3b\" AS kind_of_contract,
        cd.\"14b\" AS asset_type,
        cs.\"3c\" AS contracts_status,
        cd.\"14c\" AS technical_contract_start,
        concat (ms.id ,	' ',ms.description) AS mandate_status,
        cm.dt_ms AS change_mandate_status,
        replace(replace(replace(replace(text(array_agg(split_part(us.\"name\", ' ', 1) ORDER BY us.\"name\" asc)), '{', ''), '}', ''), ',', ', '), 'NULL', '') AS users,
        deadline_wowe(cm.dt_ms ,ms.deadline ) AS deadline,
        ms.after_deadline,
        ms.helptext_wk,
        rs.description AS rsv_status,
        date(rv.last_update) AS rsv_last_update
    FROM
        t_contracts co
    INNER JOIN t_address ad ON
        co.ida = ad.id
    INNER JOIN t_cont_bd cb ON
        co.id = cb.id
    INNER JOIN t_cont_vd cd ON
        co.id = cd.id
    left join t_isr ir ON
        cd.\"14a\" = ir.id
    INNER JOIN t_cont_sa cs ON
        co.id = cs.id
    INNER JOIN t_cont_ms cm ON
        co.id = cm.id
    INNER JOIN t_cont_rsv rv ON
        co.id = rv.id
    LEFT JOIN t_ms ms ON
        cm.id_ms = ms.id
    LEFT JOIN t_ms_users mu ON
        ms.id = mu.id_ms
    LEFT JOIN t_users us ON
        mu.id_user = us.id
    LEFT JOIN t_rsv_status rs ON
        rv.id_rsv_status = rs.id
    " . $where . "
    GROUP BY
        (co.id,
        co.refnum,
        cb.\"2b\",
        co.contnum,
        ad.lname,
        ad.fname,
        ir.presentation,
        ir.opponent,
        cs.\"3b\",
        cd.\"14b\",
        cs.\"3c\",
        cd.\"14c\",
        concat (ms.id, ' ', ms.description),
        cm.dt_ms,
        ms.deadline,
        ms.after_deadline,
        ms.helptext_wk,
        rs.description,
        date(rv.last_update)
    )";

// number of rows
$stmt = $pdo->prepare($sql);
$stmt->execute();
$rows_max = $stmt->rowCount();
$rows_filtered = $rows_max;

// filtering
if (!empty($_REQUEST['search']['value'])) {
    $sql =  "SELECT * FROM ( " . $sql . ") z_q WHERE";
    $sql .= " z_q.akz ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.kind_of_contract ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.contnum_new ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.contnum_old ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.lname ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.fname ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.contract_upon_conclusion ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.opponent ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.asset_type ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.contracts_status ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.helptext_wk ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.rsv_status ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.mandate_status ilike '%" . $_REQUEST['search']['value'] . "%' ";
    $sql .= " OR z_q.users ilike '%" . $_REQUEST['search']['value'] . "%' ";
    // date stuff
    if (is_date($_REQUEST['search']['value'])) {
        $sql .= " OR z_q.technical_contract_start IN ('" . $_REQUEST['search']['value'] . "') ";
        $sql .= " OR z_q.change_mandate_status IN ('" . $_REQUEST['search']['value'] . "') ";
        $sql .= " OR z_q.rsv_last_update IN ('" . $_REQUEST['search']['value'] . "') ";
    }
    // number stuff
    if (is_number($_REQUEST['search']['value'])) {
        $sql .= " OR z_q.deadline = '" . is_number($_REQUEST['search']['value']) . "' ";
        $sql .= " OR z_q.after_deadline = '" . is_number($_REQUEST['search']['value']) . "' ";
    }
}

$stmt = $pdo->prepare($sql);
$stmt->execute();
$rows_filtered = $stmt->rowCount();

// order
if ($_REQUEST['length'] != -1) {
    if ($col[$_REQUEST['order'][0]['column']] == "akz" && $_REQUEST['search']['value'] == "") {
        // if you want to order by akz the akz has to be splitted and ordered separately
        $sql .= " ORDER BY (RIGHT(co.refnum, 2), SUBSTRING(co.refnum, 0, STRPOS(co.refnum, '/'))) " . $_REQUEST['order'][0]['dir']   . " OFFSET " . $_REQUEST['start'] . " LIMIT " . $_REQUEST['length'] . "; ";
    } elseif ($col[$_REQUEST['order'][0]['column']] == "akz" && $_REQUEST['search']['value'] != "") {
        $sql .= " ORDER BY (RIGHT(z_q.akz, 2), SUBSTRING(z_q.akz, 0, STRPOS(z_q.akz, '/'))) " . $_REQUEST['order'][0]['dir']   . " OFFSET " . $_REQUEST['start'] . " LIMIT " . $_REQUEST['length'] . "; ";
    } else {
        $sql .= " ORDER BY " . $col[$_REQUEST['order'][0]['column']] . " " . $_REQUEST['order'][0]['dir'] . " OFFSET " . $_REQUEST['start'] . " LIMIT " . $_REQUEST['length'] . "; ";
    }
} else {
    $sql .= " ORDER BY " . $col[$_REQUEST['order'][0]['column']] . ";";
}

// fetch all results
$stmt = $pdo->prepare($sql);
$stmt->execute();

$data = array();
while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $subdata = array();
    $subdata[] = "<a href='contract.php?id=$row[id]'><strong>" . $row['akz'] . "</strong></a>";
    $subdata[] = $row['lname'];
    $subdata[] = $row['fname'];
    $subdata[] = $row['mandate_status'];
    $subdata[] = $row['change_mandate_status'];
    $subdata[] = $row['deadline'];
    $subdata[] = $row['users'];
    $subdata[] = $row['after_deadline'];
    $subdata[] = $row['helptext_wk'];
    $subdata[] = $row['rsv_status'];
    $subdata[] = $row['rsv_last_update'];
    $subdata[] = $row['contnum_old'];
    $subdata[] = $row['contnum_new'];
    $subdata[] = $row['contract_upon_conclusion'];
    $subdata[] = $row['opponent'];
    $subdata[] = $row['kind_of_contract'];
    $subdata[] = $row['asset_type'];
    $subdata[] = $row['contracts_status'];
    $subdata[] = $row['technical_contract_start'];
    $data[] = $subdata;
}

$json_data = array(
    "draw"              =>  intval($_REQUEST['draw']),
    "recordsTotal"      =>  intval($rows_max),
    "recordsFiltered"   =>  intval($rows_filtered),
    "data"              =>  $data
);

echo json_encode($json_data);
