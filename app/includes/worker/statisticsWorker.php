<?php

/**
 * Comment worker to load and save comments
 * PHP Version 7
 *
 * @category  AJAX_Statistics_Workder
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */


// include database credentials
require_once "../db.php";
// check if the required operation is "load"
if ($_POST["op"] == "uploadHistory") {
    $sql = "
            SELECT 1 as days, count(*) FROM t_cont_import WHERE inserted_at > NOW() - interval '1 days'
            UNION
            SELECT 7 as days, count(*) FROM t_cont_import WHERE inserted_at > NOW() - interval '7 days'
            UNION
            SELECT 30 as days,count(*) FROM t_cont_import WHERE inserted_at > NOW() - interval '30 days'
            UNION
            SELECT 90 as days,count(*) FROM t_cont_import WHERE inserted_at > NOW() - interval '90 days'
            UNION
            SELECT 365 as days,count(*) FROM t_cont_import WHERE inserted_at > NOW() - interval '365 days'
            ORDER by days;
        ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($row);
} elseif ($_POST["op"] == "uploadUserHistory") {
    $sql = "
			SELECT
				count(created_by) as counts,
				concat (ia.firstname , ' ' , ia.lastname, ' (', ia.company, ')') as agent
			FROM
				t_contracts co
			INNER JOIN public.t_import_agents ia ON
				co.created_by = ia.id
			GROUP BY
				created_by,
				concat (ia.firstname , ' ' , ia.lastname, ' (', ia.company, ')')
			ORDER BY
				counts DESC
			LIMIT 5;
    ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($row);
} elseif ($_POST["op"] == "isr") {
    $sql = "
			select
				ir.presentation,
				count(\"14a\") as counts
			from
				t_cont_vd cv
			inner join public.t_isr ir on
				cv.\"14a\" = ir.id
			group by
				ir.presentation
			order by
				counts desc
			limit 5;
    ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($row);
} elseif ($_POST["op"] == "uploadPerDay") {
    $sql = "
			select
				count(*) as counts,
				date(inserted_at)
			from
				t_cont_import
			group by
				date(inserted_at)
			order by
				date(inserted_at)
    ";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    echo json_encode($row);
}
