<?php

/**
 * Isr-editor worker to load, save and delete insurances
 * PHP Version 7
 *
 * @category  AJAX_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}

// validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    header("location: ../../login.php");
    exit;
} else {
    // include database credentials
    include_once "../db.php";
    // what is committed operation
    if ($_POST["op"] == "load") {
        // check if the required operation is "load"
        $id = $_POST["id"];
        $sql = "SELECT id, selection, presentation, opponent,
            country, id_ram FROM t_isr WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    } elseif ($_POST["op"] == "update") {
        // update entry
        $sql = "UPDATE t_isr SET selection=:selection,
            presentation=:presentation, opponent=:opponent,
            country=:country, id_ram=:idram WHERE id=:id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["mod_id"], PDO::PARAM_STR);
        $stmt->bindParam(":selection", $_POST["mod_selection"], PDO::PARAM_STR);
        $stmt->bindParam(":presentation", $_POST["mod_presentation"], PDO::PARAM_STR);
        $stmt->bindParam(":opponent", $_POST["mod_opponent"], PDO::PARAM_STR);
        $stmt->bindParam(":country", $_POST["mod_country"], PDO::PARAM_STR);
        // set idram null if its empty
        if ($_POST["mod_idram"] == "") {
            $stmt->bindParam(":idram", $_POST["mod_idram"] = null, PDO::PARAM_INT);
        } else {
            $stmt->bindParam(":idram", $_POST["mod_idram"], PDO::PARAM_STR);
        }

        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "save") {
        $sql = "INSERT INTO t_isr 
                    (selection, presentation, opponent, country, id_ram) 
                VALUES 
                    (:selection, :presentation, :opponent, :country, :idram);";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":selection", $_POST["selection"], PDO::PARAM_STR);
        $stmt->bindParam(":presentation", $_POST["presentation"], PDO::PARAM_STR);
        $stmt->bindParam(":opponent", $_POST["opponent"], PDO::PARAM_STR);
        $stmt->bindParam(":country", $_POST["country"], PDO::PARAM_STR);
        $stmt->bindParam(":idram", $_POST["idram"], PDO::PARAM_STR);
        // Attempt to execute
        $stmt->execute();
        unset($stmt);
    } elseif ($_POST["op"] == "delete") {
        // delete entry
        $sql = "DELETE FROM t_isr WHERE \"id\" = :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $_POST["id"], PDO::PARAM_STR);
        $stmt->execute();
        unset($stmt);
    }
}
