<?php

/**
 * RSEDITOR worker to load and save client status
 * PHP Version 7
 *
 * @category  AJAX_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// database connection
include_once "../db.php";

// sanitize function
include_once "../functions/sanitize.php";

// unset variables
unset($sql);
unset($stmt);

// contract id and user email
$contractID = trim($_POST['contractId'], '\"');
$currentUserEmail = trim($_POST['currentUserEmail'], '\"');

foreach ($_POST as $key => $value) {
    $tr = explode('__', $key);
    if ($tr[0] == 'target') {
        // sanitize value
        $value = sanitize($value);
        
        // tr = table -> row
        $table = $tr[1];
        $row = $tr[2];
       
        // build the query
        $sql = "UPDATE {$table} SET \"{$row}\" = :val, last_update = NOW(), last_user = :last_user WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':val', $value);
        $stmt->bindParam(':last_user', $currentUserEmail);
        $stmt->bindParam(':id', $contractID);
        $stmt->execute();
    }
}

$sql = "SELECT cr.last_update FROM t_cont_rsv cr WHERE cr.id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $contractID);
$stmt->execute();
$row_rsv = $stmt->fetch(PDO::FETCH_ASSOC);
$last_update = $row_rsv['last_update'];

// unset variables
unset($sql);
unset($stmt);

// return last update
echo json_encode($last_update);
