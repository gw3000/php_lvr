<?php

/**
 * MSLoadText worker to load MS Text and MS Help Text
 * PHP Version 7
 *
 * @category  AJAX_MSText_Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

if (!isset($_SESSION)) {
    session_start();
}
// validate login
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    header("location: ../../login.php");
    exit;
} else {
    // include database credentials
    include_once "../db.php";

    if ($_POST["op"] == "load") {
        // check if the required operation is "load"
        $id = $_POST["id"];
        $sql = "SELECT \"id\", \"description\", \"helptext\", \"helptext_pf\", \"helptext_wk\", \"after_deadline\" 
            FROM t_ms WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_STR);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        echo json_encode($row);
    }  //echo json_encode($_POST);
}
