<?php

/**
 * Mandatsstaus Doc Comment
 * PHP Version 7.
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// Mandatsstaus für Vertrag
$sql = 'SELECT
			cs.dt_ra,
			rv.id as id_rav,
			rv.description as rav,
			ms.id as id_ms,
			ms.description as ms,
			cs.dt_ms,
            cs.tp_nr,
            cs.ms_checked,
			ms.helptext as helptext,
			ms.helptext_pf as helptext_pf,
			ms.helptext_wk as helptext_wk,
			cs.mbd_cont_doc,
			cs.ass_doc,
			cs.auth,
            cs.org_police,
            cs.copy_id_card,
            ms.after_deadline,
            deadline_wowe(cs.dt_ms ,ms.deadline ) AS deadline
		FROM
			t_cont_ms cs
		LEFT JOIN t_rav rv ON
			cs.id_rav = rv.id
		LEFT JOIN t_ms ms ON
			cs.id_ms = ms.id
		WHERE
			cs.id = :id;';
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$row_ms = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// Kategorie Mandatsstatus
$sql = 'SELECT description AS category FROM t_ms WHERE id = (SELECT floor(id_ms/1000)*1000 FROM t_cont_ms AS tcm WHERE tcm.id=:idv)';
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':idv', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$cat = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// ist 14a besetzt? -- wurde eine Versichrung ausgewählt
$sql = "SELECT vd.\"14a\" FROM t_cont_vd vd WHERE vd.id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_14a = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

?>
<div class="list-group col-lg-12 col-md-12 col-sm-12">
<h4 class="list-group-item-heading">Mandatsstatus: Rückabwicklung/Ankauf</h4><br>
    <?php

    // well für Kategorie Mandatsstatus
    if (isset($cat['category'])) {
        echo "<div class='form-group'>";
        echo "<label for='whatMS' class='col-lg-2 control-label'>Kategorie des Mandatsstatus</label>";
        echo "<div class='col-lg-4'>";
        echo "<div id='whatMS' class='well well-sm'>" . $cat['category'] . ' </div>';
        echo '</div></div>';
    }

    // Array Mandatsstatus
    $sql = "SELECT id, concat(id, ' - ', description) as description FROM t_ms ORDER BY id ASC;";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    echo "<div class='form-group'>";
    echo "<label for='inputRSVNum' class='col-lg-2 control-label'>Mandatsstatus</label>";

    // Länge des Auswahlfeldes Mandatsstatus
    if (strlen($row_ms['ms']) >= 50 && strlen($row_ms['ms']) < 65) {
        echo "<div class='col-lg-5'>";
    } elseif (strlen($row_ms['ms']) >= 65 && strlen($row_ms['ms']) < 80) {
        echo "<div class='col-lg-6'>";
    } elseif (strlen($row_ms['ms']) >= 80) {
        echo "<div class='col-lg-7'>";
    } else {
        echo "<div class='col-lg-4'>";
    }

    // Auswahlfeld für Mandatsstatus
    if ($row_ms['id_ms'] != null) {
        $ms = $row_ms['id_ms'] . ' - ' . $row_ms['ms'];
    } else {
        $ms = '';
    }

    echo "<select onchange='dateOnChangeToday(\"inputDTMS\");changeWidth(this)' class='form-control' id='inputMS' name='target__t_cont_ms__id_ms'>";
    echo "<option value = '" . $row_ms['id_ms'] . "'>" . $ms . '</option>';
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        if ($res_14a['14a'] == '' && $row['id'] == 2100) {
            break;
        }
        if ($row['id'] % 1000 == 0) {
            echo "<optgroup id = 'optid_" . $row['id'] . "'label = '" . $row['description'] . "'>";
        }
        if ($row['id'] % 1000 != 0) {
            echo "<option value='" . $row['id'] . "'>" . $row['description'] . '</option>';
        }
        if ($row['id'] % 1000 == 0) {
            echo '</optgroup>';
        }
    }
    echo '</select></div></div>';
    ?>

    <!-- Datum der Mandatsstatus-Änderung -->
    <div class="form-group">
        <label for="inputDTMS" class="col-lg-2 control-label">Datum der Änderung des Mandatsstatus</label>
        <div class="col-lg-4">
            <input type="date" class="date-control" id="inputDTMS" name="target__t_cont_ms__dt_ms" value="<?php echo $row_ms['dt_ms']; ?>">
        </div>
    </div>

    <!-- Frist der Abarbeitung -->
    <div class="form-group hidden" id='deadlineForm'>
        <label for="deadlineInput" class="col-lg-2 control-label">Bearbeitungsfristablauf in Tagen</label>
        <div class="col-lg-4">
            <input type="number" hidden="true" id="deadlineVal" value="<?php echo $row_ms['deadline']; ?>">
            <input type="text" disabled class="form-control" id="deadlineInput" value="">
        </div>
    </div>


    <!-- Folge bei Fristablauf -->
    <?php
    if (isset($row_ms['after_deadline']) && strlen($row_ms['after_deadline']) > 0) {
        $msg_adl = $row_ms['after_deadline'];
    } else {
        $msg_adl = 'kein Eintrag vorhanden';
    }
    ?>
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Folge bei Fristablauf</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-danger' id='after_deadline'>
                <?php echo $msg_adl; ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-info' id='helptext'>
                <?php
                if (isset($row_ms['helptext']) && strlen($row_ms['helptext']) > 0) {
                    echo $row_ms['helptext'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext zum Mandatsstatus in der Plattform -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus in der Plattform</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-success' id='helptext_pf'>
                <?php
                if (isset($row_ms['helptext_pf']) && strlen($row_ms['helptext_pf']) > 0) {
                    echo $row_ms['helptext_pf'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext zum Mandatsstatus Bearbeiter -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus für Bearbeiter**in</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-warning' id='helptext_wk'>
                <?php
                if (isset($row_ms['helptext_wk']) && strlen($row_ms['helptext_wk']) > 0) {
                    echo $row_ms['helptext_wk'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Kerndokumente -->
    <div class="form-group">
        <label for="postedDocuments" class="col-lg-2 control-label">Kerndokumente</label>
        <div class="col-lg-4">
            <div class="checkbox">
                <?php
                // Vertragsunterlagen_MBD
                if (1 == $row_ms['mbd_cont_doc']) {
                    $check_mbd_cont_doc = 'checked';
                } else {
                    $check_mbd_cont_doc = '';
                }
                // Versicherungsunterlagen
                if (1 == $row_ms['ass_doc']) {
                    $check_ass_doc = 'checked';
                } else {
                    $check_ass_doc = '';
                }
                // Vollmacht
                if (1 == $row_ms['auth']) {
                    $check_auth = 'checked';
                } else {
                    $check_auth = '';
                }
                // Originalpolice
                if (1 == $row_ms['org_police']) {
                    $check_org_police = 'checked';
                } else {
                    $check_org_police = '';
                }
                // Ausweiskopie
                if (1 == $row_ms['copy_id_card']) {
                    $check_copy_id_card = 'checked';
                } else {
                    $check_copy_id_card = '';
                }
                ?>
                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__mbd_cont_doc'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__mbd_cont_doc' " <?php echo $check_mbd_cont_doc; ?> ">
                        Vertragsunterlagen MBD
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__ass_doc'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__ass_doc' " <?php echo $check_ass_doc; ?> ">
                        Versicherungsunterlagen
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__auth'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__auth' " <?php echo $check_auth; ?> ">
                        Vollmacht
                    </label>
                </div>

                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__org_police'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__org_police' " <?php echo $check_org_police; ?> ">
                        Originalpolice
                    </label>
                </div>
                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__copy_id_card'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__copy_id_card' " <?php echo $check_copy_id_card; ?> ">
                        Ausweiskopie
                    </label>
                </div>
            </div>
        </div>
    </div>

    <!-- Inhalt Rückabwicklung Schreiben an Versicherung -->
    <div class="form-group">
        <label for="inputRAV" class="col-lg-2 control-label">Rückabwicklung (Schreiben an Versicherung)</label>
        <div class="col-lg-4">
            <?php
            // Array Inhalt Rückabwicklung
            $sql = 'SELECT id, description FROM t_rav ORDER BY id ASC;';
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            // Auswahlfeld für Mandatsstatus
            echo "<select onchange='dateOnChangeToday(\"inputDTRA\")' class='form-control' id='inputRAV' name='target__t_cont_ms__id_rav' >";
            echo "<option value = '" . $row_ms['id_rav'] . "'>" . $row_ms['rav'] . '</option>';
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                echo "<option value='" . $row['id'] . "'>" . $row['description'] . '</option>';
            }
            echo '</select>';
            ?>
        </div>
    </div>

    <!-- Datum Rückabwicklung Schreiben an Versicherung -->
    <div class="form-group">
        <label for="inputDTRA" class="col-lg-2 control-label">Datum Rückabwicklung Schreiben an Versicherung</label>
        <div class="col-lg-4">
            <input type="date" class="date-control" id="inputDTRA" name="target__t_cont_ms__dt_ra" value="<?php echo $row_ms['dt_ra']; ?>">
        </div>
    </div>

    <!-- Vorlagennummer -->
    <div class="form-group">
        <label for="inputDTRA" class="col-lg-2 control-label">Vorlagennummer</label>
        <div class="col-lg-4">
            <div class="input-group">
                <span class="input-group-btn">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default" onclick="document.getElementById('inputTMPLNR').stepDown()" type="button">&ndash;</button>
                        </span>
                        <input type="number" class="form-control" id="inputTMPLNR" name="target__t_cont_ms__tp_nr" value="<?php echo $row_ms['tp_nr']; ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" onclick="document.getElementById('inputTMPLNR').stepUp()" type="button">&#43;</button>
                        </span>
                    </div>
                </span>
            </div>
        </div>
    </div>

    <!-- Mandatsstatus checked -->
    <div class="form-group">
        <label for="msChecked" class="col-lg-2 control-label">Belehrung geprüft</label>
        <div class="col-lg-4">
            <div class="checkbox">
                <?php
                // Vertragsunterlagen_MBD
                if (1 == $row_ms['ms_checked']) {
                    $check_ms_checked = 'checked';
                } else {
                    $check_ms_checked = '';
                }
                ?>
                <div class="checkbox">
                    <label>
                        <input type='hidden' value='' name='target__t_cont_ms__ms_checked'>
                        <input type='checkbox' value='1' name='target__t_cont_ms__ms_checked' " <?php echo $check_ms_checked; ?> ">
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
