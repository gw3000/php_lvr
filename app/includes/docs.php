<?php

/**
 * Abrechnung of contract Doc Comment
 * PHP Version 7.
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2021  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// build foldername
$dir = "docs/";
$folder_refnum = str_replace("/", "-", $_SESSION["refnum"]) . "/";
$dir = $dir . $folder_refnum;
?>

<div class="list-group col-lg-12 col-md-12 col-sm-12">
    <div class="list-group-item">
        <br>
        <h4 class="list-group-item-heading">Dokumente aus der Plattform</h4>
        <br>

        <?php
        // Open a directory, and read its contents
        if (is_dir($dir)) {
            $doklist = "<ul>";

            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Vollmacht_MBD.pdf" && $file != "Vollmacht_MBD.pdf" && $file != "Vertragsunterlagen_MBD.pdf") {
                        $doklist .= "<li><a target='_blank' href='" . $dir . $file . "'>" . $file . "</a></li>";
                    } elseif ($file == "Vollmacht_MBD.pdf") {
                        $vm = "<h4>Vollmacht</h4><ul><li><a target='_blank' href='" . $dir . $file . "'>" . $file . "</a></li></ul><br>";
                    } elseif ($file == "Vertragsunterlagen_MBD.pdf") {
                        $vu = "<h4>Vertragsunterlagen</h4><ul><li><a target='_blank' href='" . $dir . $file . "'>" . $file . "</a></li></ul><br>";
                    }
                }
                closedir($dh);
            }
            
            $doklist .= "</ul><br>";
            if (isset($doklist) && $doklist != "<ul></ul><br>") {
                echo $doklist;
            } else {
                echo '<span class="label label-primary">keine hochgeladenen Dokumente vorhanden</strong></span>';
            }

            if (isset($vm) && $vm != "") {
                echo $vm;
            }
            if (isset($vu) && $vu) {
                echo $vu;
            }
        } else {
            echo '<span class="label label-primary">keine Dokumente vorhanden</strong></span>';
        }
        ?>
    </div>
</div>