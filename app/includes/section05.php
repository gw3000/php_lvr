<?php

/**
 * Section 05 File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<fieldset>
    <?php
    // Tabellen name in der Datenbank
    $table = "t_cont_vd";

    // Platformdaten Datenabfrage aus DB
    $sql = "SELECT
        id,
        data->'a14aResponse'->>'value' as \"14a\",
        data->'a14bResponse'->>'value' as \"14b\",
        data->'a14cResponse'->>'value' as \"14c\",
        data->'a14dResponse'->>'value' as \"14d\",
        data->'a14eResponse'->>'value' as \"14e\",
        data->'a14fResponse'->>'value' as \"14f\",
        data->'a14gResponse'->>'value' as \"14g\",
        data->'a14hResponse'->>'value' as \"14h\",
        data->'a14iResponse'->>'value' as \"14i\",
        replace(data->'a14jResponse'->>'value', ',','.') as \"14j\",
        data->'a14kResponse'->>'value' as \"14k\",
        data->'a14lResponse'->>'value' as \"14l\",
        data->'a14mResponse'->>'value' as \"14m\",
        data->'a14nResponse'->>'value' as \"14n\",
        data->'a14oResponse'->>'value' as \"14o\",
        data->'a14pResponse'->>'value' as \"14p\",
        case when data->'a14qResponse'->>'value'='ja' then 'true' when data->'a14qResponse'->>'value'='nein' then 'false' end as \"14q\",
        case when data->'a14rResponse'->>'value'='ja' then 'true' when data->'a14rResponse'->>'value'='nein' then 'false' end as \"14r\",
        data->'a14sResponse'->>'value' as \"14s\",
        data->'a14tResponse'->>'value' as \"14t\",
        case when data->'a14uResponse'->>'value'='ja' then 'true' when data->'a14uResponse'->>'value'='nein' then 'false' end as \"14u\",
        data->'a14vResponse'->>'value' as \"14v\",
        replace(replace(data->'a14wResponse'->>'value', '.', ''), ',', '.') as \"14w\",
        data->'a15aResponse'->>'value' as \"15\",
        data->'a16aResponse'->>'value' as \"16\",
        data->'a17aResponse'->>'value' as \"17\"
    FROM
        t_contracts
    WHERE
        id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_pf = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Datenbak Abfrage
    $sql = "SELECT
		vd.id,vd.\"14a\",ir.selection,ir.presentation,ir.opponent,ir.id_ram,
		vd.\"14b\",vd.\"14c\",vd.\"14d\",vd.\"14e\",vd.\"14g\",vd.\"14h\",
		vd.\"14i\",vd.\"14j\",vd.\"14k\",vd.\"14l\",vd.\"14m\",vd.\"14p\",
		vd.\"14q\",vd.\"14r\",vd.\"14s\",vd.\"14t\",vd.\"14u\",	vd.\"14v\",
		vd.\"14w\",vd.\"15\",vd.\"17\",vd.last_update,vd.last_user
	FROM
		t_cont_vd vd
	LEFT JOIN t_isr ir ON
		vd.\"14a\" = ir.id
	WHERE
		vd.id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_db = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Formular Teil Vertragsdetails
    text_input_ins('14a', 'Versicherer bei Vertragsabschluss', 'Versicherung bei Auswahl (links) Gegner (rechts)', $res_pf['14a'], $res_db['14a'], $res_db['presentation'], $res_db['selection'], $res_db['opponent'], $table);
    radio('14b', 'Anlageart', ['klassisch mit Garantieverzinsung', 'fondsgebunden', 'Mischform aus klassisch und fondsgebunden', 'nicht bekannt'], $res_pf['14b'], $res_db['14b'], $table);
    date_input('14c', 'technischer Vertragsbeginn', $res_pf['14c'], $res_db['14c'], $table);
    text_number('14d', 'bei Vertragsschluss vereinbarte Zahlungsdauer', '1', '0', '60', $res_pf['14d'], $res_db['14d'], $table);
    date_input('14e', 'Beendigungsdatum', $res_pf['14e'], $res_db['14e'], $table);
    radio('14g', 'Beitragszahlweise', ['Einmalzahlung', 'Ratenzahlung'], $res_pf['14g'], $res_db['14g'], $table);
    text_number('14h', 'Einmalzahlung Höhe', '0.01', '0', '1000000', $res_pf['14h'], $res_db['14h'], $table);
    radio('14i', 'Ratenzahlung Art', ['monatlich', 'vierteljährlich', 'halbjährlich', 'jährlich'], $res_pf['14i'], $res_db['14i'], $table);
    text_number('14j', 'Ratenzahlung Höhe erste Rate', '0.01', '0', '1000000', $res_pf['14j'], $res_db['14j'], $table);
    radio('14k', 'Ratenzahlung Änderungen', ['Die Höhe der Raten ist über die gesamte Zahlungsdauer konstant geblieben (außer eventuelle Beitragsfreistellungen).', 'Die Höhe der Raten hat sich während der Zahlungsdauer verändert (z.B. durch Dynamiken, Beitragserhöhung, Beitragssenkung).'], $res_pf['14k'], $res_db['14k'], $table);
    text_area('14l', 'Ratenzahlungen (Zeiträume und Beträge)', $res_pf['14l'], $res_db['14l'], $table);
    radio('14m', 'Beitragsfreistellung/en', ['keine Beitragsfreistellung', 'eine/mehrere Beitragsfreistellung/en'], $res_pf['14m'], $res_db['14m'], $table);
    text_input('14p', 'Beitragsfreistellung Dauer', $res_pf['14p'], $res_db['14p'], $table);
    radio('14q', 'Darlehen von der Lebensversicherung', ['Ja', 'Nein'], $res_pf['14q'], $res_db['14q'], $table);
    radio('14r', 'Auszahlungen während der Vertragslaufzeit', ['Ja', 'Nein'], $res_pf['14r'], $res_db['14r'], $table);
    radio('14s', 'Auszahlung Art', ['einmalig', 'mehrmalig'], $res_pf['14s'], $res_db['14s'], $table);
    text_number('14t', 'Auszahlung Höhe gesamt', '0.01', '0', '1000000', $res_pf['14t'], $res_db['14t'], $table);
    radio('14u', 'Auszahlung nach Beendigung des Vertrages', ['Ja', 'Nein'], $res_pf['14u'], $res_db['14u'], $table);
    date_input('14v', 'Auszahlung Datum', $res_pf['14v'], $res_db['14v'], $table);
    text_number('14w', 'Auszahlung Höhe', '0.01', '0', '1000000', $res_pf['14w'], $res_db['14w'], $table);
    text_area('15', 'Weitere Angaben', $res_pf['15'], $res_db['15'], $table);

    // Prüfung der Verwertung Logik und Variable
    if ($res_pf['16']) {
        if ($res_pf['16'] === 'true') {
            $vd_16 = '<div class="radio-inline"><label><input disabled="" type="radio" name="source__t_cont_vd__16"  value="true" checked >Ja</label></div>
            <div class="radio-inline"><label><input disabled="" type="radio" name="source__t_cont_vd__16"  value="false" >Nein</label></div>';
        } else {
            $vd_16 = '<div class="radio-inline"><label><input disabled="" type="radio" name="source__t_cont_vd__16"  value="true" >Ja</label></div>
            <div class="radio-inline"><label><input disabled="" type="radio" name="source__t_cont_vd__16"  value="false" checked >Nein</label></div>';
        }
    } else {
        $vd_16 = "<input type='text' class='form-control' value='nicht erfasst, da nicht Teil der Eingabestrecke.' disabled=''>";
    }

    ?>

    <!-- Prüfung der Verwertung -->
    <div class="form-group"><label class="col-lg-2 control-label">
            <div class="text-secondary">Prüfung der Verwertung <small>(16)</small></div>
        </label>
        <div class="col-lg-8">
            <?php echo $vd_16; ?>
        </div>
    </div>
    <hr>

    <?php

    // Prüfung der Kündigung Logik und Variable
    radio('17', 'Prüfung der Kündigung', ['Ja', 'Nein'], $res_pf['17'], $res_db['17'], $table);
    ?>
</fieldset>