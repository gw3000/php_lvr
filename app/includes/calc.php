<?php

/**
 * Abrechnung of contract Doc Comment
 * PHP Version 7.
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// Tabellen name in der Datenbank
$table = "t_cont_calc";

// Mandatsstaus für Vertrag
$sql = "SELECT
            ca.deduct_costs,
            ca.rem_tax_demg,
            co.data->'a18aResponse'->>'value' as \"18a\",
            co.data->'a18bResponse'->>'value' as \"18b\",
            co.data->'a18cResponse'->>'value' as \"18c\",
            co.data->'a18dResponse'->>'value' as \"18d\",
            co.data->'a18eResponse'->>'value' as \"18e\",
            co.data->'a18fResponse'->>'value' as \"18f\",
            co.data->'a18gResponse'->>'value' as \"18g\",
            co.data->'a18hResponse'->>'value' as \"18h\",
            co.data->'a18iResponse'->>'value' as \"18i\",
            co.data->'a18jResponse'->>'value' as \"18j\",
            co.data->'a18kResponse'->>'value' as \"18k\",
            co.data->'a18lResponse'->>'value' as \"18l\",
            co.data->'a18mResponse'->>'value' as \"18m\"
        FROM
            t_cont_calc ca
        INNER JOIN
            t_contracts co
        ON
            ca.id = co.id
        WHERE
            ca.id = :id";

$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$row_calc = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);
?>

<div class="form-group">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
            <!-- section 01 -->
            <div class="panel panel-default" id="myCollapsible">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsBilling" aria-expanded="true" aria-controls="collapsBilling">
                            Abrechnung
                        </a>
                    </h4>
                </div>

                <div id="collapsBilling" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <h4 class="list-group-item-heading">Abrechnungsdetails</h4><br>
                        <?php
                        $name = 'Abrechnungsdetails';
                        $col_description = array(['deduct_costs', 'kein Abzug von Kosten'], ['rem_tax_demg', 'verbleibender Steuerschaden']);
                        foreach ($col_description as $description) {
                            if (1 == $row_calc[$description[0]]) {
                                $check = 'checked';
                            } else {
                                $check = '';
                            }
                            echo "<div class='checkbox " . $name . "'><label><input type='hidden' value='' name='target__" . $table . '__' . $description[0] . "'><input type='checkbox' value='1' " . $check . " name='target__" . $table . '__' . $description[0] . "'> " . $description[1] . '</label></div>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- section 02 -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo" href="#collapsAccounts">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsAccounts" aria-expanded="false" aria-controls="collapsAccounts">
                            Konten
                        </a>
                    </h4>
                </div>
                <div id="collapsAccounts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <h4 class="list-group-item-heading">Kontodaten des ersten Kontos</h4><br>
                        <?php
                        one_line_input('dt_chg_status', 'Kontoinhaber', $row_calc['18a'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Bank', $row_calc['18b'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'IBAN', $row_calc['18c'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'BIC', $row_calc['18d'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Betreff', $row_calc['18e'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Gesamtbetrag', $row_calc['18f'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Betrag', $row_calc['18g'], $table, 'text', false);
                        ?>

                        <h4 class="list-group-item-heading">Kontodaten des zweiten Kontos</h4><br>
                        <?php
                        one_line_input('dt_chg_status', 'Kontoinhaber', $row_calc['18h'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Bank', $row_calc['18i'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'IBAN', $row_calc['18j'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'BIC', $row_calc['18k'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Betreff', $row_calc['18l'], $table, 'text', false);
                        one_line_input('dt_chg_status', 'Betrag', $row_calc['18m'], $table, 'text', false);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>