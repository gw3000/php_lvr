<?php

/**
 * Mandatsstaus Doc Comment
 * PHP Version 7.
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// Mandatsstaus für Vertrag
$sql = 'SELECT 
            cds.id,
            cds.id_dsm,
            cds.dt_dsm,
            dsm.description as desc_dsm,
            dsm.helptext as ht_dsm,
            dsm.helptext_pf as htpf_dsm,
            dsm.helptext_wk as htwk_dsm,
            dsm.after_deadline as afdl_dsm
        FROM 
            t_cont_dismissal cds
        LEFT JOIN 
            t_dismissal dsm 
        ON 
            cds.id_dsm = dsm.id
        WHERE 
            cds.id = :id;';
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$row_ms = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// Kategorie Mandatsstatus
$sql = 'SELECT description AS category FROM t_dismissal WHERE id = (SELECT floor(id_ms/1000)*1000 FROM t_cont_ms AS tcm WHERE tcm.id=:idv)';
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':idv', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$cat = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);


?>
<div class="list-group col-lg-12 col-md-12 col-sm-12">
    <h4 class="list-group-item-heading">Mandatsstatus: Kündigung</h4><br>

    <?php

    // well für Kategorie Mandatsstatus
    if (isset($cat['category'])) {
        echo "<div class='form-group'>";
        echo "<label for='whatMS' class='col-lg-2 control-label'>Kategorie des Mandatsstatus</label>";
        echo "<div class='col-lg-4'>";
        echo "<div id='dsm_whatMS' class='well well-sm'>" . $cat['category'] . ' </div>';
        echo '</div></div>';
    }

    // Array Mandatsstatus
    $sql = "SELECT id, concat(id, ' - ', description) as description FROM t_dismissal ORDER BY id ASC;";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();

    echo "<div class='form-group'>";
    echo "<label for='inputRSVNum' class='col-lg-2 control-label'>Mandatsstatus</label>";

    // Länge des Auswahlfeldes Mandatsstatus
    if (strlen($row_ms['desc_dsm']) >= 50 && strlen($row_ms['desc_dsm']) < 65) {
        echo "<div class='col-lg-5'>";
    } elseif (strlen($row_ms['desc_dsm']) >= 65 && strlen($row_ms['desc_dsm']) < 80) {
        echo "<div class='col-lg-6'>";
    } elseif (strlen($row_ms['desc_dsm']) >= 80) {
        echo "<div class='col-lg-7'>";
    } else {
        echo "<div class='col-lg-4'>";
    }

    // Auswahlfeld für Mandatsstatus
    if ($row_ms['id_dsm'] != null) {
        $ms = $row_ms['id_dsm'] . ' - ' . $row_ms['desc_dsm'];
    } else {
        $ms = '';
    }

    echo "<select onchange='dateOnChangeToday(\"inputDTDSM\");changeWidth(this)' class='form-control' id='dsm_inputMS' name='target__t_cont_dismissal__id_dsm'>";
    echo "<option value = '" . $row_ms['id_dsm'] . "'>" . $ms . "</option>";
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        if ($res_14a['14a'] == '' && $row['id'] == 2100) {
            break;
        }
        if ($row['id'] % 1000 == 0) {
            echo "<optgroup id = 'optid_" . $row['id'] . "'label = '" . $row['description'] . "'>";
        }
        if ($row['id'] % 1000 != 0) {
            echo "<option value='" . $row['id'] . "'>" . $row['description'] . '</option>';
        }
        if ($row['id'] % 1000 == 0) {
            echo '</optgroup>';
        }
    }
    echo '</select></div></div>';
    ?>

    <!-- Datum der Mandatsstatus-Änderung -->
    <div class="form-group">
        <label for="inputDTDSM" class="col-lg-2 control-label">Datum der Änderung des Mandatsstatus</label>
        <div class="col-lg-4">
            <input type="date" class="date-control" id="inputDTDSM" name="target__t_cont_dismissal__dt_dsm" value="<?php echo $row_ms['dt_dsm']; ?>">
        </div>
    </div>

    <!-- Folge bei Fristablauf -->
    <?php
    if (isset($row_ms['afdl_dsm']) && strlen($row_ms['afdl_dsm']) > 0) {
        $msg_adl = $row_ms['afdl_dsm'];
    } else {
        $msg_adl = 'kein Eintrag vorhanden';
    }
    ?>
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Folge bei Fristablauf</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-danger' id='dsm_after_deadline'>
                <?php echo $msg_adl; ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-info' id='dsm_helptext'>
                <?php
                if (isset($row_ms['ht_dsm']) && strlen($row_ms['ht_dsm']) > 0) {
                    echo $row_ms['ht_dsm'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext zum Mandatsstatus in der Plattform -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus in der Plattform</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-success' id='dsm_helptext_pf'>
                <?php
                if (isset($row_ms['htpf_dsm']) && strlen($row_ms['htpf_dsm']) > 0) {
                    echo $row_ms['htpf_dsm'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>

    <!-- Hilfetext zum Mandatsstatus Bearbeiter -->
    <div class='form-group'>
        <label class='col-lg-2 control-label'>Hilfetext zum Mandatsstatus für Bearbeiter**in</label>
        <div class='col-lg-4'>
            <div class='alert alert-dismissible alert-warning' id='dsm_helptext_wk'>
                <?php
                if (isset($row_ms['htwk_dsm']) && strlen($row_ms['htwk_dsm']) > 0) {
                    echo $row_ms['htwk_dsm'];
                } else {
                    echo 'kein Eintrag vorhanden';
                }
                ?>
            </div>
        </div>
    </div>
</div>
