<?php

/**
 * Mandatsbegründung Doc Comment
 * PHP Version 7.
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2021 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
// Tabellenname in der Datenbank
$table = "t_cont_just";

// Platform Datenabfrage
$sql = "SELECT
            id,
            data->'submitOptions'->>'willBeCharged' as \"auftrag\",
            data->'submitOptions'->>'willStart' as \"taetigwerden\",
            data->'submitOptions'->>'acceptCommunication' as \"einverstaendnis_email\",
            data->'submitOptions'->>'confirmSharedDocuments' as \"vertagsunterlagen_mitteilung\",
            data->'submitOptions'->>'willBeFreed' as \"schweigepflicht\",
            data->'submitOptions'->>'confirmSharingPersonalInformation' as \"uebermittlung_pers_daten\"
        FROM
            t_contracts
        WHERE id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_jf = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// Datenbank Abfrage
$sql = "SELECT * FROM $table WHERE id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_db = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);
?>

<div class="list-group col-lg-12 col-md-12 col-sm-12">
	<h4 class="list-group-item-heading">Mandatsbegründung</h4>
</div>

<?php
if ($res_jf['auftrag'] != '') {
	echo "<br>";

	radio_just(
		'willBeCharged',
		'Wird die Kanzlei gem. A. der Vertragsunterlagen beauftragt',
		['Ja', 'Nein'],
		$res_jf['auftrag'],
		$res_db['willBeCharged'],
		$table,
		true,
		true
	);

	radio_just(
		'willStart',
		'Wird die Kanzlei beauftragt, gem B. der Vertragsunterlagen
		unverzüglich, d.h. vor Ablauf der Widerrufsfrist mit der Tätigkeit zu
		beginnen',
		['Ja', 'Nein'],
		$res_jf['taetigwerden'],
		$res_db['willStart'],
		$table,
		true,
		true
	);

	radio_just(
		'acceptCommunication',
		'Wird das Einverständnis über die Kommunikation mit unverschlüsselter
		E-Mail gem. C. der Vertragsunterlagen erklärt.',
		['Ja', 'Nein'],
		$res_jf['einverstaendnis_email'],
		$res_db['acceptCommunication'],
		$table,
		true
	);

	radio_just(
		'confirmSharedDocuments',
		'Wird bestätigt, dass die Informationen gem. D. der Vertragsunterlagen
		mitgeteilt wurden.',
		['Ja', 'Nein'],
		$res_jf['vertagsunterlagen_mitteilung'],
		$res_db['confirmSharedDocuments'],
		$table,
		true,
		true
	);

	radio_just(
		'willBeFreed',
		'Werden die Rechtsanwälte der Kanzlei von der Verschwiegenheitspflicht
		gem. E. der Vertragsunterlagen entbunden.',
		['Ja', 'Nein'],
		$res_jf['schweigepflicht'],
		$res_db['willBeFreed'],
		$table,
		true
	);

	radio_just(
		'confirmSharingPersonalInformation',
		'Wird in die Übermittlung der personenbezogenen Daten gem. F. de
		Vertragsunterlagen eingewilligt.',
		['Ja', 'Nein'],
		$res_jf['uebermittlung_pers_daten'],
		$res_db['confirmSharingPersonalInformation'],
		$table,
		false,
		false,
		true
	);
} else {
	echo '<span class="label label-primary">keine Daten vorhanden</strong></span>';
}
?>
