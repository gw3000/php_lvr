<?php

/**
 * Contract--Details File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// require_once 'functions/elements.php';

$count = 0;
error_reporting(E_ALL);

// abfrage aller kommentare und status
$sql = "SELECT id, checked FROM t_cont_check WHERE id = :id";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_check = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);
// objekt mit allen kommentar inhalten
global $comm;
$comm = json_decode($res_check['checked']);

?>

<div class="form-group">
    <div class="col-lg-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <!-- section 01 -->
            <div class="panel panel-default" id="myCollapsible">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Basisdaten <?php echo commentSection(1); ?>
                        </a>
                    </h4>
                </div>
                <!--<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">-->
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?php require "section01.php" ?>
                    </div>
                </div>
            </div>
            <!-- section 02 -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingTwo" href="#collapseTwo">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Situationsabfrage <?php echo commentSection(2); ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <?php require "section02.php" ?>
                    </div>
                </div>
            </div>
            <!-- section 03 -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Versicherungsnehmer / versicherte Person <?php echo commentSection(3); ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <?php require "section03.php" ?>
                    </div>
                </div>
            </div>
            <!-- section 04 -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Unterlagen <?php echo commentSection(4); ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                        <?php require "section04.php" ?>
                    </div>
                </div>
            </div>
            <!-- section 05 -->
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Vertragsdaten <?php echo commentSection(5); ?>
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                    <div class="panel-body">
                        <?php require "section05.php" ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>