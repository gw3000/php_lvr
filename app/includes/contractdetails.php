<?php
/**
 * Details of contract Doc Comment
 * PHP Version 7.
 *
 * @category  Includes
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

//$json =  utf8_encode($row['data']);
//$data = json_decode($json, true);
// print_r($data);

?>
<div class="row">
  <div class="col-lg-12">

    <!-- banner no imported data
    <div class="alert alert-danger banner-warning" id="nodata">-->
    <div class="hidden" id="nodata">
      <h4>Warnung!</h4>
      <p>Vor Beginn der Vorprüfung zuerst Daten mit dem Button "Import Daten" in die Datenbank kopieren und bei Vornahme von Änderungen "Änderungen speichern" klicken!</p>
    </div>
    <br>
    <div class="bs-component">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#contDetails" data-toggle="tab">Vertragsdetails</a></li>
        <li><a href="#status" data-toggle="tab">Mandatsstatus</a> </li>
        <li><a href="#rsvDetails" data-toggle="tab">RSV-Details</a></li>
        <li><a href="#partiesDetails" data-toggle="tab">Dienstleister und Gegner</a></li>
        <li><a href="#calcDetails" data-toggle="tab">Abrechnung</a></li>
        <li><a href="#docsDetails" data-toggle="tab">Dokumente</a></li>
      </ul>
      <div id="myTabContent" class="tab-content">
        <div class="tab-pane active in" id="contDetails">
            <?php require 'details.php' ?>
        </div>
        <div class="tab-pane" id="status">
            <?php require 'ms.php' ?>
        </div>
        <div class="tab-pane" id="rsvDetails">
            <?php require 'rsv.php' ?>
        </div>
        <div class="tab-pane" id="partiesDetails">
            <?php require 'parties.php' ?>
        </div>
        <div class="tab-pane" id="calcDetails">
            <?php require 'calc.php' ?>
        </div>
        <div class="tab-pane" id="docsDetails">
            <?php require 'docs.php' ?>
        </div>
      </div>
    </div>
  </div>
