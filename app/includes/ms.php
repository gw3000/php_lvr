
<div class="form-group">
    <div class="col-lg-12">
        <div class="panel-group" id="ms" role="tablist" aria-multiselectable="true">
            <!-- section Rückabwicklung / Ankauf -->
            <div class="panel panel-default" id="myCollapsibleMS">
                <div class="panel-heading" role="tab" id="headingRAAK">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion-ms" href="#collapseRAAK" aria-controls="collapseRAAK" >
                            Rückabwicklung / Ankauf
                        </a>
                    </h4>
                </div>
                <div id="collapseRAAK" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingRAAK">
                    <div class="panel-body">
                        <?php require "revtrans.php" ?>
                    </div>
                </div>
            </div>

            <!-- section Kündigung -->
            <!-- bsp: 20303/21 -->
            <div class="panel panel-default dismissal">
                <div class="panel-heading" role="tab" id="headingDismissal" href="#collapseDismissal">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-ms" role="button"  href="#collapseDismissal" aria-controls="collapseDismissal">
                            Kündigung
                        </a>
                    </h4>
                </div>
                <div id="collapseDismissal" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDismissal">
                    <div class="panel-body">
                        <?php require "dismissal.php" ?>
                    </div>
                </div>
            </div>

            <!-- section Mandatsbegründung -->
            <div class="panel panel-default" id="myCollapsibleMS">
                <div class="panel-heading" role="tab" id="headingJustification">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion-ms" href="#collapseJustification" aria-controls="collapseJustification" >
                            Mandatsbegründung
                        </a>
                    </h4>
                </div>
                <div id="collapseJustification" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingJustification">
                    <div class="panel-body">
                        <?php require "justification.php" ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
