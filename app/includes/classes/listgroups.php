<?php

/**
 * ListGroup File Doc Comment
 * PHP Version 7
 *
 * @category  ElementConstructor
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

class ListGroup
{
    private $_link;
    private $_items;
    private $_title;

    public function __construct($title, $items, $link)
    {
        $this->title = $title;
        $this->items = $items;
        $this->link = $link;
    }

    public function generate()
    {
        $lg = "<a href='" . $this->link . "' class='list-group-item'>"
            . "<h4 class='list-group-item-heading'>" . $this->title . "</h4>"
            . "<p class='list-group-item-text'>";
        foreach ($this->items as $key => $value) {
            $lg .= "<strong>" . $key . ":</strong> " . $value . "<br>";
        }
        $lg .= "</p></a>";
        return $lg;
    }
}
