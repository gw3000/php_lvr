<?php

/**
 * RSV File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

//
// Tabellen name in der Datenbank
$table = "t_cont_rsv";

// whats the right refnum
$sql = "SELECT refnum FROM t_contracts WHERE id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_rn = $stmt->fetch(PDO::FETCH_ASSOC);
$refnum = $res_rn["refnum"];
unset($stmt);

// Platform Datenabfrage
$sql = "SELECT
        id,
        data->'a12aResponse'->>'value' as \"12a\",
        data->'a12bResponse'->>'value' as \"12b\",
        data->'a12eResponse'->>'value' as \"12e\",
        data->'a12fResponse'->>'value' as \"12f\",
        data->'a12gResponse'->>'value' as \"12g\",
        data->'a12cResponse'->>'value' as \"12c\",
        data->'a12dResponse'->>'value' as \"12d\"
    FROM
        t_contracts
    WHERE
        id = :id;";

$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_pf = $stmt->fetch(PDO::FETCH_ASSOC);

// Datenbank Abfrage
$sql = "
    SELECT
        cr.id,
        id_rsv,
        vs_num,
        vs_arb,
        vm_rsv,
        ds,
        bm,
        sn,
        date_trunc('day', last_update)::date as last_update,
        last_user,
        id_rsv_status,
        \"12a\",
        \"12b\",
        \"12c\",
        \"12da\",
        \"12db\",
        \"12dc\",
        \"12e\",
        \"12f\",
        \"12g\",
        dt_chg_status,
        ad.id as ida
    FROM
        public.t_cont_rsv cr
    INNER JOIN
        t_contracts co
    ON
        cr.id=co.id
    INNER JOIN
        t_address ad
    ON
       co.ida=ad.id
    WHERE cr.id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_db_rsv = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// Rechtschutzinformationen für Vertrag
$sql = "SELECT
            cr.id_rsv,
            rv.name,
            cr.vm_rsv,
            cr.id_rsv_status,
            rs.description,
            cr.vs_num,
            cr.vs_arb,
            cr.ds,
            cr.sn,
            cr.bm,
            cr.last_update,
            trrc.rsv,
            trrc.rsv_sn,
            trrc.rsv_vn,
            trrc.rsv_vsn,
            trrc.rsv_bemerkung,
            trrc.rsv_ablagenummer
        FROM
            t_cont_rsv cr
        LEFT JOIN t_rsv rv ON
            cr.id_rsv = rv.id
        LEFT JOIN t_rsv_status rs ON
            rs.id = cr.id_rsv_status
        LEFT JOIN t_ram_rsv_cont trrc ON
            cr.id = trrc.id
        WHERE
            cr.id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$row_rsv = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// RSV counter should start at 20000
$count = 2000;

$no_ram_message = 'noch nicht im RA Micro';
if (!isset($row_rsv["rsv"])) {
    $row_rsv["rsv"] = $no_ram_message;
}
if (!isset($row_rsv["rsv_sn"])) {
    $row_rsv["rsv_sn"] = $no_ram_message;
}
if (!isset($row_rsv["rsv_vsn"])) {
    $row_rsv["rsv_vsn"] = $no_ram_message;
}
if (!isset($row_rsv["rsv_vn"])) {
    $row_rsv["rsv_vn"] = $no_ram_message;
}

// select global rsv address
$sql_ram_rsv = "SELECT rsv,rsv_id,rsv_num,bemerkung FROM public.t_ram_rsv_addr WHERE id = :ida;";
$stmt_ram_rsv = $pdo->prepare($sql_ram_rsv);
$stmt_ram_rsv->bindParam(':ida', $res_db_rsv['ida'], PDO::PARAM_INT);
$stmt_ram_rsv->execute();
$row_ram_rsv = $stmt_ram_rsv->fetch();
?>

<div class="list-group col-lg-12 col-md-12 col-sm-12">
    <div class="list-group-item">
    <br>
        <h4 class="list-group-item-heading">RSV-Details <small>vertragsspezifisch</small></h4>
        <br>
        <!-- RSV Status + Auswahlliste -->
        <div class="form-group">
            <label for="nameRSV" class="col-lg-2 control-label">Rechtsschutzstatus</label>
            <div class="col-lg-8">
                <?php
                $sql = "SELECT id, description, helptext FROM t_rsv_status ORDER BY id ASC;";
                $stmt = $pdo->prepare($sql);
                $stmt->execute();

                // Auswahlfeld für Versicherungsstatus
                echo "<select class='form-control' id='nameRSVStatus' name='target__t_cont_rsv__id_rsv_status' >";
                echo "<option value = '" . $row_rsv['id_rsv_status'] . "'>" . $row_rsv['description'] . "</option>";
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['description'] . "</option>";
                }
                echo "</select>";
                ?>
            </div>
        </div>

        <?php
            // Datum der RSV-Status Änderung
            one_line_input(
                'dt_chg_status',
                'Datum der RSV-Status Änderung',
                $res_db_rsv["dt_chg_status"],
                $table,
                'date',
                false
            );

            // RSV-Bemerkung
            one_line_input(
                'bm',
                'Bemerkung zum RSV-Status',
                $res_db_rsv["bm"],
                $table,
                'text',
                true
            );

            // Rechtsschutzversicherung
            dropdown(
                '12a',
                'Rechtsschutzversicherung',
                [
                'Die Mandantschaft ist selbst rechtsschutzversichert. ',
                'Die Mandantschaft ist mitversichert (z. B. über Partner/Familie). ',
                'Die Mandantschaft ist nicht rechtsschutzversichert.',
                ''
                ],
                $res_pf["12a"],
                $res_db_rsv["12a"],
                $table,
                false
            );

            // Privatrechtsschutz enthalten
            dropdown(
                '12b',
                'Privatrechtsschutz enthalten',
                ['Ja', 'Nein', ''],
                tfJN($res_pf["12b"]),
                $res_db_rsv["12b"],
                $table,
                false
            );

            // Rechtsschutz Dokument
            dropdown(
                '12c',
                'Rechtsschutz Dokument',
                ['Ja', 'Nein', ''],
                tfJN($res_pf["12c"]),
                $res_db_rsv["12c"],
                $table,
                false
            );

            // Rechtsschutz Dokument Art
            $res12d = substr($res_pf['12d'], 1, -1);
            $res12d = str_replace('", "', '","', $res12d);
            $res12d = str_replace('"', '', $res12d);
            $res12d_arr = explode(',', $res12d);
            $rsv_doc_type = array(
                ['12da', 'Versicherungsschein/-police (Rechtsschutz)'],
                ['12db', 'aktuelle Beitragsrechnung (Rechtsschutz)'],
                ['12dc', 'andere Dokumente (Rechtsschutz)']
            );

            checkbox(
                '12d',
                'Rechtsschutz Dokument Art',
                $rsv_doc_type,
                $res12d_arr,
                $res_db_rsv,
                $table,
                false
            );

            // Rechtsschutz-Versicherer
            text_input(
                '12e',
                'Rechtsschutz-Versicherer',
                rsvANum($res_pf["12e"]),
                $row_rsv["rsv"],
                $table,
                false,
                $disabled = true,
                $ram = true
            );

            // Rechtsschutz-Versicherungsnummer
            text_input(
                '12f',
                'Rechtsschutz-Versicherungsnummer',
                $res_pf["12f"],
                // eXcolon($row_rsv["rsv_vsn"]),
                $row_rsv["rsv_vsn"],
                $table,
                false,
                $disabled = true,
                $ram = true
            );

            // Rechtsschutz Beginn
            text_number(
                '12g',
                'Rechtsschutz Beginn',
                '1',
                '1950',
                '2030',
                $res_pf["12g"],
                $res_db_rsv["12g"],
                $table,
                true
            );

            // Schadennummer
            one_line_input(
                'vs_num',
                'Schadennummer',
                eXcolon($row_rsv["rsv_sn"]),
                $table,
                'text',
                $hl = false,
                $disabled = true,
                $ram = true
            );

            // Deckungssumme
            one_line_input(
                'ds',
                'Deckungssumme',
                $res_db_rsv["ds"],
                $table,
                'number',
                false
            );

            // Rechtsschutzbedingungen
            one_line_input(
                'vs_arb',
                'Rechtsschutzbedingungen',
                $res_db_rsv["vs_arb"],
                $table,
                'text',
                false
            );

            // Bemerkung Rechtsschutz
            one_line_input(
                'vs_bem',
                'Bemerkung',
                $row_rsv["rsv_bemerkung"],
                $table,
                'text',
                $hl = true,
                $disabled = true,
                $ram = true
            );
        ?>

        <br>
        <h4 class="list-group-item-heading">RSV-Details <small>mandantenspezifisch</small></h4>
        <br>

        <?php
            if ($row_rsv['rsv'] == '') {
                $ram_rsv_id = 'bisher keine RSV in RAM hinterlegt';
            } else {
                $ram_rsv_id = $row_rsv['rsv'];
            }

            // rsv nummer
            if ($row_ram_rsv['rsv_num'] == '') {
                $ram_rsv_num = 'bisher keine RSV in RAM hinterlegt';
            } else {
                $ram_rsv_num = $row_ram_rsv['rsv_num'];
            }

            // rsv bemerkung
            if ($row_ram_rsv['bemerkung'] == '') {
                $ram_rsv_bem = 'bisher keine Bemerkung in RA Micro hinterlegt';
            } else {
                $ram_rsv_bem = $row_ram_rsv['bemerkung'];
            }

            one_line_input(
                '',
                'Rechtsschutzversicherung',
                // eXcolon($ram_rsv_id),
                $ram_rsv_id,
                $table,
                'text',
                $hl = false,
                $disabled = true,
                $ram = true
            );

            one_line_input(
                '',
                'Versicherungsnummer',
                // eXcolon($ram_rsv_num),
                $ram_rsv_num,
                $table,
                'text',
                $hl = false,
                $disabled = true,
                $ram = true
            );

            one_line_input(
                '',
                'Bemerkung',
                // eXcolon($ram_rsv_num),
                $ram_rsv_bem,
                $table,
                'text',
                $hl = false,
                $disabled = true,
                $ram = true
            );
        ?>
    </div>
</div>
