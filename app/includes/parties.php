<?php

/**
 * Parties = agent(s) + oppponent
 * PHP Version 7
 *
 * @category  Include
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// require_once 'functions/elements.php';
require_once 'classes/listgroups.php';

$sql_agent = "SELECT vd.\"14a\", ag.id, ag.lastname,ag.firstname,
    ag.company,ag.email,ag.status,ag.role,ag.ownerid
         FROM t_contracts co
         INNER JOIN t_agents ag
         ON co.created_by =ag.id
         LEFT JOIN t_cont_vd vd
         ON co.id=vd.id
         WHERE co.id = :id;";
$stmt = $pdo->prepare($sql_agent);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$row_agent = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

if (isset($row_agent) && $row_agent != false) {
  // owner id if exists
  if ($row_agent['ownerid'] != '') {
    $sql_agent_owner = "SELECT ag.id,ag.lastname,ag.firstname,ag.company,
        ag.email,ag.status,ag.role,ag.ownerid
      FROM t_agents ag WHERE ag.id = :id";
    $stmt = $pdo->prepare($sql_agent_owner);
    $stmt->bindParam(':id', $row_agent['ownerid'], PDO::PARAM_STR);
    $stmt->execute();
    $row_agent_owner = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);
  }

  // opponent if exists
  if ($row_agent['14a'] != '') {
    $sql_opponent = "SELECT ir.id_ram , ir.opponent
      FROM t_cont_vd vd
      INNER JOIN t_isr ir ON
      vd.\"14a\" = ir.id
      WHERE vd.id = :id";
    $stmt = $pdo->prepare($sql_opponent);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $row_opponent = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);
  }

  echo "<div class='list-group col-lg-12 col-md-12 col-sm-12'>";
  //  agent
  $title_agent = 'Vermittler';
  $items_agent = array(
    'Adressnummer RAMicro' => $row_agent['id'] + 30000,
    'Name' => $row_agent['lastname'],
    'Vorname' => $row_agent['firstname'],
    'Firma' => $row_agent['company']
  );
  $link_agent = "agent.php?id=" . $row_agent['id'];

  $agent = new ListGroup($title_agent, $items_agent, $link_agent);
  echo $agent->generate();

  // grand agent
  if (isset($row_agent_owner['id']) && $row_agent_owner['id'] != '') {
    $title_gagent = 'Obervermittler';
    $ownerid = $row_agent_owner['id'] + 30000;
    $items_gagent = array(
      'Adressnummer RAMicro' => $ownerid,
      'Name' => $row_agent_owner['lastname'],
      'Vorname' => $row_agent_owner['firstname'],
      'Firma' => $row_agent_owner['company']
    );
    $link_gagent = "agent.php?id=" . $row_agent_owner['id'];

    $grand_agent = new ListGroup($title_gagent, $items_gagent, $link_gagent);
    echo $grand_agent->generate();
  }

  // opponent
  if (isset($row_opponent['id_ram']) && $row_opponent['id_ram'] != '') {
    $title_opponent = 'Gegner';
    $items_opponent = array(
      'Adressnummer RAMicro' => $row_opponent['id_ram'],
      'Name' => $row_opponent['opponent']
    );
    $link_opponent = "#";

    $opponent = new ListGroup($title_opponent, $items_opponent, $link_opponent);
    echo $opponent->generate();
  }
  echo "</div>";
} else {
  echo '<div class="list-group col-lg-12 col-md-12 col-sm-12">';
  echo '<div class="list-group-item"><br>';
  echo '<h4 class="list-group-item-heading">Dienstleister und Gegner</h4>';
  echo '<span class="label label-primary">keine Daten von Dienstleistern oder vorhanden</strong></span></div></div>';
}
