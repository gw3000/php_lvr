<?php

/**
 * Section 01 File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<fieldset>
    <!-- <legend>Basisdaten</legend> -->

    <?php
    // Tabellen name in der Datenbank
    $table = "t_cont_bd";

    // Platform Datenabfrage
    $sql = "SELECT
        id,
        data->'a09aResponse'->>'value' as \"2a\",
        data->'a02bResponse'->>'value' as \"2b\",
        data->'a02cResponse'->>'value' as \"2c\",
        data->'a02dResponse'->>'value' as \"2d\",
        data->'a02eResponse'->>'value' as \"2e\",
        data->'a02fResponse'->>'value' as \"2f\"
    FROM
        t_contracts
    WHERE
        id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_pf = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Datenbak Abfrage
    $sql = "SELECT * FROM $table WHERE id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_db = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Formular Teil
    dropdown('9a', 'Mandatstyp', ['eine Einzelperson', 'eine Einzelperson als Alleinerbe/Alleinerbin', 'eine Erbengemeinschaft', 'Eheleute/Lebenspartnerschaft', 'Firma/Gesellschaft'], $res_pf['2a'], $res_db['9a'], $table);
    text_input('2b', 'Versicherungsnummer', $res_pf['2b'], $res_db['2b'], $table);
    radio('2c', 'Eingabeberechtigung', ['Ja', 'Nein'], $res_pf['2c'], $res_db['2c'], $table);
    radio('2d', 'Ausschlussprodukte', ['Ja', 'Nein'], $res_pf['2d'], $res_db['2d'], $table);
    ?>

    <div id='abschlussprodukte'>
        <div class='form-group'>
            <label class='col-lg-2 control-label'></label>
            <div class='col-lg-9 bs-component'>
                <div class='col-lg-6'>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__kapzr'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__kapzr' id='asp_1' <?php checked($res_db['kapzr']) ?>>
                            kein Ausschlussprodukt bzw. -Zeitraum
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__bav'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__bav' id='asp_2' <?php checked($res_db['bav']) ?>>
                            betriebliche Altersvorsorge (bAV)
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__vwl'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__vwl' id='asp_3' <?php checked($res_db['vwl']) ?>>
                            vermögenswirksame Leistungen (VWL)
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__sgv'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__sgv' id='asp_4' <?php checked($res_db['sgv']) ?>>
                            Sterbegeldversicherung
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__uvbrg'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__uvbrg' id='asp_5' <?php checked($res_db['uvbrg']) ?>>
                            Unfallversicherung mit/ohne Beitragsrückgewähr
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__lvtf'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__lvtf' id='asp_6' <?php checked($res_db['lvtf']) ?>>
                            Lebensversicherung, die ausschließlich das Todesfallrisiko absichert (Risikolebensversicherung)
                        </label>
                    </div>
                </div>
                <div class='col-lg-6'>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__svrk'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__svrk' id='asp_7' <?php checked($res_db['svrk']) ?>>
                            sonstige Versicherungen, die ausschließlich ein Risiko absichern (z.B. reine
                            Berufsunfähigkeitsversicherung)
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__kaiv'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__kaiv' id='asp_8' <?php checked($res_db['kaiv']) ?>>
                            Kapitalanlage/Investment ohne Versicherung
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__aav'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__aav' id='asp_9' <?php checked($res_db['aav']) ?>>
                            Der Antrag zum Versicherungsvertrag wurde vor dem 01.01.1991 unterschrieben.
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__vnd'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__vnd' id='asp_10' <?php checked($res_db['vnd']) ?>>
                            Der Vertrag wurde nach dem 30.06.2010 abgeschlossen
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type='hidden' value='' name='target__t_cont_bd__vvd'>
                            <input type="checkbox" value="1" name='target__t_cont_bd__vvd' id='asp_11' <?php checked($res_db['vvd']) ?>>
                            Der Vertrag wurde vor dem 01.01.2003 vollständig beendet
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <hr>
    </div>

    <?php
    radio('2e', 'Insolvenz', ['Ja', 'Nein'], $res_pf['2e'], $res_db['2e'], $table);
    radio('2f', 'Selbstvermittlung', ['Ja', 'Nein'], $res_pf['2f'], $res_db['2f'], $table, false);
    ?>

</fieldset>