<?php

/**
 * Section 03 File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<fieldset>
    <!-- <legend>Situationsabfrage</legend> -->

    <?php
    // Tabellen name in der Datenbank
    $table = "t_cont_vn";

    // Platform Datenabfrage
    $sql = "SELECT
        id,
        data->'a10aaResponse'->>'value' as \"10a1\",
        data->'a10aResponse'->>'value' as \"10a\",
        data->'a10cResponse'->>'value' as \"10c\",
        data->'a10dResponse'->>'value' as \"10d\",
        data->'a10eResponse'->>'value' as \"10e\",
        data->'a10fResponse'->>'value' as \"10f\",
        data->'a10gResponse'->>'value' as \"10g\",
        data->'a11aResponse'->>'value' as \"11a\",
        data->'a11bResponse'->>'value' as \"11b\",
        data->'a11cResponse'->>'value' as \"11c\",
        data->'a11fResponse'->>'value' as \"11f\",
        data->'a11gResponse'->>'value' as \"11g\",
        data->'a11hResponse'->>'value' as \"11h\",
        data->'a11iResponse'->>'value' as \"11i\"
    FROM
        t_contracts
    WHERE
        id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_pf = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Datenbak Abfrage
    $sql = "SELECT * FROM $table WHERE id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_db = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Formular Teil Versicherungsnehmer und versicherte Person
    radio('10a1', 'Mandant/urspünglicher alleiniger Versicherungsnehmer', ['Ja', 'Nein'], $res_pf['10a1'], $res_db['10a1'], $table);
    dropdown('10a', 'ursprünglicher Versicherungsnehmer', ['Eine Person außer der Mandantschaft war bei Abschluss des Vertrages Versicherungsnehmer', 'Mehrere Personen außer der Mandantschaft waren bei Abschluss des Vertrages Versicherungsnehmer', 'Eine weitere Person neben der Mandantschaft war bei Abschluss des Vertrages Versicherungsnehmer'], $res_pf['10a'], $res_db['10a'], $table);
    text_input('10c', 'Name ursprünglicher Versicherungsnehmer / Bezeichnung Erbengemeinschaft', $res_pf['10c'], $res_db['10c'], $table);
    date_input('10d', 'Geburtsdatum Mandant 1', $res_pf['10d'], $res_db['10d'], $table);
    date_input('10e', 'Geburtsdatum Mandant 2', $res_pf['10e'], $res_db['10e'], $table);
    date_input('10f', 'Geburtsdatum anderer Versicherungsnehmer', $res_pf['10f'], $res_db['10f'], $table);
    date_input('10g', 'Geburtsdatum weiterer Versicherungsnehmer', $res_pf['10g'], $res_db['10g'], $table);
    radio('11a', 'Mdt. 1 ist versicherte Person 1', ['Ja', 'Nein'], $res_pf['11a'], $res_db['11a'], $table);
    date_input('11b', 'Geburtsdatum versicherte Person 1', $res_pf['11b'], $res_db['11b'], $table);
    radio('11c', 'Geschlecht versicherte Person 1', ['männlich', 'weiblich'], $res_pf['11c'], $res_db['11c'], $table);
    radio('11f', 'weitere versicherte Person', ['Ja', 'Nein'], $res_pf['11f'], $res_db['11f'], $table);
    radio('11g', 'Mdt. 2 ist versicherte Person 2', ['Ja', 'Nein'], $res_pf['11g'], $res_db['11g'], $table);
    date_input('11h', 'Geburtsdatum versicherte Person 2', $res_pf['11h'], $res_db['11h'], $table);
    radio('11i', 'Geschlecht versicherte Person 2', ['männlich', 'weiblich'], $res_pf['11i'], $res_db['11i'], $table, false);
    ?>

</fieldset>
