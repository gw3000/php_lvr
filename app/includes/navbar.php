<?php

/**
 * Navbar File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// badge unseen contracts
$sql = "
    SELECT
        count(*)
    FROM
        public.t_cont_bd tb
    INNER JOIN
        t_contracts tc
    ON
        tb.id = tc.id
    WHERE
        last_update is NULL
    AND
      tc.created_by != 45
";
$stmt_badge = $pdo->prepare($sql);
$stmt_badge->execute();
$num_unseen = $stmt_badge->fetch(PDO::FETCH_ASSOC);
if ($num_unseen["count"] > 0) {
  $badge = "<span class='badge' id='badge' >" . $num_unseen["count"] . "</span>";
} else {
  $badge = "";
}

?>
<div class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <a href="../" class="navbar-brand">lvr &middot; db</a>
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li>
          <a href="search.php">Suche</a>
        </li>
        <li>
          <a href="results.php?lname=%">Adressen</a>
        </li>
        <li>
          <a href="contracts.php?state=de">Verträge DE</a>
        </li>
        <li>
          <a href="contracts.php?state=at">Verträge AT</a>
        </li>
        <li>
          <a href="contracts.php?unseen">Verträge ungesichtet <?php echo $badge; ?></a>
        </li>
        <li>
          <a href="dismissable.php">Verträge kündbar</a>
        </li>
        <li>
          <a href="agents.php">Vermittler</a>
        </li>
        <?php
        if ($_SESSION['role'] == 'user-fa' || $_SESSION['role'] == 'user-ra') {
          echo "<li><a href='mseditor.php'>Rückabwicklung</a></li>";
        }
        ?>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <?php
        if ($_SESSION['role'] == 'admin') {
          echo "<li class='dropdown'>" .
            "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>Administration <span class='caret'></span></a>" .
            "<ul class='dropdown-menu' role='menu'>" .
                "<li><a href='users.php'>Account Management</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='passwd.php'>Passwort ändern</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='isreditor.php'>Versicherer</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='dsmeditor.php'>Kündigung</a></li>" .
                "<li><a href='mseditor.php'>Rückabwicklung</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='rseditor.php'>Rechtschutzstatus</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='notsubmitted.php'>Vertragsdaten@lv-r.de (nicht eingereicht)</a></li>" .
            "</ul>";
        } elseif ($_SESSION['role'] == 'manager') {
          echo "<li class='dropdown'>" .
            "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>Management <span class='caret'></span></a>" .
            "<ul class='dropdown-menu' role='menu'>" .
                "<li><a href='dsmeditor.php'>MS-Kündigung</a></li>" .
                "<li><a href='mseditor.php'>MS-Rückabwicklung</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='rseditor.php'>Rechtsschutzstatus</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='passwd.php'>Passwort zurücksetzen</a></li>" .
            "</ul>";
        } elseif ($_SESSION['role'] == 'insure') {
          echo "<li class='dropdown'>" .
            "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>Management <span class='caret'></span></a>" .
            "<ul class='dropdown-menu' role='menu'>" .
                "<li><a href='isreditor.php'>Versicherer</a></li>" .
                "<li class='divider'></li>" .
                "<li><a href='passwd.php'>Passwort zurücksetzen</a></li>" .
            "</ul>";
        } else {
          echo '<li><a href="passwd.php">Passwort zurücksetzen</a></li>';
        }
        ?>
        <li><a href="logout.php">Logout</a></li>
      </ul>
    </div>
  </div>
</div>
