<?php

function is_number($item)
{
    if (preg_match("'^-?\d{0,}[{.,}]\d{0,}^'", $item)) {
        if (stristr($item, ',')) {
            $item = str_replace(',', '.', $item);
        }
        return $item;
    } elseif (is_numeric($item)) {
        return $item;
    }
}


function is_date($item)
{
    if (preg_match("'\d{4}[-]\d{2}[-]\d{2}'", $item)) {
        return true;
    }
}
