<?php

function sanitize($value)
{
    $value = trim($value);
    $value = trim($value, '\"\"');
    $value = stripslashes($value);
    $value = htmlspecialchars($value, ENT_QUOTES | ENT_HTML5 | ENT_DISALLOWED | ENT_SUBSTITUTE, 'UTF-8');
    if (empty($value)) {
        $value = null;
    }
    return $value;
}
