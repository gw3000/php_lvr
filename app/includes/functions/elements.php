<?php
/**
 * Functions Doc Comment
 * PHP Version 7.
 *
 * @category  Functions
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// funktion fuer radiobuttons
function radio($name, $description, $answers, $db_res_pf, $db_res_db, $table, $hl = true)
{
    if ($answers == ['Ja', 'Nein']) {
        $answers = ['true', 'false'];
    }

    global $count;
    $count_l = $count_r = $count;

    $body = '';
    foreach (['source', 'target'] as $key1 => $value1) {
        foreach ($answers as $value2) {
            if (0 == $key1) {
                // add check if the db_res_pfult
                if ($value2 == $db_res_pf) {
                    $body = $body . "<div class='radio-inline'><label><input disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_l . "' value='" . $value2 . "' checked>" . tfJN($value2) . '</label></div>';
                } else {
                    $body = $body . "<div class='radio-inline'><label><input disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_l . "' value='" . $value2 . "'>" . tfJN($value2) . '</label></div>';
                }
                ++$count_l;
            } else {
                if ($value2 == $db_res_db) {
                    $body = $body . "<div class='radio-inline'><label><input type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_r . "' value='" . $value2 . "' checked>" . tfJN($value2) . '</label></div>';
                } else {
                    $body = $body . "<div class='radio-inline'><label><input type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_r . "' value='" . $value2 . "' >" . tfJN($value2) . '</label></div>';
                }
                ++$count_r;
            }
        }
        if (0 == $key1) {
            // working on with the right side
            $body = $body . "</div><div class='col-lg-4 " . $name . "'>";
        }
    }
    $count = $count_l;
    echo "<div id='" . $name . "'><div class='form-group'><label class='col-lg-2 control-label'><div " . diff_pf_db($db_res_pf, $db_res_db) . ">$description <small>($name)</small></div></label><div class='col-lg-4'>" . $body . '</div>' . checkButton($name) . '</div>' . hline($hl) . '</div>';
}

// funktion fuer mandatsbegruendung
function radio_just($name, $description, $answers, $db_res_pf, $db_res_db, $table, $hl = true, $disabled=false)
{

    if ($answers == ['Ja', 'Nein']) {
        $answers = ['true', 'false'];
    }

    // disable radio buttons on right sight
    if ($db_res_db==''){
        $disabled = '';
    } else {
        if ($disabled) {
            $disabled='disabled';
        } else {
            $disabled = '';
        }
    }

    global $count;
    $count_l = $count_r = $count;

    $body = '';
    foreach (['source', 'target'] as $key1 => $value1) {
        foreach ($answers as $value2) {
            if (0 == $key1) {
                // add check if the db_res_pfult
                if ($value2 == $db_res_pf) {
                    $body = $body . "<div class='radio-inline'><label><input disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_l . "' value='" . $value2 . "' checked>" . tfJN($value2) . '</label></div>';
                } else {
                    $body = $body . "<div class='radio-inline'><label><input disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_l . "' value='" . $value2 . "'>" . tfJN($value2) . '</label></div>';
                }
                ++$count_l;
            } else {
                if ($value2 == $db_res_db) {
                    $body = $body . "<div class='radio-inline'><label><input $disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_r . "' value='" . $value2 . "' checked>" . tfJN($value2) . '</label></div>';
                } else {
                    $body = $body . "<div class='radio-inline'><label><input $disabled type='radio' name='" . $value1 . '__' . $table . '__' . $name . "' id='" . $value1 . '_' . $count_r . "' value='" . $value2 . "' >" . tfJN($value2) . '</label></div>';
                }
                ++$count_r;
            }
        }
        if (0 == $key1) {
            // working on with the right side
            $body = $body . "</div><div class='col-lg-3 " . $name . "'>";
        }
    }
    $count = $count_l;
    echo "<div id='" . $name . "'><div class='form-group'><label class='col-lg-5 control-label'><div " . diff_pf_db($db_res_pf, $db_res_db) . ">$description</div></label><div class='col-lg-3'>" . $body . '</div></div>' . hline($hl) . '</div>';
}

// funktion fuer dropdownmenue
function dropdown($name, $description, $answers, $db_res_pf, $db_res_db, $table, $hl = true)
{
    global $count;

    $left = "<div id='" . $name . "'><div class='form-group'><label class='col-lg-2 col-md-2 col-sm-2 control-label'><div " . diff_pf_db($db_res_pf, $db_res_db) . '>' . $description . ' <small>(' . $name . ")</small></div></label>
        <div class='col-lg-4 col-md-4 col-sm-4'><input type='text' class='form-control' id='source_" . $count . "' disabled value='" . $db_res_pf . "'></div>";

    $mid = "<div class='col-lg-4 col-md-4 col-sm-4'><select class='form-control " . $name . "' name = 'target__" . $table . '__' . $name . "' id='target_" . $count . "'>";

    $sel = "<option value = '$db_res_db'>$db_res_db</option><option disabled>──────────────────────────────</option>";
    foreach ($answers as $answer) {
        ('' == $answer) ? $value = 'leer' : $value = $answer;
        $sel = $sel . "<option value='" . $answer . "'>" . $value . '</option>';
    }

    $right = '</select></div>' . checkButton($name) . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $left . $mid . $sel . $right;
}

// funktion fuer checkboxes
function checkbox($name, $description, $col_description, $res_pf, $res_db, $table, $hl = true)
{
    global $count;
    $count_l = $count_r = $count;
    $left = "<div id='" . $name . "'><div class='form-group'><label class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ")</small></label><div class='col-lg-4'>";
    foreach ($col_description as $description) {
        $check = (in_array($description[1], $res_pf)) ? 'checked' : '';
        $left = $left . "<div class='checkbox'><label><input type='checkbox' disabled value='1' " . $check . "  name='source__" . $table . '__' . $description[0] . "' id='source_" . $count_l . "'> " . $description[1] . '</label></div>';
        ++$count_l;
    }
    $left = $left . '</div>';
    $right = "<div class='col-lg-4'>";
    foreach ($col_description as $description) {
        if (1 == $res_db[$description[0]]) {
            $check = 'checked';
        } else {
            $check = '';
        }

        $right = $right . "<div class='checkbox " . $name . "'><label><input type='hidden' value='' name='target__" . $table . '__' . $description[0] . "'><input type='checkbox' value='1' " . $check . " name='target__" . $table . '__' . $description[0] . "' id='target_" . $count_r . "'> " . $description[1] . '</label></div>';
        ++$count_r;
    }
    $right = $right . '<br></div>';
    $count = $count_r++;
    ++$count;
    echo $left . $right . checkButton($name) . '</div>' . hline($hl) . '</div>';
}

// funktion fuer text input felder
function text_input($name, $description, $db_res_pf, $db_res_db, $table, $hl = true, $disabled = false, $ram = false)
{
    global $count;

    if ($ram) {
        $ram_pic = " <img src='../../img/ram.png' alt='RAMICRO' width='15' height='15'>";
    } else {
        $ram_pic = "";
    }

    // check if the right pane is disabled
    if ($disabled == true) {
        $da = 'disabled';
    } else {
        $da = '';
    }

    $element = "<div id='" . $name . "'><div class='form-group'><div " . diff_pf_db($db_res_pf, $db_res_db) . "><label for='" . $name . "' class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ')</small>' . $ram_pic . '</label></div>'
        . "<div class='col-lg-4'><input type='text' class='form-control' id='source_" . $count . "' value='" . $db_res_pf . "' disabled></div>"
        . "<div class='col-lg-4 " . $name . "'><input " . $da . " type='text' class='form-control' name='target__" . $table . '__' . $name . "' id='target_" . $count . "' value ='" . $db_res_db . "'></div>"
        . checkButton($name)
        . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $element;
}

// funktion fuer text area felder
function text_area($name, $description, $db_res_pf, $db_res_db, $table, $hl = true)
{
    global $count;
    $element = "<div id='" . $name . "'><div class='form-group'><div " . diff_pf_db($db_res_pf, $db_res_db) . "><label for='" . $name . "' class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ')</small></label></div>'
        . "<div class='col-lg-4'><textarea class='form-control' id='source_" . $count . "' rows='6' disabled>" . $db_res_pf . ' </textarea></div>'
        . "<div class='col-lg-4 " . $name . "'><textarea class='form-control' name='target__" . $table . '__' . $name . "' id='target_" . $count . "' rows='6'>" . $db_res_db . '</textarea></div>'
        . checkButton($name)
        . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $element;
}
// funktion fuer text input versicherung
function text_input_ins($name, $description, $description2, $db_res_pf, $db_res_id, $db_res_pres, $db_res_sel, $db_res_opp, $table, $hl = true)
{
    global $count;
    $element = "<div id='" . $name . "'><div class='form-group'><div class='text-secondary'><label for='" . $name . "' class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ')</small></label></div>'
        . "<div id='insurance_l1' class='col-lg-4'><input type='text' class='form-control' id='source_" . $count . "' value='" . $db_res_pf . "' disabled></div>"
        // ."<div name='target__".$table."__".$name."' id='target_".$count."' value ='".$db_res_id."'></div>"
        . "<div class='col-lg-4 hidden'><input type='text' name='target__" . $table . '__' . $name . "' id='target_" . $count . "' value ='" . $db_res_id . "' class='form-control'></div>"
        . "<div class='col-lg-4 " . $name . "'><input type='text' class='form-control' id='insurance_presentation' value ='" . $db_res_pres . "'></div>"
        . checkButton($name)
        . '</div>'
        . hline($hl)
        . "<div id='insurance_l2'><div class='form-group'><div class='text-secondary'><label for='insurance_l2' class='col-lg-2 control-label'>" . $description2 . '</label></div>'
        . "<div class='col-lg-4'><textarea class='form-control' rows='3' id='insurance_selection' disabled>" . $db_res_sel . '</textarea></div>'
        . "<div class='col-lg-4'><textarea class='form-control' rows='3' id='insurance_opponent' disabled>" . $db_res_opp . '</textarea></div>'
        . '</div>'
        . hline($hl)
        . '</div>'
        . '</div>';
    ++$count;
    echo $element;
}

// funktion fuer links text rechts zahlen
function text_number($name, $description, $step, $min, $max, $db_res_pf, $db_res_db, $table, $hl = true)
{
    global $count;
    $element = "<div id='" . $name . "'><div class='form-group'><div " . diff_pf_db($db_res_pf, $db_res_db) . "><label for='" . $name . "' class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ')</small></label></div>'
        . "<div class='col-lg-4'><input type='text' class='form-control' id='source_" . $count . "' value='" . $db_res_pf . "' disabled></div>"
        . "<div class='col-lg-4 " . $name . "'><input type='number' step=" . $step . ' min=' . $min . ' max=' . $max . " class='form-control' name='target__" . $table . '__' . $name . "' id='target_" . $count . "' value ='" . $db_res_db . "'></div>"
        . checkButton($name)
        . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $element;
}

// funktion fuer datumsfelder
function date_input($name, $description, $db_res_pf, $db_res_db, $table, $hl = true)
{
    global $count;
    $element = "<div id='" . $name . "'><div class='form-group'><div " . diff_pf_db(DateFormating($db_res_pf), $db_res_db) . "><label for='" . $name . "' class='col-lg-2 control-label'>" . $description . ' <small>(' . $name . ')</small></label></div>'
        . "<div class='col-lg-4'><input type='date' class='date-control' id='source_" . $count . "' value='" . DateFormating($db_res_pf) . "' disabled></div>"
        . "<div class='col-lg-4 " . $name . "'><input type='date' class='date-control' name='target__" . $table . '__' . $name . "' id='target_" . $count . "' value ='" . $db_res_db . "'></div>"
        . checkButton($name)
        . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $element;
}

// kommentar button
function checkButton($name)
{
    // rufe globale variable für kommentare
    global $comm;

    // prüfe ob status gesetzt ist
    if (isset($comm->{$name}->{'status'})) {
        $checked = $comm->{$name}->{'status'};
    } else {
        $checked = 'noch nicht bearbeitet';
    }

    // prüfe ob comment gesetzt ist
    if (isset($comm->{$name}->{'comment'})) {
        $comment = $comm->{$name}->{'comment'};
    } else {
        $comment = '';
    }

    // weise icon zu in abhängigkeit ob kommentar vorhanden oder nicht
    if ('' != $comment) {
        $icon = "<i class='fas fa-comment-medical'></i>";
    } elseif ('' == $comment) {
        $icon = "<i class='fas fa-comment-slash'></i>";
    }

    $button = '';
    // schreibe indiv. icon in variable
    if ('ungeprüft' == $checked) {
        $button = "<button type='button' id=check_" . $name . ' onclick = "loadModal(check_' . $name . ")\" class='btn btn-primary btn-sm' data-toggle='modal' data-target='#commentModal' value=" . $checked . " title='Status: " . $checked . "'>" . $icon . '</button>';
    } elseif ('geprüft' == $checked) {
        $button = "<button type='button' id=check_" . $name . ' onclick = "loadModal(check_' . $name . ")\" class='btn btn-success btn-sm' data-toggle='modal' data-target='#commentModal' value=" . $checked . " title='Status: " . $checked . "'>" . $icon . '</button>';
    } elseif ('unklar' == $checked) {
        $button = "<button type='button' id=check_" . $name . ' onclick = "loadModal(check_' . $name . ")\" class='btn btn-info btn-sm' data-toggle='modal' data-target='#commentModal' value=" . $checked . " title='Status: " . $checked . "'>" . $icon . '</button>';
    } elseif ('keine gegenteiligen Indizien' == $checked) {
        $button = "<button type='button' id=check_" . $name . ' onclick = "loadModal(check_' . $name . ")\" class='btn btn-warning btn-sm' data-toggle='modal' data-target='#commentModal' value=" . $checked . " title='Status: " . $checked . "'>" . $icon . '</button>';
    } elseif ('noch nicht bearbeitet' == $checked or '' == $checked) {
        $button = "<button type='button' id=check_" . $name . ' onclick = "loadModal(check_' . $name . ")\" class='btn btn-secondary btn-sm' data-toggle='modal' data-target='#commentModal' value=" . $checked . " title='Status: " . $checked . "'>" . $icon . '</button>';
    }
    $element = "<div class='col-lg-1 pull-right'>" . $button . '</div>';

    return $element;
}

// make class text danger if results from pf and db differ
function diff_pf_db($db_res_pf, $db_res_db)
{
    if ($db_res_pf != $db_res_db && $db_res_pf != '') {
        return "class='text-danger'";
    } else {
        return "class='text-secondary'";
    }
}

// just one line input
function one_line_input($name, $description, $db_res_db, $table, $type = 'text', $hl = true, $disabled = false, $ram = false)
{
    global $count;

    if ($ram) {
        $ram_pic = " <img src='../../img/ram.png' alt='RAMICRO' width='15' height='15'>";
    } else {
        $ram_pic = "";
    }

    // check if input is disabled
    $da = ($disabled) ? 'disabled' : '';

    if ($type == 'date') {
        $class = 'date-control';
    } else {
        $class = 'form-control';
    }

    // if ($name !=''){
    //     $name = ' <small>(' . $name . ')</small>';
    // }else{
    //     $name = '';
    // }

    $element = "<div id='" . $name . "'><div class='form-group'><label for='" . $name . "' class='col-lg-2 control-label'>" . $description .  $ram_pic .  '</label>'
        . "<div class='col-lg-8 " . $name . "'><input type='" . $type . "' " . $da . " class='" . $class . "'  name='target__" . $table . '__' . $name . "' id='target_" . $count . "' value ='" . $db_res_db . "'></div>"
        . '</div>' . hline($hl) . '</div>';
    ++$count;
    echo $element;
}

// function returns Ja if var = 'true', Nein if var = 'false' and XXX if var = 'xxx'
function tfJN($var)
{
    if ('true' == $var) {
        return 'Ja';
    } elseif ('false' == $var) {
        return 'Nein';
    } else {
        return $var;
    }
}

// Daten aus Plattform Datenbank sind im bescheidenen Format abgelegt
function DateFormating($date)
{
    if ('' != $date) {
        $dateArr = explode('.', $date);

        return $dateArr[2] . '-' . $dateArr[1] . '-' . $dateArr[0];
    }
}

// checked attribut fuer radio buttons
function checked($vari)
{
    if ('1' == $vari) {
        echo 'checked';
    }
}

function commentSection($num)
{
    return  "<span id='section" . $num . "_0' class='hide label label-info pull-right'></span>
        <span id='section" . $num . "_1' class='hide label label-success pull-right'></span>
        <span id='section" . $num . "_2' class='hide label label-primary pull-right'></span>
        <span id='section" . $num . "_3' class='hide label label-default pull-right'></span>";
}

// returns a <hr> element if its needed
function hline($hl)
{
    if ($hl) {
        return '<hr>';
    } else {
        return '';
    }
}

function toNum($strg, $delit)
{
    if (strpos($strg, $delit) != false) {
        $val = explode($delit, $strg)[0];
    } else {
        $val = $strg;
    }
    return $val;
}

function rsvANum($rsv)
{
    if ($rsv == 'ADAC') {
        return 'ADAC [70376]';
    } elseif ($rsv == 'AdvoCard') {
        return 'AdvoCard [36]';
    } elseif ($rsv == 'Allianz') {
        return 'Allianz [285]';
    } elseif ($rsv == 'Allrecht') {
        return 'Allrecht [121]';
    } elseif ($rsv == 'ARAG') {
        return 'ARAG [7134]';
    } elseif ($rsv == 'Auxilia') {
        return 'Auxilia [128]';
    } elseif ($rsv == 'BBV (Bayerische Beamten Versicherung)') {
        return 'BBV (Bayerische Beamten Versicherung) [2895]';
    } elseif ($rsv == 'BGV (Badische Versicherungen)') {
        return 'BGV (Badische Versicherungen) [224]';
    } elseif ($rsv == 'Bruderhilfe (jetzt HUK)') {
        return 'Bruderhilfe (jetzt HUK) [6]';
    } elseif ($rsv == 'Concordia') {
        return 'Concordia [432]';
    } elseif ($rsv == 'Continentale') {
        return 'Continentale [4449]';
    } elseif ($rsv == 'D.A.S. (jetzt ERGO)') {
        return 'D.A.S. (jetzt ERGO) [43]';
    } elseif ($rsv == 'Debeka') {
        return 'Debeka [60]';
    } elseif ($rsv == 'Deurag') {
        return 'Deurag [135]';
    } elseif ($rsv == 'DEVK') {
        return 'DEVK [97]';
    } elseif ($rsv == 'DMB') {
        return 'DMB [517]';
    } elseif ($rsv == 'Gegenseitigkeit') {
        return 'Gegenseitigkeit [747]';
    } elseif ($rsv == 'HDI-Gerling jetzt Roland') {
        return 'HDI-Gerling jetzt Roland [66]';
    } elseif ($rsv == 'Huk-Coburg') {
        return 'Huk-Coburg [6]';
    } elseif ($rsv == 'IDEAL') {
        return 'IDEAL [70065]';
    } elseif ($rsv == 'Itzehoer') {
        return 'Itzehoer [7796]';
    } elseif ($rsv == 'Jurpartner Service GmbH') {
        return 'Jurpartner Service GmbH [26000]';
    } elseif ($rsv == 'Karlsruher (jetzt Württembergische)') {
        return 'Karlsruher (jetzt Württembergische) [204]';
    } elseif ($rsv == 'LVM') {
        return 'LVM [455]';
    } elseif ($rsv == 'Mecklenburgische') {
        return 'Mecklenburgische [1035]';
    } elseif ($rsv == 'NORDVERS') {
        return 'NORDVERS [27048]';
    } elseif ($rsv == 'NRV') {
        return 'NRV [78]';
    } elseif ($rsv == 'ÖRAG') {
        return 'ÖRAG [30]';
    } elseif ($rsv == 'R+V') {
        return 'R+V [117]';
    } elseif ($rsv == 'Roland') {
        return 'Roland [66]';
    } elseif ($rsv == 'RS-Union (jetzt Itzehoer)') {
        return 'RS-Union (jetzt Itzehoer) [69]';
    } elseif ($rsv == 'VGH') {
        return 'VGH [463]';
    } elseif ($rsv == 'WGV') {
        return 'WGV [18]';
    } elseif ($rsv == 'Württembergische') {
        return 'Württembergische [204]';
    } elseif ($rsv == 'Uelzener') {
        return 'Uelzener [989]';
    } elseif ($rsv == 'Zurich') {
        return 'Zurich [14461]';
    } elseif ($rsv == 'Andere') {
        return 'Andere [47]';
    } else {
        return $rsv;
    }
}

function eXcolon($strg)
{
    if (strpos($strg, ': ') >= 1) {
        $rtstrg = explode(': ', $strg)[1];
    } elseif (strpos($strg, ':') >= 1) {
        $rtstrg = explode(':', $strg)[1];
    } else {
        $rtstrg = $strg;
    }
    return $rtstrg;
}
