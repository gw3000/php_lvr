<?php

/**
 * Footer Doc Comment
 * PHP Version 7.
 *
 * @category  Partial
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

?>

<footer>
    <div class="row">
        <div class="col-lg-12 footer-desc">
            <?php
            if (isset($row_rsv['last_update'])) {
                echo "<span>letzte Änderung: " . substr($row_rsv['last_update'], 0, 19) . " </span>";
            } elseif (isset($max_import)) {
                echo "<span>letzter Import: " . date_format(new DateTime($max_import), "Y-m-d H:i:s") . " </span>";
            }
            ?>
            <hr class="my-4">
            <span class="pull-right">angemeldet als: <?php echo $_SESSION['name'] . ' | seit: ' . $_SESSION["login"];  ?></span>
            <span class="hidden" id="user_group"><?php echo $_SESSION['role']; ?></span>
        </div>
    </div>
</footer>
