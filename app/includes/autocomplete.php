<?php
/**
 * Autocompletion Doc Comment
 * PHP Version 7.
 *
 * @category  Worker
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020  Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'db.php';

$action = $_GET['autocomplete'];
$getData = htmlspecialchars(trim(strip_tags($_GET['term'])));

if ($action == 'refnum') {
    $stmt = $pdo->prepare("SELECT refnum FROM t_contracts WHERE refnum ILIKE :refnum LIMIT 10");
    $stmt->execute(array(':refnum' => $getData . '%'));
    while ($row = $stmt->fetch()) {
        $data[] = $row['refnum'];
    }
} elseif ($action == 'contnum') {
    $stmt = $pdo->prepare("(SELECT contnum FROM t_contracts WHERE contnum ILIKE :contnum) UNION (SELECT \"2b\" FROM t_cont_bd WHERE \"2b\" ILIKE :contnum) LIMIT 10");
    $stmt->execute(array(':contnum' => $getData . '%'));
    while ($row = $stmt->fetch()) {
        $data[] = $row['contnum'];
    }
} elseif ($action == 'name') {
    $stmt = $pdo->prepare("SELECT distinct(concat(lname, ', ', fname)) as name FROM t_address WHERE lname ILIKE :lname LIMIT 10");
    $stmt->execute(array(':lname' => $getData . '%'));
    while ($row = $stmt->fetch()) {
        $data[] = $row['name'];
    }
} elseif ($action == 'insurance') {
    $data = array();
    // $stmt = $pdo->prepare("SELECT id, selection, presentation, opponent FROM t_isr WHERE selection ILIKE :selection");
    $stmt = $pdo->prepare("SELECT id, CASE WHEN country = 'Österreich' THEN CONCAT('[A] ', selection) ELSE selection END, opponent FROM t_isr WHERE selection ILIKE :selection");
    $stmt->execute(array(':selection' => '%' . $getData . '%'));
    while ($row = $stmt->fetch()) {
        array_push($data, array("id" => $row["id"], "label" => $row["selection"], "value" => $row["selection"], "presentation" => strip_tags($row["presentation"]), "opponent" => strip_tags($row["opponent"])));
    }
}

$output = json_encode($data);

if ($_GET["callback"]) {
    // Escape special characters to avoid XSS attacks via direct loads of this
    // page with a callback that contains HTML. This is a lot easier than validating
    // the callback name.
    $output = htmlspecialchars($_GET["callback"]) . "($output);";
}

echo $output;
