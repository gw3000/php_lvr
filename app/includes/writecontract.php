<?php

/**
 * Write All Variables to DB
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// var_dump($_POST);

// set default timezone
date_default_timezone_set('Europe/Berlin');

// unset variables
unset($sql);
unset($stmt);

foreach ($_POST as $key => $value) {
    $tr = explode('__', $key);
    if ($tr[0] == 'target') {
        // tr = table -> row
        $sql = "UPDATE " . $tr[1] . " SET \"" . $tr[2] . "\" = :value, last_update = NOW(), last_user = :last_user WHERE id = :id;";
        $stmt = $pdo->prepare($sql);
        $value = trim($value);
        if (empty($value)) {
            $value = null;
        }
        $stmt->bindParam(':value', $value);
        $stmt->bindParam(':id', $_SESSION['id']);
        $stmt->bindParam(':last_user', $_SESSION['email']);
        $stmt->execute();
    }
}

// unset variables
unset($sql);
unset($stmt);


// **********************************
// Trigger to add contract in RAMICRO
// **********************************

// check if contract is in mssql->ram->eloakte
$sql = "SELECT id_ms, in_ram, refnum FROM t_cont_ms cm INNER JOIN t_contracts co ON cm.id=co.id WHERE cm.id= :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id']);
$stmt->execute();
$ms = $stmt->fetch(PDO::FETCH_ASSOC);

// aktenanlage in ramicro
// if (($ms['id_ms'] == '' && $_POST["target__t_cont_ms__id_ms"] == '1010') && ($ms['in_ram'] == '' || $ms['in_ram'] != 1)) {
//     $create_record = true;
// } elseif (($ms['id_ms'] == '9998' && $_POST["target__t_cont_ms__id_ms"] == '1010') && ($ms['in_ram'] == '' || $ms['in_ram'] != 1)) {
//     $create_record = true;
// } elseif (($ms['id_ms'] == '1020' && $_POST["target__t_cont_ms__id_ms"] == '1010') && ($ms['in_ram'] == '' || $ms['in_ram'] != 1)) {
//     $create_record = true;
// } elseif (($ms['id_ms'] == '1030' && $_POST["target__t_cont_ms__id_ms"] == '1040') && ($ms['in_ram'] == '' || $ms['in_ram'] != 1)) {
//     $create_record = true;
// } else {
//     $create_record = false;
// }

$create_record = true;
if ($create_record == true) {
    $sql = "notify lvr_mssql_worker, '" . $ms['refnum'] . "';";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    unset($sql);
    unset($stmt);
}
