<?php

/**
 * Section 02 File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2019 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
// Tabellenname in der Datenbank
$table = "t_cont_sa";

// Platform Datenabfrage
$sql = "SELECT
    id,
    data->'a03aResponse'->>'value' as \"3a\",
    data->'a03bResponse'->>'value' as \"3b\",
    data->'a03cResponse'->>'value' as \"3c\",
    data->'a03dResponse'->>'value' as \"3d\",
    data->'a03eResponse'->>'value' as \"3e\",
    data->'a03fResponse'->>'value' as \"3f\",
    data->'a03fAdditionalResponse'->>'value' as \"3f_tdfl\",
    data->'a03gResponse'->>'value' as \"3g\",
    data->'a03gResponse'->>'value' as \"3ga\",
    data->'a03g1Response'->>'value' as \"3g1\",
    data->'a03hResponse'->>'value' as \"3h\",
    data->'a03iResponse'->>'value' as \"3i\",
    data->'a03jResponse'->>'value' as \"3j\",
    data->'a03kResponse'->>'value' as \"3k\",
    data->'a03lResponse'->>'value' as \"3l\",
    data->'a03mResponse'->>'value' as \"3m\",
    data->'a03nResponse'->>'value' as \"3n\",
    data->'a03oResponse'->>'value' as \"3o\",
    data->'a03pResponse'->>'value' as \"3p\",
    data->'a03qResponse'->>'value' as \"3q\"
FROM
    t_contracts
WHERE
    id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_pf = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

// Datenbak Abfrage
$sql = "SELECT * FROM $table WHERE id = :id;";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
$stmt->execute();
$res_db = $stmt->fetch(PDO::FETCH_ASSOC);
unset($stmt);

# TDFL
if ($res_pf['3f'] == 'TDFL pauschal bei Vertragsbeginn (Bitte in Euro angeben)') {
    $res_pf['3f'] = 'TDFL pauschal bei Vertragsbeginn';
}

// Formular Teil Situationsabfrage
radio('3a', 'Abschlussland Deutschland', ['Ja', 'Nein'], $res_pf['3a'], $res_db['3a'], $table, false);
dropdown('3b', 'Vertragsart', ['', 'Basis-/Rüruprente', 'Riesterrente', 'sonstiger Renten- oder Lebensversicherungsvertrag'], $res_pf['3b'], $res_db['3b'], $table);
dropdown('3c', 'Vertragsstatus', ['laufender Vertrag', 'beendeter Vertrag (gekündigt oder abgelaufen)'], $res_pf['3c'], $res_db['3c'], $table);
dropdown('3f', 'Todesfallleistung', ['', 'Auszahlung des Vertragsguthabens', 'Auszahlung von 101% des Fondsguthabens', 'Beitragsrückgewähr', 'Beitragsbefreiung', 'TDFL pauschal bei Vertragsbeginn', 'nicht bekannt'], $res_pf['3f'], $res_db['3f'], $table);
text_number('3fa', 'TDFL pauschal bei Vertragsbeginn', '0.01', '0', '1000000', toNum($res_pf['3f_tdfl'], '€'), $res_db['3fa'], $table);


// Zusatzversicherung  (3g)
$res3ga = substr($res_pf['3ga'], 1, -1);
$res3ga = str_replace('", "', '","', $res3ga);
$res3ga = str_replace('"', '', $res3ga);
$res3ga_arr = explode(',', $res3ga);
$desc_col = array(['3ga', 'Berufsunfähigkeit (Beitragsbefreiung)'], ['3gb', 'Berufsunfähigkeit (Rente)'], ['3gc', 'Unfallversicherung'], ['3gd', 'Invaliditätsversicherung '], ['3ge', 'Pflegeversicherung'], ['3gf', 'sonstige Zusatzversicherung außer Todesfallabsicherung (bitte angeben) '], ['3gg', 'schwere Krankheit'], ['3gh', 'keine Zusatzversicherung']);
checkbox('3g', 'Zusatzversicherung', $desc_col, $res3ga_arr, $res_db, $table);
?>

<fieldset>
    <!-- sonstige zusatzversicherung -->
    <div id='3ga'>
        <div class="form-group">
            <label for="otherInsurance" id="otherInsuranceHeading" class="col-lg-2 control-label">sonstige
                Versicherung (3ga)</label>
            <div class="col-lg-4">
                <input type="text" class="form-control" id="otherInsuranceTextSource" name="source__t_cont_sa__3gi" placeholder="sonstige Versicherung" value="">
            </div>
            <div class="col-lg-4 3ga">
                <input type="text" class="form-control" id="otherInsuranceTextTarget" name="target__t_cont_sa__3gi" placeholder="sonstige Versicherung" value="<?php echo $res_db['3gi']; ?>">
            </div>
        </div>
        <hr>
    </div>

    <?php
    radio('3g1', 'Zusatzversicherung nachträglich', ['Ja', 'Nein'], $res_pf['3g1'], $res_db['3g1'], $table);
    radio('3h', 'Risikofall eingetreten', ['Ja', 'Nein'], $res_pf['3h'], $res_db['3h'], $table);
    radio('3i', 'Verkauf oder Ankauf', ['Ja', 'Nein'], $res_pf['3i'], $res_db['3i'], $table);
    radio('3j', 'Abtretung allgemein', ['Ja', 'Nein'], $res_pf['3j'], $res_db['3j'], $table);
    radio('3k', 'Abtretung bei Vertragsschluss ', ['Ja', 'Nein'], $res_pf['3k'], $res_db['3k'], $table);
    radio('3l', 'Abtretung mehrfach', ['Ja', 'Nein'], $res_pf['3l'], $res_db['3l'], $table);
    radio('3m', 'Abtretung laufend', ['Ja', 'Nein'], $res_pf['3m'], $res_db['3m'], $table);
    radio('3n', 'Prozess', ['Ja', 'Nein'], $res_pf['3n'], $res_db['3n'], $table);
    dropdown('3o', 'Prozessdetail', ['', 'Der Prozess wurde teilweise oder voll gewonnen', 'Es wurde ein Vergleich abgeschlossen', 'Der Prozess wurde verloren'], $res_pf['3o'], $res_db['3o'], $table);
    radio('3p', 'eigener Widerspruch/Widerruf/Rücktritt ', ['Ja', 'Nein'], $res_pf['3p'], $res_db['3p'], $table);
    date_input('3q', 'Datum eigener Widerspruch/Widerruf/Rücktritt ', $res_pf['3q'], $res_db['3q'], $table, false);
    ?>
</fieldset>
