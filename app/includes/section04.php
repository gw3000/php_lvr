<?php

/**
 * Section 04 File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */
?>

<fieldset>
    <!-- <legend>Unterlagen</legend> -->

    <?php
    // Tabellen name in der Datenbank
    $table = "t_cont_ul";

    // Platform Datenabfrage
    $sql = "SELECT
    id,
    data->'a13aResponse'->>'value' as \"13a\",
    data->'a13bResponse'->>'value' as \"13b\",
    data->'a13cResponse'->>'value' as \"13c\",
    data->'a13dResponse'->>'value' as \"13d\",
    data->'a13eResponse'->>'value' as \"13e\",
    data->'a13fResponse'->>'value' as \"13f\"
    FROM
    t_contracts
    WHERE
    id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_pf = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Datenbak Abfrage
    $sql = "SELECT * FROM $table WHERE id = :id;";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(':id', $_SESSION['id'], PDO::PARAM_STR);
    $stmt->execute();
    $res_db = $stmt->fetch(PDO::FETCH_ASSOC);
    unset($stmt);

    // Formular Teil Unterlagen
    radio('13a', 'Unterlagen Abschluss', ['ursprüngliche(r) Versicherungsschein/-police', 'andere Unterlage, aus der sich der Beginn des Vertrages und der Umfang des Versicherungsschutzes ergibt (z.B. Versicherungsantrag)'], $res_pf['13a'], $res_db['13a'], $table);
    radio('13b', 'Unterlagen Beendigung', ['Abrechnungsschreiben des Versicherers', 'andere Unterlage, aus der sich der Zeitpunkt der Beendigung und die Auszahlungshöhe ergibt (z.B. Kontoauszug)', 'Vertrag ohne Gegenwert erloschen'], $res_pf['13b'], $res_db['13b'], $table);
    radio('13c', 'Unterlagen laufend', ['aktuelle Wertmitteilung', 'andere Unterlage, aus der sich der aktuelle Wert des Vertrages ergibt'], $res_pf['13c'], $res_db['13c'], $table);
    radio('13d', 'Letzte vorliegende Wertermittlung zum beendeten Versicherungsvertrag', ['Ja', 'Nein'], $res_pf['13d'], $res_db['13d'], $table);
    radio('13f', 'Erklärung Erbschein', ['Ja', 'Nein'], $res_pf['13f'], $res_db['13f'], $table);

    ?>
</fieldset>