<?php

/**
 * Password Reset File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

// init vars
$pwd_old = $pwd_new = $pwd_new_confirm = '';
$pwd_old_err = $pwd_new_err = $pwd_new_confirm_err = '';
$_SESSION['id'] = '';
// echo $_SESSION['email'];

// process form when post submit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // sanitize post
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    // put post vars in regular vars
    $pwd_old = trim($_POST['pwd_old']);
    $pwd_new = trim($_POST['pwd_new']);
    $pwd_new_confirm = trim($_POST['pwd_new_confirm']);

    // check if input fields are empty
    $pwd_old_cont = (!empty($pwd_old)) ? 1 : 0;
    $pwd_new_cont = (!empty($pwd_new)) ? 10 : 0;
    $pwd_new_confirm_cont = (!empty($pwd_new_confirm)) ? 100 : 0;

    // build an error pattern
    $err_num = $pwd_old_cont + $pwd_new_cont + $pwd_new_confirm_cont;

    // evaluate the error pattern and write error messages
    if ($err_num == 0) {
        $pwd_old_err = 'Bitte geben Sie ihr altes Passwort ein! Und ...';
        $pwd_new_err = '... geben ein neues Passwort ein ...';
        $pwd_new_confirm_err = '.. und wiederholen ihr neues Passwort!';
    } elseif ($err_num >= 1 && $err_num < 111) {
        if ($err_num == 1) {
            $pwd_new_err = 'Bitte das neue Passwort eingeben und ...';
            $pwd_new_confirm_err = '... das neue Passwort bestätigen!';
        } elseif ($err_num == 10) {
            $pwd_old_err = 'Bitte das alte Passwort eingeben! Und ...';
            $pwd_new_confirm_err = '... das neue Passwort wiederholen!';
        } elseif ($err_num == 11) {
            $pwd_new_confirm_err = 'Bitte das neue Passwort bestätigen!';
        } else {
            $pwd_old_err = 'Bitte geben Sie ihr altes Passwort ein! Und ...';
            $pwd_new_err = '... geben ein neues Passwort ein ...';
            $pwd_new_confirm_err = '.. und wiederholen ihr neues Passwort!';
        }
    } else {
        if (strlen($pwd_new) < 4) {
            $pwd_new_err = $pwd_new_confirm_err = 'Das Password sollte mindestens 3 Zeichen beinhalten!';
        } elseif ($pwd_new != $pwd_new_confirm) {
            $pwd_new_err = 'Die Passwörter ...';
            $pwd_new_confirm_err = '... müssen übereinstimmen!';
        } elseif ($pwd_new == $pwd_old) {
            $pwd_old_err = 'Das alte Passwort ...';
            $pwd_new_err = '... und das neue Passwort dürfen nicht gleich sein!';
        } else {
            $pwd_new = password_hash($pwd_new, PASSWORD_DEFAULT);
            //prepare statement
            $sql = "SELECT password from t_users WHERE email = :email;";
            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(':email', $_SESSION['email'], PDO::PARAM_STR);
                if ($stmt->execute()) {
                    // Check if refnum exists
                    if ($row = $stmt->fetch()) {
                        unset($stmt);
                        if (password_verify($pwd_old, $row['password'])) {
                            $sql = "UPDATE t_users SET password=:pwd_new WHERE email=:email;";
                            $stmt = $pdo->prepare($sql);
                            $stmt->bindParam(':pwd_new', $pwd_new, PDO::PARAM_STR);
                            $stmt->bindParam(':email', $_SESSION['email'], PDO::PARAM_STR);
                            if ($stmt->execute()) {
                                $pwdSuccess = "Passwort wurde erfolgreich geändert!";
                            }
                        }
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
    }
}

// queries
if (empty($ida_err) && empty($name_err) && empty($contnum_err) && empty($refnum_err)) {
    if (!empty($ida)) {
        $sql = 'SELECT id FROM t_address WHERE id = :ida';
        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables
            $stmt->bindParam(':ida', $ida, PDO::PARAM_INT);
            // Attempt to execute
            if ($stmt->execute()) {
                // Check if refnum exists
                if ($stmt->rowCount() === 0) {
                    $ida_err = 'ID existiert nicht! Versuchen Sie es mit einem anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: address.php?ida=' . $ida);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
        // prepare refnum
    } elseif (!empty($refnum)) {
        // if input has NO slash
        if (!strpos($refnum, '/') > 0) {
            $refnum_core = substr($refnum, 0, strlen($refnum) - 2);
            $refnum_year = substr($refnum, -2);
            $refnum = $refnum_core . "/" . $refnum_year;
        }
        $sql = 'SELECT id from t_contracts WHERE refnum = :refnum';
        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables
            $stmt->bindParam(':refnum', $refnum, PDO::PARAM_STR);
            // Attempt to execute
            if ($stmt->execute()) {
                // Check if refnum exists
                if ($stmt->rowCount() === 0) {
                    $refnum_err = 'Aktenzeichen existiert nicht! Versuchen Sie es mit einer anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: contract.php?id=' . $row['id']);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
    } elseif (!empty($contnum)) {
        // prepare contnum
        $sql = 'SELECT id from t_contracts WHERE contnum = :contnum';
        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables
            $stmt->bindParam(':contnum', $contnum, PDO::PARAM_STR);
            // Attempt to execute
            if ($stmt->execute()) {
                // Check if refnum exists
                if ($stmt->rowCount() === 0) {
                    $contnum_err = 'Vertrag existiert nicht! Versuchen Sie es mit einer anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: contract.php?id=' . $row['id']);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
    } elseif (!empty($name)) {
        // prepare name
        if (strpos($name, ',') > 0) {
            list($lname, $fname) = explode(',', $name);
            $lname = trim(htmlspecialchars(trim($lname)));
            $fname = trim(htmlspecialchars(trim($fname)));
            $sql = "SELECT id FROM t_address WHERE lname like :lname AND fname like :fname";
            if ($stmt = $pdo->prepare($sql)) {
                // Bind variables
                $stmt->bindParam(':lname', $lname, PDO::PARAM_STR);
                $stmt->bindParam(':fname', $fname, PDO::PARAM_STR);
                // Attempt to execute
                if ($stmt->execute()) {
                    // Check if refnum exists
                    if ($stmt->rowCount() === 0) {
                        $name_err = 'Adresse existiert nicht! Versuchen Sie es mit einer anderen!';
                    } elseif ($stmt->rowCount() === 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: address.php?ida=' . $row['id']);
                        }
                    } elseif ($stmt->rowCount() > 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: results.php?lname=' . $lname . '&fname=' . $fname);
                        }
                    }
                } else {
                    die('Something went wrong');
                }
            }
            unset($stmt);
            unset($sql);
        } else {
            $sql = "SELECT * FROM t_address WHERE lname like :lname";
            $lname = $name;
            if ($stmt = $pdo->prepare($sql)) {
                // Bind variables
                $stmt->bindParam(':lname', trim($lname), PDO::PARAM_STR);
                // Attempt to execute
                if ($stmt->execute()) {
                    // Check if refnum exists
                    if ($stmt->rowCount() === 0) {
                        $name_err = 'Name existiert nicht! Versuchen Sie es mit einem anderen!';
                    } elseif ($stmt->rowCount() === 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: address.php?ida=' . $row['id']);
                        }
                    } elseif ($stmt->rowCount() > 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: results.php?lname=' . $lname);
                        }
                    }
                } else {
                    die('Something went wrong');
                }
            }
            unset($stmt);
            unset($sql);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <!-- jquery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>

    <title>lvr &middot; db</title>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-5">

                    <?php
                    if (empty($pwdSuccess)) {
                        echo "<h3 class='h3-spacing'>Passwort ändern</h3>";
                    } else {
                        echo "<h3 class='h3-spacing'>" . $pwdSuccess . "</h3>";
                    }

                    ?>

                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal" id="searching">

                        <!-- Passwort alt-->
                        <div class="form-group <?php echo (!empty($pwd_old_err)) ? 'has-warning' : ''; ?>">
                            <label for="pwd_old" class="col-lg-2 control-label">Passwort alt</label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control form-control-lg" name="pwd_old" id="pwd_old" placeholder="altes Passwort" onkeydown="resetForm()">
                                <span class="invalid-feedback"><?php echo $pwd_old_err; ?></span>
                            </div>
                        </div>

                        <!-- Passwort neu -->
                        <div class="form-group <?php echo (!empty($pwd_new_err)) ? 'has-warning' : ''; ?>">
                            <label for="pwd_new" class="col-lg-2 control-label">Passwort neu</label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control form-control-lg" name="pwd_new" id="pwd_new" placeholder="neues Passwort" onkeydown="resetForm()" title="Um die Suche auszuweiten, bitte ein '%' voranstellen! Wenn Sie nach einem Namen und nach einem Vornamen suchen, verwenden Sie bitte ein Komma um Vor- und Nachnamen abzutrennen! (Nachname, Vorname)">
                                <span class="invalid-feedback"><?php echo $pwd_new_err; ?></span>
                            </div>
                        </div>

                        <!-- Passwort neu 2 -->
                        <div class="form-group <?php echo (!empty($pwd_new_confirm_err)) ? 'has-warning' : ''; ?>">
                            <label for="pwd_new_confirm" class="col-lg-2 control-label">Passwort neu bestätigen</label>
                            <div class="col-lg-9">
                                <input type="password" class="form-control form-control-lg" name="pwd_new_confirm" id="pwd_new_confirm" placeholder="neues Passwort bestätigen" onkeydown="resetForm()" title="Um die Suche auszuweiten, bitte ein '%' voranstellen!">
                                <span class="invalid-feedback"><?php echo $pwd_new_confirm_err; ?></span>
                            </div>
                        </div>

                        <!-- Rücksetzen und Suchen -->
                        <div class="form-group">
                            <div class="btn-group col-lg-9 col-lg-offset-2">
                                <button type="reset" onclick="resetForm()" class="btn btn-default col-lg-6 col-md-6 col-sm-6">Rücksetzen</button>
                                <button type="submit" class="btn btn-success col-lg-6 col-md-6 col-sm-6">Passwort
                                    ändern</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
</body>

</html>