<?php

/**
 * Search File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

// init vars
$ida = $name = $contnum = $refnum = '';
$ida_err = $name_err = $contnum_err = $refnum_err = '';
$ida_cont = $name_cont = $contnum_cont = $refnum_cont = '';
$_SESSION['id'] = '';

// process form when post submit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // sanitize post
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    // put post vars in regular vars
    $ida = trim($_POST['inputIDA']);
    $name = trim($_POST['inputName']);
    $contnum = trim($_POST['inputContNum']);
    $refnum = trim($_POST['inputRefNum']);

    // check if input fields are empty
    $ida_cont = (!empty($ida)) ? '1' : '0';
    $name_cont = (!empty($name)) ? '1' : '0';
    $contnum_cont = (!empty($contnum)) ? '1' : '0';
    $refnum_cont = (!empty($refnum)) ? '1' : '0';

    // build an error pattern
    $err_input = $ida_cont . $name_cont . $contnum_cont . $refnum_cont;
    // check if there is only one written input field
    $err_num = substr_count($err_input, '1');

    // evaluate the error pattern and write error messages
    if ($err_num == 0) {
        $ida_err = 'Bitte eine ID eingeben! Oder...';
        $name_err = '... einen Namen eingeben! Oder...';
        $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
        $refnum_err = '... ein Aktenzeichen eingeben!';
    } elseif ($err_num > 1) {
        if ($err_input == '0011') {
            $contnum_err = 'Bitte eine Vertragsnummer eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '0101') {
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '0110') {
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
        } elseif ($err_input == '0111') {
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '1001') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '1010') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
        } elseif ($err_input == '1011') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '1100') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $name_err = 'Bitte einen Namen eingeben! Oder...';
        } elseif ($err_input == '1101') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        } elseif ($err_input == '1110') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
        } elseif ($err_input == '1111') {
            $ida_err = 'Bitte eine ID eingeben! Oder...';
            $name_err = 'Bitte einen Namen eingeben! Oder...';
            $contnum_err = '... eine Vertragsnummer eingeben! Oder...';
            $refnum_err = '... ein Aktenzeichen eingeben!';
        }
    } elseif ($err_num == 1) {
        if ($err_input == '0100' && strlen($name) < 4) {
            $name_err = 'Der Name sollte mindestens 3 Zeichen beinhalten!';
        }
    }
}

// queries
if (empty($ida_err) && empty($name_err) && empty($contnum_err) && empty($refnum_err)) {
    if (!empty($ida)) {
        $sql = 'SELECT id FROM t_address WHERE id = :ida';
        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables
            $stmt->bindParam(':ida', $ida, PDO::PARAM_INT);
            // Attempt to execute
            if ($stmt->execute()) {
                // Check if refnum exists
                if ($stmt->rowCount() === 0) {
                    $ida_err = 'ID existiert nicht! Versuchen Sie es mit einem anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: address.php?ida=' . $ida);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
    } elseif (!empty($refnum)) {
        // if input has NO slash
        if (!strpos($refnum, '/') > 0) {
            $refnum_core = substr($refnum, 0, strlen($refnum) - 2);
            $refnum_year = substr($refnum, -2);
            $refnum = $refnum_core . "/" . $refnum_year;
        }
        $sql = 'SELECT id from t_contracts WHERE refnum = :refnum';
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(':refnum', $refnum, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() === 0) {
                    $refnum_err = 'Aktenzeichen existiert nicht! Versuchen Sie es mit einer anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: contract.php?id=' . $row['id']);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
    } elseif (!empty($contnum)) {
        // prepare contnum
        $sql = "(SELECT id from t_contracts WHERE contnum = :contnum)
        UNION
        (SELECT	id FROM t_cont_bd WHERE \"2b\" = :contnum)";
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(':contnum', $contnum, PDO::PARAM_STR);
            if ($stmt->execute()) {
                if ($stmt->rowCount() === 0) {
                    $contnum_err = 'Vertrag existiert nicht! Versuchen Sie es mit einer anderen!';
                } elseif ($stmt->rowCount() === 1) {
                    if ($row = $stmt->fetch()) {
                        header('location: contract.php?id=' . $row['id']);
                    }
                }
            } else {
                die('Something went wrong');
            }
        }
        unset($stmt);
        unset($sql);
    } elseif (!empty($name)) {
        // prepare name
        if (strpos($name, ',') > 0) {
            list($lname, $fname) = explode(',', $name);
            $lname = trim(htmlspecialchars(trim($lname)));
            $fname = trim(htmlspecialchars(trim($fname)));
            $sql = "SELECT id FROM t_address WHERE lname ILIKE :lname AND fname ILIKE :fname";
            if ($stmt = $pdo->prepare($sql)) {
                // Bind variables
                $stmt->bindParam(':lname', $lname, PDO::PARAM_STR);
                $stmt->bindParam(':fname', $fname, PDO::PARAM_STR);
                // Attempt to execute
                if ($stmt->execute()) {
                    // Check if refnum exists
                    if ($stmt->rowCount() === 0) {
                        $name_err = 'Adresse existiert nicht! Versuchen Sie es mit einer anderen!';
                    } elseif ($stmt->rowCount() === 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: address.php?ida=' . $row['id']);
                        }
                    } elseif ($stmt->rowCount() > 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: results.php?lname=' . $lname . '&fname=' . $fname);
                        }
                    }
                } else {
                    die('Something went wrong');
                }
            }
            unset($stmt);
            unset($sql);
        } else {
            $sql = "SELECT * FROM t_address WHERE lname ILIKE :lname";
            $lname = $name;
            if ($stmt = $pdo->prepare($sql)) {
                // Bind variables
                $stmt->bindParam(':lname', trim($lname), PDO::PARAM_STR);
                // Attempt to execute
                if ($stmt->execute()) {
                    // Check if refnum exists
                    if ($stmt->rowCount() === 0) {
                        $name_err = 'Name existiert nicht! Versuchen Sie es mit einem anderen!';
                    } elseif ($stmt->rowCount() === 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: address.php?ida=' . $row['id']);
                        }
                    } elseif ($stmt->rowCount() > 1) {
                        if ($row = $stmt->fetch()) {
                            header('location: results.php?lname=' . $lname);
                        }
                    }
                } else {
                    die('Something went wrong');
                }
            }
            unset($stmt);
            unset($sql);
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/styles.css">

    <title>lvr &middot; db</title>

    <!-- jquery -->
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-lg-7 ">


                    <h3 class="h3-spacing">Suche</h3>

                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" class="form-horizontal" id="searching">
                        <div class="jumbotron">
                            <!-- IDA=address id-->
                            <div class="form-group <?php echo (!empty($ida_err)) ? 'has-warning' : ''; ?>">
                                <label for="inputIDA" class="col-lg-2 control-label">ID</label>
                                <div class="col-lg-10">
                                    <input type="number" class="form-control form-control-lg" name="inputIDA" id="inputIDA" placeholder="Mandanten Nummer (ID)" onkeydown="resetForm()">
                                    <span class="invalid-feedback"><?php echo $ida_err; ?></span>
                                </div>
                            </div>

                            <!-- Name -->
                            <div class="form-group <?php echo (!empty($name_err)) ? 'has-warning' : ''; ?>">
                                <label for="inputName" class="col-lg-2 control-label">Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control form-control-lg" name="inputName" id="inputName" placeholder="Name, Vorname" onkeydown="resetForm()" title="Um die Suche auszuweiten, bitte ein '%' voranstellen! Wenn Sie nach einem Namen und nach einem Vornamen suchen, verwenden Sie bitte ein Komma um Vor- und Nachnamen abzutrennen! (Nachname, Vorname)">
                                    <span class="invalid-feedback"><?php echo $name_err; ?></span>
                                </div>
                            </div>

                            <!-- Vertragsnummer -->
                            <div class="form-group <?php echo (!empty($contnum_err)) ? 'has-warning' : ''; ?>">
                                <label for="inputContNum" class="col-lg-2 control-label">Vertrag</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control form-control-lg" name="inputContNum" id="inputContNum" placeholder="Vertragsnummer" onkeydown="resetForm()" title="Um die Suche auszuweiten, bitte ein '%' voranstellen!">
                                    <span class="invalid-feedback"><?php echo $contnum_err; ?></span>
                                </div>
                            </div>

                            <!-- Aktenzeichen -->
                            <div class="form-group <?php echo (!empty($refnum_err)) ? 'has-warning' : ''; ?>">
                                <label for="inputRefNum" class="col-lg-2 control-label">Aktenzeichen</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control form-control-lg" name="inputRefNum" id="inputRefNum" placeholder="Aktenzeichen" onkeydown="resetForm()" title="Um die Suche auszuweiten, bitte ein '%' voranstellen! Beim Eingeben eines Aktenzeichens können sie slash (/) weglassen!">
                                    <span class="invalid-feedback"><?php echo $refnum_err; ?></span>
                                </div>
                            </div>

                            <hr class="my-4">
                            <!-- Rücksetzen und Suchen -->
                            <div class="form-group">
                                <div class="btn-group col-lg-10 col-sm-12 col-md-12 col-lg-offset-2">
                                    <button type="reset" onclick="resetForm()" class="btn btn-default col-lg-6 col-md-6 col-sm-6">Rücksetzen</button>
                                    <button type="submit" class="btn btn-primary col-lg-6 col-md-6 col-sm-6">Suche</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/main.js"></script>
    <script src="js/search.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/mousewheel.js"></script>
</body>

</html>