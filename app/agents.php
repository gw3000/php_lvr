<?php

/**
 * Agents Table
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

require_once 'includes/header.php';

// query import timestamp
$sql_ts = "SELECT max(inserted_at) FROM public.t_import_agents;";
$stmt_ts = $pdo->prepare($sql_ts);
$stmt_ts->execute();
$row_ts = $stmt_ts->fetch(PDO::FETCH_ASSOC);
$max_import = $row_ts['max'];

// query
$sql = "SELECT * FROM t_agents WHERE id > 3;";
$stmt = $pdo->prepare($sql);
$stmt->execute();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <meta http-equiv="X-UA-Compatible" content="ie=edge"> -->

    <title>lvr &middot; db</title>

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/custom.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <!-- css bootstrap datatables stuff -->
    <link rel="stylesheet" href="css/datatables.min.css" />

    <!-- js datatables stuff -->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/datatables.min.js"></script>

    <!-- init the table -->
    <script>
        // datatables jsquery
        $(document).ready(function() {
            $('#agentsTable').DataTable({
                "language": {
                    "url": "json/German.json"
                },
                "lengthMenu": [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "Alle"]
                ],
                "columnDefs": [
                            { width: 50, targets: 0 },
                            { width: 150, targets: 1 },
                            { width: 140, targets: 2 },
                            { width: 140, targets: 3 },
                            { width: 140, targets: 4 },
                            { width: 100, targets: 5 },
                        ],
                "fixedColumns": true,
                "stateSave": true,
                "localStorage": 1,
                "scrollCollapse": true,
                "scrollY": 550,
                "scrollX": true,
            });
        });

        // make rows to links
        $('tr[data-href]').on("click", function() {
            document.location = $(this).data('href');
        });
    </script>

</head>

<body>
    <!-- navbar -->
    <?php require_once 'includes/navbar.php'; ?>

    <!-- main part -->
    <div class="container">
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h3>Übersicht aller Vermittler</h3>
                </div>

                <!-- table -->
                <div class="col-lg-12 col-md-12 col-sm-12 tabley">
                    <table id="agentsTable" class="display table table-bordered table-striped table-hover cell-border nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nachname</th>
                                <th>Vorname</th>
                                <th>Firma</th>
                                <th>Email</th>
                                <!--<th>FID</th>-->
                                <th>Rolle</th>
                                <th>Status</th>
                                <th>Anlagedatum</th>
                                <!-- <th>in RAM</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $num = 1;
                            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

                                // status of agent in german language
                                if ($row['status'] == 'AccessGranted') {
                                    $status = 'Zugriff gewährt';
                                } elseif ($row['status'] == 'PasswordRequired') {
                                    $status = 'Passwort benötigt';
                                } elseif ($row['status'] == 'VerificationRequired') {
                                    $status = 'Überprüfung erforderlich';
                                } elseif ($row['status'] == 'Deactivated') {
                                    $status = 'Account deaktiviert';
                                } else {
                                    $status = $row['status'];
                                }

                                // roles in german language
                                if ($row['role'] == 'admin') {
                                    $role = 'Administrator';
                                } elseif ($row['role'] == 'manager') {
                                    $role = 'Verwalter';
                                } elseif ($row['role'] == 'agent') {
                                    $role = 'Vermittler';
                                } elseif ($row['role'] == 'subagent') {
                                    $role = 'Untervermittler';
                                } else {
                                    $role = $row['role'];
                                }

                                // human readable date
                                $date = new DateTime($row['created_at']);

                                // colorized lines if status not granted
                                if ($row['status'] == 'VerificationRequired') {
                                    $hclass = 'danger';
                                } elseif ($row['status'] == 'PasswordRequired') {
                                    $hclass = 'info';
                                } else {
                                    $hclass = '';
                                }

                                echo "<tr onclick=\"parent.location='agent.php?id=$row[id]'\" class=$hclass>";
                                echo "<td>" . $row['id'] . "</td>";
                                echo "<td>" . $row['lastname'] . "</td>";
                                echo "<td>" . $row['firstname'] . "</td>";
                                echo "<td>" . $row['company'] . "</td>";
                                echo "<td>" . $row['email'] . "</td>";
                                //echo "<td>" . $row['employeenumber'] . "</td>";
                                echo "<td>" . $role . "</td>";
                                echo "<td>" . $status . "</td>";
                                echo "<td>" . $date->format('Y-m-d') . "</td>";

                                if ($row['in_ram'] == true) {
                                    $checked = "<i class='fa fa-check' aria-hidden='true'></i>";
                                } else {
                                    $checked = "";
                                }
                                // echo "<td>" . $checked . "</td>";

                                echo " </tr> ";
                                $num++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <button id="delFilter" type="button" class="btn btn-primary" onclick="delFilter()">Tabellenfilter löschen</button>
        </div>
        <!-- footer -->
        <?php require_once 'includes/footer.php'; ?>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap3.min.js"></script>
    <script src="js/main.js"></script>
</body>

</html>
