<?php
// return data in json format
header("Content-Type: application/json; charset=UTF-8");

// what's the memory limit
$limit = ini_get('memory_limit');
// unlimit memory limitations and do heavy stuff
ini_set('memory_limit', -1);

// database incredentials
include_once "../../includes/db.php";

// what is the current url
$uri = explode("?", $_SERVER['REQUEST_URI']);

if ($uri[0] == '/api/v1/contracts.php') {
    // parameter limit
    // endpoint example: api/v1/contracts.php?limit=10
    // endpoint example: api/v1/contracts.php?limit=all
    if ($_GET['limit']) {
        if ($_GET['limit'] == 'all') {
            $limit = " ";
        } elseif ($_GET['limit'] > 0) {
            $limit = " limit " . $_GET['limit'];
        } elseif ($_GET['limit'] == 0) {
            $limit = " limit 0" . $_GET['limit'];
        }
    } else {
        $limit = 0;
    }

    // parameter contid
    // endpoint example: api/v1/contracts.php?contid='0f45b7a9-6feb-43bf-80ad-e4b89d4e6f30'
    if ($_GET['contid']) {
        $contid = $_GET['contid'];
        $limit = " limit 1";
    }
    $where = ($contid) ? " where co.id = " . $contid ." " : "";

    // parameter refnum
    // endpoint example: api/v1/contracts.php?refnum='55028/19'
    if ($_GET['refnum']) {
        $refnum = $_GET['refnum'];
        $limit = " limit 1";
    }
    $where = ($refnum) ? " where co.refnum = " . $refnum ." " : "";

    $sql = "SELECT
    ad.id as \"ID\",
    ad.lname as \"Nachname\",
    ad.fname as \"Vorname\",
    ad.street as \"Strasse\",
    ad.city as \"Stadt\",
    ad.zip as \"PLZ\",
    ad.country as \"Land\",
    ad.degree as \"Titel (Person 1)\",
    ad.salutation as \"Anrede (Person 1)\",
    ad.phone as \"Telefon (Person 1)\",
    ad.mobile as \"Telefon (Mobil) (Person 1)\",
    ad.mail as \"Email (Person 1)\",
    ad.lname2 as \"Nachname (Person 2)\",
    ad.fname2 as \"Vorname (Person 2)\",
    ad.degree2 as \"Titel (Person 2)\",
    ad.salutation2 as \"Anrede (Person 2)\",
    ad.mail2 as \"Email (Person 2)\",
    ad.clienttype as \"Mandanttyp\",
    ad.customerid as \"Vermittlernummer\",
    ad.in_ram as \"Addresse in RAM\",
    co.contnum as \"Vertragsnummer\",
    co.refnum as \"Aktenzeichen\",
    co.in_ram as \"Vertrag in RAM\",
    co.status as \"Status\",
    cb.\"9a\" as \"Mandatstyp\",
    cb.\"2b\" as \"Versicherungsnummer\",
    cb.\"2c\" as \"Eingabeberechtigung\",
    cb.\"2d\" as \"Ausschlussprodukte \",
    cb.\"2e\" as \"Insolvenz\",
    cb.\"2f\" as \"Selbstvermittlung\",
    cb.kapzr as \"kein Ausschlussprodukt bzw. -Zeitraum\",
    cb.bav as \"betriebliche Altersvorsorge (bAV)\",
    cb.vwl as \"vermögenswirksame Leistungen (VWL)\",
    cb.sgv as \"Sterbegeldversicherung\",
    cb.uvbrg as \"Unfallversicherung mit/ohne Beitragsrückgewähr\",
    cb.lvtf as \"Risikolebensversicherung\",
    cb.svrk as \"sonstige Versicherungen\",
    cb.kaiv as \"Kapitalanlage/Investment ohne Versicherung\",
    cb.aav as \"Antrag zum Versicherungsvertrag vor 01.01.1991 unterschrieben\",
    cb.vnd as \"Vertrag wurde nach dem 30.06.2010 abgeschlossen\",
    cb.vvd as \"Vertrag wurde vor dem 01.01.2003 vollständig beendet\",
    ca.\"3a\" as \"Abschlussland Deutschland\",
    ca.\"3b\" as \"Vertragsart\",
    ca.\"3c\" as \"Vertragsstatus\",
    ca.\"3f\" as \"Todesfallleistung\",
    ca.\"3fa\" as \"Todesfallleistung_a\",
    ca.\"3g\" as \"Zusatzversicherung\",
    ca.\"3ga\" as \"Berufsunfähigkeit Beitragsbefreiung\",
    ca.\"3gb\" as \"Berufsunfähigkeit Rente\",
    ca.\"3gc\" as \"Unfallversicherung\",
    ca.\"3gd\" as \"Invaliditätsversicherung\",
    ca.\"3ge\" as \"Pflegeversicherung\",
    ca.\"3gf\" as \"sonstige Zusatzversicherung\",
    ca.\"3gg\" as \"schwereKrankheit\",
    ca.\"3gh\" as \"keine Zusatzversicherung\",
    ca.\"3gi\" as \"sonstigen Versicherungen\",
    ca.\"3g1\" as \"Zusatzversicherung nachträglich\",
    ca.\"3h\" as \"Risikofall\",
    ca.\"3i\" as \"Verkauf\",
    ca.\"3j\" as \"Abtretung\",
    ca.\"3k\" as \"Abtretung bei Vertragsschluss\",
    ca.\"3l\" as \"Abtretung mehrfach\",
    ca.\"3m\" as \"Abtretung laufend\",
    ca.\"3n\" as \"Prozess\",
    ca.\"3o\" as \"Prozessdetail\",
    ca.\"3p\" as \"eigener Widerspruch Widerruf Ruecktritt\",
    ca.\"3q\" as \"Datum eigener Widerspruch Widerruf Ruecktritt\",
    cv.\"10a1\" as \"Mandant_urspuenglicher_Versicherungsnehmer\",
    cv.\"10a\" as \"urspruenglicher_Versicherungsnehmer\",
    cv.\"10c\" as \"Name urspr. Versicherungsnehmer / Erbengemeinschaft\",
    cv.\"10d\" as \"Geburtsdatum Mandant 1\",
    cv.\"10e\" as \"Geburtsdatum Mandant 2\",
    cv.\"10f\" as \"Geburtsdatum anderer Versicherungsnehmer\",
    cv.\"10g\" as \"Geburtsdatum weiterer Versicherungsnehmer\",
    cv.\"11a\" as \"versicherte Person 1\",
    cv.\"11b\" as \"Geburtsdatum versicherte Person 1\",
    cv.\"11c\" as \"Geschlecht versicherte Person 1\",
    cv.\"11f\" as \"weitere versicherte Person\",
    cv.\"11g\" as \"weitere versicherte Person Mandant 2\",
    cv.\"11h\" as \"Geburtsdatum versicherte Person 2\",
    cv.\"11i\" as \"Geschlecht versicherte Person 2\",
    cu.\"13a\" as \"Unterlagen Abschluss\",
    cu.\"13b\" as \"Unterlagen Beendigung\",
    cu.\"13c\" as \"Unterlagen laufend\",
    cu.\"13d\" as \"Abschlusserklärung Unterlagen\",
    cu.\"13f\" as \"Erklärung Erbschein\",
    cd.\"14a\" as \"Versicherer bei Vertragsabschluss\",
    isr.opponent,
    isr.presentation ,
    cd.\"14b\" as \"Anlageart\",
    cd.\"14c\" as \"technischer Vertragsbeginn\",
    cd.\"14d\" as \"bei Vertragsschluss vereinbarte Zahlungsdauer\",
    cd.\"14e\" as \"Beendigungsdatum\",
    cd.\"14g\" as \"Beitragszahlweise\",
    cd.\"14h\" as \"Einmalzahlung Höhe\",
    cd.\"14i\" as \"Ratenzahlung Art\",
    cd.\"14j\" as \"Ratenzahlung Höhe\",
    cd.\"14k\" as \"Ratenzahlung Änderungen\",
    cd.\"14l\" as \"Ratenzahlungen\",
    cd.\"14m\" as \"Beitragsfreistellung(en)\",
    cd.\"14p\" as \"Beitragsfreistellung Dauer\",
    cd.\"14q\" as \"Darlehen von der Lebensversicherung\",
    cd.\"14r\" as \"Auszahlungen während der Vertragslaufzeit\",
    cd.\"14s\" as \"Auszahlung Art\",
    cd.\"14t\" as \"Auszahlung Höhe gesamt\",
    cd.\"14u\" as \"Auszahlung nach Beendigung des Vertrages\",
    cd.\"14v\" as \"Auszahlung Datum\",
    cd.\"14w\" as \"Auszahlung Höhe\",
    cd.\"15\" as \"Weitere Angaben\",
    cm.dt_ra,
    cm.id_rav,
    rav.description as rav_description,
    cm.id_ms,
    ms.description ,
    ms.helptext,
    ms.helptext_pf,
    ms.helptext_wk,
    ms.deadline,
    ms.after_deadline,
    cm.dt_ms,
    cm.mbd_cont_doc,
    cm.ass_doc,
    cm.auth,
    rav.description,
    cr.id_rsv,
    rv.name,
    cr.vm_rsv,
    cr.id_rsv_status,
    cr.dt_chg_status,
    rs.description,
    cr.vs_num,
    cr.vs_arb,
    cr.ds,
    cr.sn,
    cr.bm,
    cr.last_update,
    cm.ms_checked,
    cm.tp_nr,
    ag.company,
    ag.firstname,
    ag.lastname,
    ag.email
from
    t_contracts co
inner join t_address ad on
    co.ida = ad.id
left join t_agents ag on
    ad.customerid = ag.id 
inner join t_cont_bd cb on
    co.id = cb.id
inner join t_cont_sa ca on
    co.id = ca.id
inner join t_cont_vn cv on
    co.id = cv.id
inner join t_cont_ul cu on
    co.id = cu.id
inner join t_cont_vd cd on
    co.id = cd.id
inner join t_cont_ms cm on
    co.id = cm.id
inner join t_cont_rsv cr on
    co.id = cr.id
left join t_rsv rv on
    cr.id_rsv = rv.id
left join t_rsv_status rs on
    rs.id = cr.id_rsv_status
left join t_ms ms on
    cm.id_ms = ms.id
left join t_isr isr on
    cd.\"14a\" = isr.id
left join public.t_rav rav on
    cm.id_rav = rav.id" . $where . "
order by ad.id " . $limit;
}

// fetcg the sql query
$stmt = $pdo->prepare($sql);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

// replace unreadable stuff
foreach ($result as &$str) {
    $str = str_replace('true', "Ja", $str);
    $str = str_replace('false', "Nein", $str);
    $str = str_replace('None', "", $str);
}

// return json formated data
echo json_encode($result);

// turn back memory limit
ini_set('memory_limit', $limit);
