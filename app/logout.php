<?php

/**
 * Login File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */

// Init session
session_start();

// Include db config
require_once 'includes/db.php';

// Write USER log
$logout = date("Y-m-d H:i:s");
$sql = "INSERT INTO t_users_log (username, date_time, activity) VALUES (:username, :date_time, 'logout')";
$stmt = $pdo->prepare($sql);
$stmt->bindParam(':username', $_SESSION['name'], PDO::PARAM_STR);
$stmt->bindParam(':date_time', $logout, PDO::PARAM_STR);
$stmt->execute();

// Unset all session values
$_SESSION = array();

// Destroy session
session_destroy();

// Redirect to login
header('location: login.php');
exit;
