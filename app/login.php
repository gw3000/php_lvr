<?php

/**
 * Login File Doc Comment
 * PHP Version 7
 *
 * @category  Site
 * @package   LVR
 * @author    Gunther Weissenbaeck <gunther.weissenbaeck@muellerboon.de>
 * @copyright 2020 Gunther Weissenbaeck
 * @license   GNU General Public License version 2 or later; see LICENSE
 * @link      http://lvr
 */


// set default timezone
date_default_timezone_set('Europe/Berlin');

// Include db config
require_once 'includes/db.php';

// Init vars
$email = $password = '';
$email_err = $password_err = '';

// Process form when post submit
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Sanitize POST
  $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

  // Put post vars in regular vars
  $email = trim($_POST['email']);
  $password = trim($_POST['password']);

  // Validate email
  if (empty($email)) {
    $email_err = 'Bitte eine gültige Email-Adresse eingeben!';
  }

  // Validate password
  if (empty($password)) {
    $password_err = 'Bitte eine gültiges Passwort eingeben!';
  }

  // Make sure errors are empty
  if (empty($email_err) && empty($password_err)) {
    // Prepare query
    $sql = 'SELECT name, email, password, role FROM t_users WHERE email = :email';

    // Prepare statement
    if ($stmt = $pdo->prepare($sql)) {
      // Bind params
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);

      // Attempt execute
      if ($stmt->execute()) {
        // Check if email exists
        if ($stmt->rowCount() === 1) {
          if ($row = $stmt->fetch()) {
            $hashed_password = $row['password'];
            if (password_verify($password, $hashed_password)) {
              // SUCCESSFUL LOGIN
              session_start();
              $_SESSION['email'] = $email;
              $_SESSION['name'] = $row['name'];
              $_SESSION['role'] = $row['role'];
              $_SESSION['login'] = date("Y-m-d H:i:s");
              $sql = "INSERT INTO t_users_log (username, date_time, activity) VALUES (:username, :date_time, 'login')";
              $stmt = $pdo->prepare($sql);
              $stmt->bindParam(':username', $_SESSION['name'], PDO::PARAM_STR);
              $stmt->bindParam(':date_time', $_SESSION['login'], PDO::PARAM_STR);
              $stmt->execute();
              header('location: search.php');
            } else {
              // Display wrong password message
              $password_err = 'Das von ihnen eingegebene Passwort ist ungültig!';
            }
          }
        } else {
          $email_err = 'Keinen Account für diese Email-Adresse gefunden!';
        }
      } else {
        die('Etwas ging schief!');
      }
    }
    // Close statement
    unset($stmt);
  }

  // Close connection
  unset($pdo);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/styles.css">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <title>lvr &middot; login</title>
</head>

<body>
  <div class="container">
    <form class="form-signin" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
      <div class="jumbotron">
        <h2>lvr &middot; Datenbank <small class="red">... Login</small></h2>
        <hr class="my-4">
        <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?> ">
          <label for="email">Email Adresse</label>
          <input type="email" name="email" class="form-control" value="<?php echo $email; ?>">
          <span class="invalid-feedback"><?php echo $email_err; ?></span>
        </div>
        <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
          <label for="password">Passwort</label>
          <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
          <span class="invalid-feedback"><?php echo $password_err; ?></span>
        </div>
        <div class="form-row">
          <hr class="my-4">
          <div class="col">
            <input type="submit" value="Anmelden" class="btn btn-default btn-block btn-lg">
          </div>
        </div>
      </div>
    </form>
  </div>
</body>

</html>