#!/bin/bash
SRCE="/srv/docker/lvr/backup"
YEAR=$(date '+%Y')
MONT=$(date '+%m')
FOLD="$SRCE/$YEAR/$MONT/"

rm -fr "$SRCE/dump.sql" 2>/dev/null
rm -fr "$SRCE/dump.sql.tar.gz" 2>/dev/null
docker exec -t lvr-postgres pg_dumpall -c -U postgres > "$SRCE/dump.sql"
tar -czvf "$SRCE/dump.sql.tar.gz" "$SRCE/dump.sql"
rm -fr "$SRCE/dump.sql" 2>/dev/null
cp "$SRCE/dump.sql.tar.gz" "$SRCE/dump_`date +%Y-%m-%d"__"%H_%M_%S`.sql.tar.gz"

if [ ! -d $FOLD ]; then
    mkdir -p $FOLD
fi
if [ -d $FOLD ]; then
    mv $SRCE/dump_$YEAR-$MONT-*.sql.tar.gz $FOLD
fi

cd $SRCE
# scp -q *.sql.tar.gz gw@192.168.4.24:/volume1/backup/lvr/
# scp -q -r * gw@192.168.4.24:/volume1/backup/lvr/
rsync -azv -u -e ssh $SRCE/* localroot@192.168.4.21:/volume1/data_backup/braun/backup_db
