#!/bin/bash
pfad="../backup"

docker exec -it lvr-postgres psql -U lvr -d postgres -c "DROP DATABASE db_lvr;"
docker exec -it lvr-postgres psql -U lvr -d postgres -c "CREATE DATABASE db_lvr;"
cat "$pfad/dump.sql" | docker exec -i lvr-postgres psql -U lvr -d db_lvr
