"""module to download all documents"""

import json
import dotenv
import requests

from os import environ as e, system


# load .env Environment
dotenv.load()

# path to download documents to
dl_path = e.get("DL_PATH")
if dl_path == "":
    dl_path = "./"
else:
    if dl_path[-1] != "/":
        dl_path += "/"

# api token for request
header = {"Authorization": e.get("API_LVR_TOKEN")}
# token for curl
token = "'Authorization: " + e.get("API_LVR_TOKEN") + "'"
# all submitted contracts
url_subm = "https://api.lv-r.de/contracts?status=Submitted"
# the specific contracts and its data
url_spec = "https://api.lv-r.de/contracts/"  # + id


def get_contract_ids(url):
    contract_ids = []
    document_ids = []
    """fetches the contract data from platform db"""
    req = requests.get(url + "&page=1", headers=header)
    if req.ok:  # if status is 200, do something
        data = json.loads(req.text)
        # fetch all continue
        counter = 2
        data_a = data
        while data != []:
            req = requests.get(url + "&page=" + str(counter), headers=header)
            if req.ok:
                data = json.loads(req.text)
                data_a += data
            counter += 1

        for item in data_a:
            if (item["client"] is not None and
                    item["createdBy"]["user"]["id"] != 5):
                # show id specific document url
                document_ids.append(
                    [item["id"], [item["fileNumber"].replace("/", "-")]])
                url_doc = url_spec + item["id"] + "/required-documents"
                req_doc = requests.get(url_doc, headers=header)
                if req_doc.ok:
                    contract_ids.append(
                        [item["id"], [item["fileNumber"].replace("/", "-")]])

    return (contract_ids, document_ids)


def gen_link(id):
    """return document name and url to download the file"""
    gen_links = []
    url_doc = url_spec + id + "/required-documents"
    req_doc = requests.get(url_doc, headers=header)
    if req_doc.ok:
        doc_json = json.loads(req_doc.text)
        for item in doc_json:
            doc_type = item["type"]
            if "fileNames" in item:
                files = item["fileNames"]
                for file in files:
                    # "https://api.lv-r.de/contracts/:id/required-documents/:type/fileName(s)"
                    url_doc = url_spec + id + "/required-documents/" + doc_type + "/" + file
                    target_name = doc_type + "____" + file
                    gen_links.append([url_doc, target_name])
                    target_name = url_doc = ""
        return gen_links


def documents_worker():
    """a function to work them all"""
    ids = get_contract_ids(url_subm)
    ids_documents = ids[0]
    for id, refnum in ids_documents:
        links = gen_link(id)
        for link, doc_name in links:
            # make folders
            dirname = refnum[0].replace("/", "-")
            system(f'mkdir -p {dl_path}{dirname}')
            system(
                f"curl -X GET -H  'accept: application/json' -H {token} '{link}' -o '{dl_path}{dirname}/{doc_name}'")

    ids_pdf = ids[1]
    for id, refnum in ids_pdf:
        # authority-documet: Vollmacht_MBD.pdf
        # curl -X GET "https://api.lv-r.de/contracts/Xqo/authority-document" -H  "accept: application/json" -H  "Authorization: 2hTsLSjmy1E4jUBAf53xhbkpRDcuprppyriWytMLbzC1"

        # document: Vertragsunterlagen_MBD.pdf
        # curl -X GET "https://api.lv-r.de/contracts/Xqo/document" -H  "accept: application/json" -H  "Authorization: 2hTsLSjmy1E4jUBAf53xhbkpRDcuprppyriWytMLbzC1"

        docs = ["authority-document", "document"]
        for doc in docs:
            # download link
            link = f"https://api.lv-r.de/contracts/{id}/{doc}"
            
            # dirname in fs
            dirname = refnum[0].replace("/", "-")
            
            # name of downloaded Document
            if doc == "authority-document":
                doc_name = "Vollmacht_MBD.pdf"
            elif doc == "document":
                doc_name = "Vertragsunterlagen_MBD.pdf"
            
            # make the folder
            system(f'mkdir -p {dl_path}{dirname}')

            # download file to folder
            shell_cmd = f"curl -X GET -H  'accept: application/json' -H {token} '{link}' -o '{dl_path}{dirname}/{doc_name}'"
            system(shell_cmd)
            print(shell_cmd)


if __name__ == "__main__":
    documents_worker()
