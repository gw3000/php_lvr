# LVR - Lebensversicherung WebApp

Goal of the project is to access client data from an external web application,
which manages contract data. The entered data is to fetch it up from the
database/api of the external app into this app. This app is also to compare
and complete data.

## Dockerized web stack

-   nginx proxy
-   nginx
-   php
-   postgres

## WebApp

Technologies used:

-   html / php
-   css / bootstrap / bootswatch
-   sql / postgres
-   javascript / jquery
-   ajax
-   datatable / bootstrap 4

## Docker

docker-compose needs an env-file (.env) in the docker root folder:

```bash
# domain name
VIRTUAL_HOST=lvr.local

# database incredentials
POSTGRES_USER=...
POSTGRES_PASSWORD=...
POSTGRES_DB=db_lvr
```

## Backup and Restore PostgreSQL Database

You can find scripts to backup and restore the database in the bin folder:

-   backup_db.sh
-   restore_db.sh

The backups will be placed in the backup folder.
